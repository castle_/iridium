#pragma once
#include "SDK.h"
#include "Util.h"

class CAutoAirblast : public IGameEventListener2
{
public:

	void Run(CBaseEntity * pLocal, CUserCmd * pCommand);

	void FireGameEvent(IGameEvent * event);

	void InitEvent();

};

extern  CAutoAirblast gAutoAirblast;