#pragma once
#include "SDK.h"
#include "FrameStageNotify.h"
#include "Controls.h"

class CMisc
{
public:
	Checkbox		misc_bhop				=				Checkbox("Bunny Hop");
	Checkbox		misc_astrafe			=				Checkbox("Auto Strafe");
	Checkbox		misc_tp					=				Checkbox("Third Person");
	Checkbox		misc_cheats				=				Checkbox("Cheats Bypass");
	Checkbox		misc_pure				=				Checkbox("Pure Bypass");
	Checkbox		misc_smac				=				Checkbox("Anti-SMAC");
	Checkbox		misc_noisem				=				Checkbox("Noisemaker Spam");
	Checkbox		misc_tourn				=				Checkbox("Tournament Spam");
	Checkbox		misc_ts					=				Checkbox("Time Shift");
	Slider			misc_ts_value			=				Slider("Time Shift Value", 0, 0, 5000, 10);
	Checkbox		misc_decloak			=				Checkbox("Insta Decloak");
	Checkbox		misc_dshoot				=				Checkbox("Double Shoot");
	Checkbox		misc_sticky				=				Checkbox("Sticky Spam");
	Checkbox		misc_instarev			=				Checkbox("Insta Rev");

	Slider			misc_fov				=				Slider("Custom FOV", 90, 75, 150, 1);
	Checkbox		misc_chatspam			=				Checkbox("Chat Spam");
	Slider			misc_newlines			=				Slider("Chat Newlines", 0, 0, 32, 1);
	Listbox			misc_killsay			=				Listbox("Killsay", { "OFF", "ON" });
	Slider			misc_ks_newls			=				Slider("Killsay Newlines", 0, 0, 32, 1);
	Checkbox		misc_icontest			=				Checkbox("drawtest");

	Checkbox		spec_list				=				Checkbox("Spectator List");
	Checkbox		alt_keybinds			=				Checkbox("Stable Keybinds");
	
	//KeyBind			misc_ts_key				=				KeyBind("Time Shift", 0x46); //F
	KeyBind			misc_thirdperson_key    =				KeyBind("Thirdperson", VK_MBUTTON); //mouse3

	void Run(CBaseEntity* pLocal, CUserCmd* pCommand, bool* bSendPacket);
	void FSN(CBaseEntity* you, ClientFrameStage_t curStage);

	Vector lastAngles = Vector(0, 0, 0);

private:
	//Other funcs

	void NoisemakerSpam(PVOID kv);
};

extern CMisc gMisc;