#pragma once
#include "SDK.h"
#include "Util.h"

class CTimeShift
{
public:
	void Run(CBaseEntity* pLocal, CUserCmd* pCmd);
	void Regular(int amount);
	void InstaDecloak(CBaseEntity* pLocal, CUserCmd* pCmd);
	void DoubleShoot(CBaseEntity* pLocal, CUserCmd* pCmd);
	void InstaRev(CBaseEntity* pLocal, CUserCmd* pCmd);
	void StickySpam(CBaseEntity* pLocal, CUserCmd* pCmd);
private:
	void TimeShift(int amount);
	//float fLastTime = 0;
	//bool bIsHolding, bWasHolding;
};

extern CTimeShift gTShift;