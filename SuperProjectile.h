#pragma once
#include "SDK.h"
#include "Util.h"
#include "CGlobalVars.h"

class SuperProjectile
{
public:
	void Predict(float projvel);

	void CreateMove(CUserCmd* cmd);
	void Hitscan();
	CBaseEntity* targEnt;
	int backtrack = 0; //set to 0 every loop, if its not 0 we change
	int bone = 0; // only used to tell the ESP which bone we are aiming at
	float currChargeDmg = 0;

	Vector predictedTicks[2048][250]; //projectile aimbot shit to be read by ESP
	int predictedTickCount;
	float ticksToHit;

	Vector myWeapon; //move to protected when done debugging
protected:
	CBaseEntity* lp;
	Vector target, myAngles, myHead;
	int* chosenArray;
	bool sniperDamageCheck = false;
	int localTeam;
	float fov;
	float gravity = 0.18;
	trace_t tr;
	Ray_t Ray;
	CTraceFilter filter;
	CTraceFilter filterNoPlayer;
};

extern SuperProjectile gSuperProj;