#include "Menu.h"
#include "CDrawManager.h"
#include "GUI.h"

#include "Aimbot.h"
#include "Triggerbot.h"
#include "Misc.h"
#include "ESP.h"
#include "Radar.h"
#include "CAntiAim.h"

Menu gMenu;

#define keyDown GetAsyncKeyState
#define keyPress(x) keyDown(x) & 1
#define boolStr(boolean) string(boolean? "True" : "False")

//literally meaningless fucking numbers
#define WPARAM_M4 65568
#define WPARAM_M5 131136
#define WPARAM_M4_UP 65536
#define WPARAM_M5_UP 131072

CKey gKey;

// - Feel free to use whatever default width you want
#define GROUP_WIDTH 180

void Menu::CreateGUI()
{
	Tabs.AddTab(
		new Tab("Aimbot", {
			new Groupbox("Main",
				{	
					&gAim.aimbot_active,
					&gAim.aimbot_fov,
					&gAim.aimbot_slowaim,
					&gAim.aimbot_slow_amt,
					&gAim.aimbot_autoshoot,
					&gAim.aimbot_silent
				}, GROUP_WIDTH),
				new Groupbox("Hitbox",
					{
						&gAim.aimbot_hitscan,
						&gAim.aimbot_position
					}, GROUP_WIDTH),
			new Groupbox("Misc",
				{
					&gAim.aimbot_minigun_toggle,
					&gAim.aimbot_ignore_cloak,
					&gAim.aimbot_adv_feet_aim,
					&gAim.aimbot_wait_for_charge,
					&gAim.aimbot_scoped_only,
					&gAim.aimbot_canshoot
				}, GROUP_WIDTH)
			})
	);

	Tabs.AddTab(
		new Tab("Trigger", {
			new Groupbox("Trigger Bot",
				{
					&gTrigger.triggerbot_active,
					&gTrigger.triggerbot_headonly
				}, GROUP_WIDTH),
				new Groupbox("Trigger Shoot",
					{
						&gTrigger.triggershoot_active,
						&gTrigger.triggershoot_autobackstab
					}, GROUP_WIDTH)
			})
	);

	Tabs.AddTab(
		new Tab("ESP", {
			new Groupbox("Players",
				{
					&gESP.esp_enabled,
					&gESP.esp_name,
					&gESP.esp_class,
					&gESP.esp_boxes,
					&gESP.esp_bones,
					&gESP.esp_health,
					&gESP.esp_conditions,
					&gESP.esp_snaplines,
				}, GROUP_WIDTH),
			new Groupbox("Preferences",
				{
					&gESP.esp_position,
					&gESP.esp_enemy_only,
					&gESP.esp_visible_only,
					&gESP.esp_vis_check,
					&gESP.esp_disguise	

				}, GROUP_WIDTH),
			new Groupbox("Misc",
				{
					//and buildings
					&gESP.esp_items,
					&gESP.esp_glow,
					&gESP.esp_glow_enemy_only,
					&gESP.esp_glow_items,
				}, GROUP_WIDTH)
			})
	);

	Tabs.AddTab(
		new Tab("Visuals", {
			new Groupbox("Main",
				{
					&gESP.visual_nozoom,
					&gESP.visual_aimware
				}, GROUP_WIDTH),
			new Groupbox("Chams",
				{
					&gESP.visual_chams_active,
					&gESP.visual_chams_player,
					&gESP.visual_chams_weapons,
					&gESP.visual_hands
				}, GROUP_WIDTH),
			new Groupbox("Misc",
				{
					&gESP.visual_spawndoors,
					&gESP.visual_nightmode,
				}, GROUP_WIDTH),
			new Groupbox("Colors",
				{
					&gESP.visual_colors,
					&gESP.visual_clr_aim,
					&gESP.visual_clr_friend,
					&gESP.visual_clr_rage,
					&gESP.visual_clr_red,
					&gESP.visual_clr_red_menu,
					&gESP.visual_clr_blu,
					&gESP.visual_clr_blu_menu
				}, GROUP_WIDTH)
			})
	);

	Tabs.AddTab(
		new Tab("Radar", {
			new Groupbox("Main",
				{
					&gRadar.radar_active,
					&gRadar.radar_enemy_only,
					&gRadar.radar_x,
					&gRadar.radar_y,
					&gRadar.radar_size,
					&gRadar.radar_range,
					&gRadar.radar_alpha,
					&gRadar.radar_style
				}, GROUP_WIDTH),
			new Groupbox("Icons",
				{
					&gRadar.radar_health,
					&gRadar.radar_portraits,
					&gRadar.radar_icon_bg,
					&gRadar.radar_icn_size
				}, GROUP_WIDTH)
			})
	);

	Tabs.AddTab(
		new Tab("HvH", {
			new Groupbox("Main",
				{
					&gAntiAim.aa_active,
					&gAntiAim.aa_pitch_fake,
					&gAntiAim.aa_pitch_real,
					&gAntiAim.aa_pitch_real_custom,
					&gAntiAim.aa_yaw,
					&gAntiAim.aa_spin_speed,
					&gAntiAim.aa_resolver,
					&gAntiAim.aa_resolver_rageonly //cfg
				}, GROUP_WIDTH),
			new Groupbox("Misc",
				{
					&gAntiAim.aa_real_local_angs //cfg
				}, GROUP_WIDTH),
			new Groupbox("Fakelag",
				{
					&gAntiAim.fakelag_active,
					&gAntiAim.fakelag_disable_attack,
					&gAntiAim.fakelag_choke,
					&gAntiAim.fakelag_unchoke
				}, GROUP_WIDTH)
			})
	);

	Tabs.AddTab(
		new Tab("Misc", {
			new Groupbox("Main",
				{
					&gMisc.misc_bhop,
					&gMisc.misc_astrafe,
					&gMisc.misc_tp,
					&gMisc.misc_cheats,
					&gMisc.misc_pure,
					&gMisc.misc_smac,
					&gMisc.misc_noisem,
					&gMisc.misc_tourn,
					&gMisc.spec_list
				}, GROUP_WIDTH),
				//new Groupbox("Time Shift",
				//	{
				//		&gMisc.misc_ts,
				//		&gMisc.misc_ts_value
				//	}, GROUP_WIDTH),
				//new Groupbox("TS Exploits",
				//	{
				//		&gMisc.misc_decloak,
				//		&gMisc.misc_dshoot,
				//		&gMisc.misc_sticky,
				//		&gMisc.misc_instarev //cfg
				//	}, GROUP_WIDTH),
			//backtracking missing
				new Groupbox("Misc",
					{
						&gMisc.misc_fov,
						&gMisc.misc_chatspam,
						&gMisc.misc_newlines,
						&gMisc.misc_killsay, //cfg
						&gMisc.misc_ks_newls, //cfg
						&gMisc.misc_icontest
					}, GROUP_WIDTH)
			})
	);

	Tabs.AddTab(
		new Tab("Hotkeys", {
			new Groupbox("Binds",
				{
					&gAim.aimbot_key,
					&gTrigger.triggerbot_key,
					//&gMisc.misc_ts_key,
					&gMisc.misc_thirdperson_key
				}, GROUP_WIDTH),
			new Groupbox("Alt Binds",
				{
					&gMisc.alt_keybinds,
					&gAim.aimbot_key_alt,
					&gTrigger.triggerbot_key_alt
				}, GROUP_WIDTH)
			})

	);
}

#define TAB_WIDTH 150
#define MENU_TOPBAR 30
//#define MARGIN 4

void Menu::Draw()
{
#pragma region Handle input
	if (key == VK_INSERT || key == VK_F11)
	{
		enabled = !enabled;
		gSettings.SaveSettings(".\\Iridium.json");
	}

	if (!enabled)
		return;
		
	//style->WaterMark("read if gay", 20, 20);
#ifdef DEV_BUILD
	gDraw.DrawStringFont(5, 5, Color::White(), /*XorString(*/"Iridium Developer Build :)"/*)*/, gFonts.consolas);
#else
	gDraw.DrawStringFont(5, 5, Color::White(), "Iridium, Public build!", gFonts.consolas);
#endif
	gDraw.DrawStringFont(5, 5 + 14, Color::White(), __TIMESTAMP__, gFonts.consolas);
	gDraw.DrawStringFont(5, 5 + (14 * 2), Color::White(), "Use mouse to navigate the menu", gFonts.consolas);
	gDraw.DrawStringFont(5, 5 + (14 * 3), Color::White(), "Use scroll wheel/mouse clicks to change menu values", gFonts.consolas);

	static bool dragging = false;

	if (mb == e_mb::lclick && mouseOver(pos.x, pos.y, scale.x, MENU_TOPBAR))
		dragging = true;
	else if (mb != e_mb::ldown)
		dragging = false;

	if (dragging && focus == 0)
	{
		pos.x += mouse.x - pmouse.x;
		pos.y += mouse.y - pmouse.y;
	}

	POINT _pos = pos, _scale = scale;
	_scale.y += MENU_TOPBAR;
#pragma endregion

#pragma region Main window
	int topbar = style->TopBar(_pos.x, _pos.y, _scale.x, "Iridium");

	// Re-adjusting pos and scale for easy coding
	_pos.y += topbar, _scale.y -= topbar;

	// Tab region
	gDraw.DrawRect(_pos.x, _pos.y, TAB_WIDTH, _scale.y, Color(25));
	// Dividing line
	gDraw.DrawRect(_pos.x + TAB_WIDTH - 2, _pos.y, 2, _scale.y, Color(20));

	Tabs.SetPos(_pos.x, _pos.y + topbar);
	Tabs.SetWidth(TAB_WIDTH);
	Tabs.HandleInput();
	Tabs.Draw(false);

	// Control region
	gDraw.DrawRect(_pos.x + TAB_WIDTH, _pos.y, _scale.x - TAB_WIDTH, _scale.y, Color(36, 36, 42));
	// Re-adjusting pos and scale again
	_pos.x += TAB_WIDTH + 3, _scale.x = scale.x - (_pos.x - pos.x);

#pragma endregion

#pragma region Controls
	if (Tabs.active)
	{
		int cx = _pos.x + 13, cy = _pos.y + 12;
		int maxWidth = 0;
		vector<BaseControl*> controls = Tabs.active->GetChildren();
		for (size_t i = 0; i < controls.size(); i++)
		{
			if (controls[i]->flags & nodraw)
				continue;

			if (cy + controls[i]->GetHeight() > scale.y + _pos.y - 12)
				cy = _pos.y + 12, cx += 13 + maxWidth + 10, maxWidth = 0;

			if (controls[i]->GetWidth() > maxWidth)
				maxWidth = controls[i]->GetWidth();
			controls[i]->SetPos(cx, cy);

			bool over = mouseOver(cx, cy, controls[i]->GetWidth(), controls[i]->GetHeight());
			bool getInput = !(controls[i]->flags & noinput) && over && !IsDialogOpen();
			if (getInput)
				controls[i]->HandleInput();

			controls[i]->Draw(getInput);

			cy += controls[i]->GetHeight() + SPACING;
		}
	}
#pragma endregion

#pragma region Dialogs
	size_t last = dialogs.size() - 1;
	if (dialogs.size() > 1)
	{
		e_mb new_mb = mb;
		e_mw new_mw = mw;
		POINT new_mouse = mouse, new_pmouse = pmouse;

		// Enforce focus so that only the last dialog gets to use these variables
		mb = e_mb::null, mw = e_mw::null, mouse = pmouse = { 0, 0 };

		for (size_t i = 0; i < last; i++)
		{
			if (dialogs[i] == nullptr)
				continue;

			dialogs[i]->Draw(dialogs[i]->data, i + 1);
		}
		mb = new_mb, mw = new_mw, mouse = new_mouse, pmouse = new_pmouse;
		dialogs[last]->Draw(dialogs[last]->data, last + 1);
	}
	else if (!last)
		dialogs[last]->Draw(dialogs[last]->data, last + 1);

	if (key == VK_ESCAPE && dialogs.size())
		dialogs.pop_back();
#pragma endregion
}

bool Menu::mouseOver(int x, int y, int w, int h)
{
	return mouse.x >= x && mouse.x <= x + w && mouse.y >= y && mouse.y <= y + h;
}

void Menu::GetInput()
{
	int mx = 0, my = 0;
	gInts.Surface->GetCursorPosition(mx, my);

	pmouse = mouse;
	mouse = { mx, my };

	if (keyDown(VK_LBUTTON))
	{
		if (mb == e_mb::lclick || mb == e_mb::ldown)
			mb = e_mb::ldown;
		else
			mb = e_mb::lclick;
	}
	else if (keyDown(VK_RBUTTON))
	{
		if (mb == e_mb::rclick || mb == e_mb::rdown)
			mb = e_mb::rdown;
		else
			mb = e_mb::rclick;
	}
	else
		mb = e_mb::null;
}

void Menu::EndInput()
{
	// Reseting Window message variables so they won't stick
	mw = e_mw::null;
	key = NULL;
}

LRESULT __stdcall Hooked_WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_MOUSEWHEEL:
		if ((int)wParam < 0)
			gMenu.mw = e_mw::up;
		else
			gMenu.mw = e_mw::down;
		break;
	case WM_KEYDOWN:
		if (wParam > 255)
			break;
		gMenu.keys[wParam] = true, gMenu.last_key = wParam, gMenu.key = wParam;

		break;
	case WM_KEYUP:
		if (wParam > 255)
			break;
		gMenu.keys[wParam] = false;
		if (gMenu.last_key == wParam)
			gMenu.last_key = NULL;
		break;
	case WM_MBUTTONDOWN:
		if (wParam > 255)
			break;
		if (wParam == MK_MBUTTON)
		{
			gMenu.keys[VK_MBUTTON] = true;
			gMenu.last_key = VK_MBUTTON;
			gMenu.key = VK_MBUTTON;
			break;
		}
	case WM_MBUTTONUP:
		if (wParam > 255)
			break;
		//0? seriously?
		if (wParam == 0) //WPARAM_MOUSE3_UP
		{
			gMenu.keys[VK_MBUTTON] = false;
			//if (gMenu.last_key == VK_MBUTTON)
			gMenu.last_key = NULL;
			break;
		}
	case WM_XBUTTONDOWN:
		//65568 = mouse4
		//131136 = mouse5
		if (wParam == WPARAM_M4)
		{
			gMenu.keys[VK_XBUTTON1] = true;
			gMenu.last_key = VK_XBUTTON1;
			gMenu.key = VK_XBUTTON1;
			break;
		}
		else if (wParam == WPARAM_M5)
		{
			gMenu.keys[VK_XBUTTON2] = true;
			gMenu.last_key = VK_XBUTTON2;
			gMenu.key = VK_XBUTTON2;
			break;
		}
	case WM_XBUTTONUP:
		if (wParam == WPARAM_M4_UP)
		{
			gMenu.keys[VK_XBUTTON1] = false;
				gMenu.last_key = NULL;
			break;
		}
		else if (wParam == WPARAM_M5_UP)
		{
			gMenu.keys[VK_XBUTTON2] = false;
				gMenu.last_key = NULL;
			break;
		}
	}

	//if (!gCvars.cade_menu)
	//	return CallWindowProc(gGUI.windowProc, hWnd, uMsg, wParam, lParam);
	//else
		return CallWindowProc(gMenu.windowProc, hWnd, uMsg, wParam, lParam);
}

void Menu::OpenDialog(Dialog& dlg)
{
	dialogs.push_back(&dlg);
	focus = dialogs.size();
}
void Menu::CloseDialog(size_t Index)
{
	if (Index == 0)
		return;

	Index--;
	if (Index >= dialogs.size())
		return;

	dialogs.erase(dialogs.begin() + Index);
	focus = dialogs.size();
}