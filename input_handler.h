#pragma once
#include "SDK.h"
#include <windef.h>
#include <minwindef.h>

namespace GLOBAL
{
	//extern HWND csgo_hwnd;

	extern bool should_send_packet;
	extern bool is_fakewalking;
	extern int choke_amount;

	extern Vector real_angles;
	extern Vector fake_angles;
}

namespace Utils
{
	namespace INPUT
	{
		LRESULT CALLBACK WndProcCallback(HWND Hwnd, UINT Message, WPARAM wParam, LPARAM lParam);

		struct MouseInfo
		{
			Vector2D position;

			int scroll;
			int left;
			int right;
		};
		class InputHandler
		{
		public:
			void Init();
			void Update();

			MouseInfo GetMouseInfo();
			void GetKeyboardState(int*);
			int GetKeyState(int);

			int GetKeyStateEx(int key);

			void Callback(UINT, WPARAM, LPARAM);

		private:
			MouseInfo cached_mouse_info;
			int cached_pressed_keys[255];
			int cached_pressed_keys_ex[255]; // these are only reset when you call GetKeyStateEx()

			Vector2D GetMousePosition();
		}; extern InputHandler input_handler;
	}
}