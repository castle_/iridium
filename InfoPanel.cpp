#include "InfoPanel.h"
#include "CDrawManager.h"
#include <string>
#include "SDK.h"
#include "CDrawManager.h"
#include "Util.h"
InfoPanel gInfoPanel;

#define keyDown GetAsyncKeyState
#define keyPress(x) keyDown(x) & 1
#define boolStr(boolean) string(boolean? "ON" : "OFF")

#define MENU_WIDTH 200

//#include "Aim.h"
//#include "ESP.h"
//#include "Misc.h"

void InfoPanel::createGUI()
{
	static bool
		main = true,
		trigger = false,
		esp = false,
		menu = false,
		misc = false;

	if (addTab("", main))
	{
		addBool("- Test", gCvars.misc_bunnyhop);
	}
}

void InfoPanel::draw()
{
	if (keyPress(VK_INSERT) || keyPress(VK_F11))
		enabled = !enabled;

	//if (!enabled)
	//	return;

	if (enabled)
	{
		getInput();
	}
	else
	{
		static bool dragging = false;
		if (mb == info_e_mb::lclick && mouseOver(pmenu.x, pmenu.y, MENU_WIDTH, 14))
			dragging = true;
		else if (mb != info_e_mb::ldown)
			dragging = false;

		if (dragging)
		{
			pmenu.x += mouse.x - pmouse.x;
			pmenu.y += mouse.y - pmouse.y;
		}

	}

	CBaseEntity *pLocal = GetBaseEntity(me);
	Color clrColor = gDraw.GetPlayerColor(pLocal, 255);

	createGUI();
	gDraw.DrawRect(pmenu.x, pmenu.y, MENU_WIDTH, (itemlist.size() + 1) * 14 + 3, /*clrColor*/ Color(0, 0, 0, 128));
	gDraw.OutlineRect(pmenu.x, pmenu.y, MENU_WIDTH, (itemlist.size() + 1) * 14 + 3, clrColor /*Color(50, 100, 255)*/);
	gDraw.DrawString(pmenu.x + MENU_WIDTH / 2 - 30, pmenu.y + 3, Color(255, 255, 255, 255), "Info Panel");


	int x = pmenu.x + 5, y = pmenu.y + 16;
	for (size_t i = 0; i < itemlist.size(); i++)
	{
		if (itemlist[i].type == e_type::null)
			continue;

		if (mouseOver(x, y, MENU_WIDTH - 10, 13))
		{
			gDraw.DrawRect(x, y, MENU_WIDTH - 10, 13, Color(255, 255, 255, 255));
			if (mb == info_e_mb::lclick || mb == info_e_mb::rclick)
			{
				bool add = mb == info_e_mb::rclick;
				switch (itemlist[i].type)
				{
				case e_type::tab:
				case e_type::boolean: *itemlist[i].bValue = !*itemlist[i].bValue; break;
				case e_type::integer:
					*itemlist[i].iValue += add ? itemlist[i].step : -itemlist[i].step;
					if (*itemlist[i].iValue > itemlist[i].max)
						*itemlist[i].iValue = itemlist[i].min;
					else if (*itemlist[i].iValue < itemlist[i].min)
						*itemlist[i].iValue = itemlist[i].max;
				}
			}
		}
		if (itemlist[i].type != e_type::tab)
			gDraw.DrawString(x, y, Color(255, 255, 255, 255), itemlist[i].name);
		else
			gDraw.DrawString(x, y, clrColor, itemlist[i].name);

		if (itemlist[i].type != e_type::tab)
		{
			string value = itemlist[i].type == e_type::boolean ? boolStr(*itemlist[i].bValue) : to_string(*itemlist[i].iValue);
			//if (itemlist[i].type == e_type::integer)
			//{
			//	if (itemlist[i].name == " - Key" || itemlist[i].name == " - - Key")
			//	{
			//		switch (*itemlist[i].iValue)
			//		{
			//		case 0: value = "None"; break;
			//		case 1: value = "Mouse1"; break;
			//		case 2: value = "Mouse2"; break;
			//		case 3: value = "Mouse3"; break;
			//		case 4: value = "Mouse4"; break;
			//		case 5: value = "Mouse5"; break;
			//		case 6: value = "Shift"; break;
			//		case 7: value = "Alt"; break;
			//		case 8: value = "F"; break;
			//		default:
			//			break;
			//		}
			//	}
			//	else if (itemlist[i].name == " - Target")
			//	{
			//		switch (*itemlist[i].iValue)
			//		{
			//		case 0: value = "Enemy"; break;
			//		case 1: value = "Team"; break;
			//		case 2: value = "Both"; break;
			//		default:
			//			break;
			//		}
			//	}
			//	else if (itemlist[i].name == " - - AA Pitch")
			//	{
			//		switch (*itemlist[i].iValue)
			//		{
			//		case 0: value = "OFF"; break;
			//		case 1: value = "Fakedown"; break;
			//		case 2: value = "Fakeup"; break;
			//		case 3: value = "Half up"; break;
			//		default:
			//			break;
			//		}
			//	}
			//	else if (itemlist[i].name == " - - AA Yaw")
			//	{
			//		switch (*itemlist[i].iValue)
			//		{
			//		case 0: value = "OFF"; break;
			//		case 1: value = "Left"; break;
			//		case 2: value = "Right"; break;
			//		case 3: value = "Jitter"; break;
			//		case 4: value = "Spin"; break;
			//		default:
			//			break;
			//		}
			//	}
			//}
			//d
			gDraw.DrawString(x + MENU_WIDTH - 50, y, Color(255, 255, 255, 255), value.c_str());
		}

		y += 14;
	}

	itemlist.clear();
}

bool InfoPanel::mouseOver(int x, int y, int w, int h)
{
	return mouse.x >= x && mouse.x <= x + w && mouse.y >= y && mouse.y <= y + h;
}

void InfoPanel::getInput()
{
	int mx = 0, my = 0;
	gInts.Surface->GetCursorPosition(mx, my);

	pmouse = mouse;
	mouse = { mx, my };

	if (keyDown(VK_LBUTTON))
	{
		if (mb == info_e_mb::lclick || mb == info_e_mb::ldown)
			mb = info_e_mb::ldown;
		else
			mb = info_e_mb::lclick;
	}
	else if (keyDown(VK_RBUTTON))
	{
		if (mb == info_e_mb::rclick || mb == info_e_mb::rdown)
			mb = info_e_mb::rdown;
		else
			mb = info_e_mb::rclick;
	}
	else
		mb = info_e_mb::null;
}

bool InfoPanel::addTab(const char* name, bool &value)
{
	Item temp;
	temp.name = name;
	temp.type = e_type::tab;
	temp.bValue = &value;

	itemlist.push_back(temp);
	return value;
}

void InfoPanel::addBool(const char* name, bool &value)
{
	Item temp;
	temp.name = name;
	temp.type = e_type::boolean;
	temp.bValue = &value;

	itemlist.push_back(temp);
}

void InfoPanel::addInt(const char* name, int &value, int min, int max, int step)
{
	Item temp;
	temp.name = name;
	temp.type = e_type::integer;
	temp.iValue = &value;
	temp.min = min, temp.max = max, temp.step = step;

	itemlist.push_back(temp);
}