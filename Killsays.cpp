#include "KillSays.h"
#include "Misc.h"

CKillSay gKillSay;
//CMisc gMisc;

/*void CKillSay::Run(CBaseEntity* pLocal, CUserCmd* pCommand)
{
if(!gCvars.killsay.enabled || !gCvars.killsay.selection)
return;
}*/

void CKillSay::InitKillSay()
{
	gInts.EventManager->AddListener(this, "player_death", false);
	wasEventInitialized = true;
}

void CKillSay::FireGameEvent(IGameEvent *event)
{

	if (!strcmp(event->GetName(), "player_death") && gMisc.misc_killsay.value > 0)
	{
		/*	int vid = event->GetInt("victim_entindex", -1);

		int attacker = event->GetInt("attacker", -1);

		if(vid < 1 || attacker < 1) return;

		CBaseEntity* victim = gInts.EntList->GetClientEntity(vid);
		CBaseEntity* att = gInts.EntList->GetClientEntity(attacker);
		if(!att || !victim)return;
		if(att->GetIndex() != GetBaseEntity(me)->GetIndex()) return;*/

		int vid = event->GetInt("victim_entindex");
		int kid = event->GetInt("attacker");
		if (gInts.Engine->GetPlayerForUserID(kid) != me) return;

		const std::vector<std::string>* source = nullptr;
		std::string msg;
		msg.append("say ");

		switch (gMisc.misc_killsay.value)
		{
		case 1://Niggerhook
			source = &KillSay::niggerhook;
			break;
		case 2://nullcore
			source = &KillSay::ncc;
			break;
		case 3: //chill
			source = &KillSay::chill;
		}

		if (gMisc.misc_ks_newls.value)//\x0D
		{
			msg.append(" ");
			msg.append(repeat(gMisc.misc_ks_newls.value, "\x0D"));
		}
		msg.append(source->at(rand() % source->size()));

		player_info_t pInfo;
		if (gInts.Engine->GetPlayerInfo(vid, &pInfo))
		{
			Util->ReplaceString(msg, "%name%", pInfo.name);
			Util->ReplaceString(msg, "% name %", pInfo.name);
		}
		/*	Util->ReplaceString(msg, "\n", "\x0D");*/
		gInts.Engine->ClientCmd(msg.c_str());



	}
}