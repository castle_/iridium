#include "Util.h"
#include "Aimbot.h"

CUtil* Util;

void CUtil::VectorTransform(const Vector& vSome, const matrix3x4& vMatrix, Vector& vOut)
{
	for (auto i = 0; i < 3; i++)
		vOut[i] = vSome.Dot((Vector&)vMatrix[i]) + vMatrix[i][3];
}
DWORD CUtil::killCvars() // credits f1/outi idk its stolen from niggerhook
{
	ConCommandBase *base = gInts.Cvar->GetCommands();

	int count = 0;

	while (base)
	{

		if (base->m_nFlags & (int)ConvarFlags::FCVAR_CHEAT)
			base->m_nFlags &= ~(int)ConvarFlags::FCVAR_CHEAT;

		if (base->m_nFlags & (int)ConvarFlags::FCVAR_REPLICATED)
			base->m_nFlags &= ~(int)ConvarFlags::FCVAR_REPLICATED;

		if (base->m_nFlags & (int)ConvarFlags::FCVAR_PROTECTED)
			base->m_nFlags &= ~(int)ConvarFlags::FCVAR_PROTECTED;

		if (base->m_nFlags & (int)ConvarFlags::FCVAR_SPONLY)
			base->m_nFlags &= ~(int)ConvarFlags::FCVAR_SPONLY;

		if (base->m_nFlags & (int)ConvarFlags::FCVAR_HIDDEN)
			base->m_nFlags &= ~(int)ConvarFlags::FCVAR_HIDDEN;

		if (base->m_nFlags & (int)ConvarFlags::FCVAR_DEVELOPMENTONLY)
			base->m_nFlags &= ~(int)ConvarFlags::FCVAR_DEVELOPMENTONLY;

		if (base->m_nFlags & (int)ConvarFlags::FCVAR_NOT_CONNECTED)
			base->m_nFlags &= ~(int)ConvarFlags::FCVAR_NOT_CONNECTED;

		if (ConVar *c = dynamic_cast<ConVar *>(base))
		{
			c->m_bHasMax = false;
			c->m_bHasMin = false;
		}

		base = base->m_pNext;
		count++;
	}

	gInts.Engine->ClientCmd_Unrestricted("echo Unprotected  cvars\n");
	

	return 0;
}
bool CUtil::IsVisible(void* pLocal, void* pEntity, Vector vStart, Vector vEnd)
{
	trace_t Trace;
	Ray_t Ray;			 // the future of variable naming
	CTraceFilter Filter;

	Filter.pSkip = pLocal;

	Ray.Init(vStart, vEnd);

	gInts.EngineTrace->TraceRay(Ray, MASK_SHOT, &Filter, &Trace);

	return (Trace.m_pEnt == pEntity);
}
bool CUtil::AssumeVis(CBaseEntity* pLocal, Vector vStart, Vector vEnd)
{
	trace_t Trace;
	Ray_t Ray;
	CTraceFilter Filter;
	
	Filter.pSkip = pLocal;

	Ray.Init(vStart, vEnd);

	gInts.EngineTrace->TraceRay(Ray, MASK_SHOT, &Filter, &Trace);

	// If we aren't hitting a wall, then we're fine.
	if (!Trace.DidHit())
		return true;
	return false;
}
bool CUtil::IsVisible2(Vector& vecAbsStart, Vector& vecAbsEnd, CBaseEntity* pLocal, CBaseEntity* pEntity)
{
	trace_t tr;
	Ray_t ray;
	CTraceFilter filter;

	ray.Init(vecAbsStart, vecAbsEnd);
	gInts.EngineTrace->TraceRay(ray, 0x200400B, &filter, &tr);

	if (tr.m_pEnt == NULL)
		if (tr.fraction == 1.0f)
			return true;

	return (tr.m_pEnt->GetIndex() == pEntity->GetIndex());
}
bool CUtil::IsKeyPressed(int i)
{
	switch (i)
	{
	case 0: //None
		return true;
	case 1: //Mouse 1
		return GetAsyncKeyState(VK_LBUTTON);
	case 2:
		return GetAsyncKeyState(VK_RBUTTON);
	case 3:										  //Mouses 1-5
		return GetAsyncKeyState(VK_MBUTTON);
	case 4:
		return GetAsyncKeyState(VK_XBUTTON1);
	case 5:
		return GetAsyncKeyState(VK_XBUTTON2);
	case 6: //Shift
		return GetAsyncKeyState(VK_SHIFT);
	case 7: //Alt
		return GetAsyncKeyState(VK_MENU);
	case 8: //F
		return GetAsyncKeyState(0x46);
	default:
		return false;
	}

	return false;
}
float CUtil::getVectorLength(Vector vector)
{
	return (float)sqrt(pow(vector.x, 2) + pow(vector.y, 2) + pow(vector.z, 2));
}
float CUtil::getVectorDistance(Vector vec1, Vector vec2)
{
	return getVectorLength(vec2 - vec1);
}
bool CUtil::IsHeadshotWeapon(CBaseEntity* pLocal, CBaseCombatWeapon* pWep)
{
	if (pWep->GetItemDefinitionIndex() == WPN_Huntsman || pWep->GetItemDefinitionIndex() == WPN_CompoundBow)
		return true;

	if (pWep->GetSlot() == 0 && pLocal->GetClassNum() == TF2_Sniper && pLocal->GetCond() & TFCond_Zoomed)
		if (pWep->GetItemDefinitionIndex() != WPN_Huntsman && pWep->GetItemDefinitionIndex() != WPN_CompoundBow)
			return true;

	if (pWep->GetItemDefinitionIndex() == WPN_Ambassador || pWep->GetItemDefinitionIndex() == WPN_FestiveAmbassador)
		return true;

	return false;
}

PVOID CUtil::InitKeyValue() //Credits f1ssion
{
	typedef PDWORD(__cdecl* Init_t)(int);
	static DWORD dwInitLocation = gSignatures.GetClientSignature("E8 ? ? ? ? 83 C4 14 85 C0 74 10 68") + 0x1;
	static DWORD dwInit = ((*(PDWORD)(dwInitLocation)) + dwInitLocation + 4);
	static Init_t InitKeyValues = (Init_t)dwInit;
	return InitKeyValues(32);
}
void CUtil::ReplaceString(std::string& input, const std::string& what, const std::string& with_what)
{
	size_t index;
	index = input.find(what);
	while (index != std::string::npos)
	{
		input.replace(index, what.size(), with_what);
		index = input.find(what, index + with_what.size());
	}
}

Vector CUtil::EstimateAbsVelocity(CBaseEntity *ent)
{
	typedef void(__thiscall * EstimateAbsVelocityFn)(CBaseEntity *, Vector &);

	static DWORD dwFn = gSignatures.GetClientSignature("E8 ? ? ? ? F3 0F 10 4D ? 8D 85 ? ? ? ? F3 0F 10 45 ? F3 0F 59 C9 56 F3 0F 59 C0 F3 0F 58 C8 0F 2F 0D ? ? ? ? 76 07") + 0x1;

	static DWORD dwEstimate = ((*(PDWORD)(dwFn)) + dwFn + 4);

	EstimateAbsVelocityFn vel = (EstimateAbsVelocityFn)dwEstimate;

	Vector v;
	vel(ent, v);

	return v;
}

void CUtil::SilentMovementFix(CUserCmd *pUserCmd, Vector angles)
{
	Vector vecSilent(pUserCmd->forwardmove, pUserCmd->sidemove, pUserCmd->upmove);
	float flSpeed = sqrt(vecSilent.x * vecSilent.x + vecSilent.y * vecSilent.y);
	Vector angMove;
	VectorAngles(vecSilent, angMove);
	float flYaw = DEG2RAD(angles.y - pUserCmd->viewangles.y + angMove.y);
	pUserCmd->forwardmove = cos(flYaw) * flSpeed;
	pUserCmd->sidemove = sin(flYaw) * flSpeed;
	//pUserCmd->viewangles = angles;
}

bool CUtil::IsThirdPerson(CBaseEntity * ent)
{
	if (*(bool*)(ent + nPlayer.m_nForceTauntCam))
		return true;

	if (gCvars.misc_thirdperson)
		return true;

	//ConVar* thirdperson = gInts.Cvar->FindVar("thirdperson");
	//if (thirdperson->g)
	//	return true;

	return false;
}

bool CUtil::CanShoot(CBaseEntity * pLocal, CBaseCombatWeapon * pWep)
{
	return pWep->flNextPrimaryAttack() <= (float)(pLocal->GetTickBase() * gInts.g_pGlobals->interval_per_tick);
}

