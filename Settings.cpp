#include "Settings.h"

#include "Aimbot.h"
#include "ESP.h"
#include "Misc.h"
#include "Triggerbot.h"
#include "Radar.h"
#include "CTimeShift.h"
#include "CAntiAim.h"

GSettings gSettings;

void GSettings::GetVal(Json::Value &config, int* setting)
{
	if (config.isNull())
		return;

	*setting = config.asInt();
}

void GSettings::GetVal(Json::Value &config, bool* setting)
{
	if (config.isNull())
		return;

	*setting = config.asBool();
}

void GSettings::GetVal(Json::Value &config, float* setting)
{
	if (config.isNull())
		return;

	*setting = config.asFloat();
}
/*
void GetVal(Json::Value &config, ImColor* setting)
{
if (config.isNull())
return;

GetVal(config["r"], &setting->Value.x);
GetVal(config["g"], &setting->Value.y);
GetVal(config["b"], &setting->Value.z);
GetVal(config["a"], &setting->Value.w);
}
*/
void GSettings::GetVal(Json::Value &config, char** setting)
{
	if (config.isNull())
		return;

	*setting = strdup(config.asCString());
}

void GSettings::GetVal(Json::Value &config, char* setting)
{
	if (config.isNull())
		return;

	strcpy(setting, config.asCString());
}

//void GSettings::GetVal(Json::Value &config, byte* setting)
//{
//	if (config.isNull())
//		return;
//
//	strcpy((char*)setting, config.asCString());
//}

template <typename Ord, Ord(*lookupFunction)(std::string)>
void GetOrdinal(Json::Value& config, Ord* setting)
{
	if (config.isNull())
		return;

	Ord value;
	if (config.isString())
		value = lookupFunction(config.asString());
	else
		value = (Ord)config.asInt();

	*setting = value;
}
inline bool GSettings::exists_test3(const std::string& name)
{
	struct stat buffer;
	return (stat(name.c_str(), &buffer) == 0);
}

bool GSettings::LoadSettings(const char* path)
{
	if (!exists_test3(path))
		return false;
	Json::Value settings;
	std::ifstream configDoc(path, std::ifstream::binary);
	configDoc >> settings;

   /*
	*	Known problem: Keys cannot be loaded (game crashes), even if casted as char pointers,
	*	no clue why, doesn't really matter anyways as binding keys takes like 2 seconds, but it
	*	would be nice for them to save anyways.
	*/

	auto section = "Aimbot";
	GetVal(settings[section]["Active"], &gAim.aimbot_active.value);
	GetVal(settings[section]["FOV"], &gAim.aimbot_fov.value);
	//GetVal(settings[section]["Key"], (char*)&gAim.aimbot_key.key);
	GetVal(settings[section]["Hitscan"], &gAim.aimbot_hitscan.value);
	//GetVal(settings[section]["Projectile"], &gCvars.aimbot_projectiles);
	GetVal(settings[section]["Hitbox"], (int*)&gAim.aimbot_position.value);
	GetVal(settings[section]["Smooth"], &gAim.aimbot_slowaim.value);
	GetVal(settings[section]["Smooth amt"], &gAim.aimbot_slow_amt.value);
	GetVal(settings[section]["Autoshoot"], &gAim.aimbot_autoshoot.value);
	GetVal(settings[section]["Silent"], &gAim.aimbot_silent.value);
	GetVal(settings[section]["Minigun Toggle"], &gAim.aimbot_minigun_toggle.value);
	GetVal(settings[section]["Ignore Cloak"], &gAim.aimbot_ignore_cloak.value);
	GetVal(settings[section]["Advanced Feet Aim"], &gAim.aimbot_adv_feet_aim.value);


	section = "Triggerbot";
	GetVal(settings[section]["Active"], &gTrigger.triggerbot_active.value);
	//GetVal(settings[section]["Key"], (char*)&gTrigger.triggerbot_key.key);
	GetVal(settings[section]["Head Only"], &gTrigger.triggerbot_headonly.value);

	section = "Visuals";

	GetVal(settings[section]["Active"], &gESP.esp_enabled.value);
	GetVal(settings[section]["Enemy Only"], &gESP.esp_enemy_only.value);
	GetVal(settings[section]["Visible Only"], &gESP.esp_visible_only.value);
	GetVal(settings[section]["Visibility Check"], &gESP.esp_vis_check.value);
	GetVal(settings[section]["Name"], (int*)&gESP.esp_name.value);
	GetVal(settings[section]["Class"], &gESP.esp_class.value);
	GetVal(settings[section]["Boxes"], &gESP.esp_boxes.value);
	GetVal(settings[section]["Bones"], (int*)&gESP.esp_bones.value);
	GetVal(settings[section]["Health"], (int*)&gESP.esp_health.value);
	GetVal(settings[section]["Snaplines"], (int*)&gESP.esp_snaplines.value);
	GetVal(settings[section]["Anti-Disguise"], &gESP.esp_disguise.value);
	GetVal(settings[section]["Buildings"], &gCvars.esp_buildings);
	GetVal(settings[section]["Items"], &gESP.esp_items.value);
	GetVal(settings[section]["No Zoom"], (int*)&gESP.visual_nozoom.value);
	//GetVal(settings[section]["No Scope Crosshair"], &gCvars.visual_noscope_crosshair);
	GetVal(settings[section]["Rainbow Menu"], &gCvars.visual_rainbow);
	GetVal(settings[section]["Color Style"], (int*)&gESP.visual_colors.value);
	GetVal(settings[section]["Color Menu"], &gCvars.visual_menu_hue);
	GetVal(settings[section]["Color Red"], &gCvars.visual_red_hue);
	GetVal(settings[section]["Color Blu"], &gCvars.visual_blu_hue);
	GetVal(settings[section]["Color Aim"], &gCvars.visual_aim_hue);
	GetVal(settings[section]["Color Friend"], &gCvars.visual_friend_hue);
	GetVal(settings[section]["Color Rage"], &gCvars.visual_rage_hue);


	section = "Radar";
	GetVal(settings[section]["Active"], &gRadar.radar_active.value);
	GetVal(settings[section]["Enemy Only"], &gRadar.radar_enemy_only.value);
	GetVal(settings[section]["Radar X"], &gRadar.radar_x.value);
	GetVal(settings[section]["Radar Y"], &gRadar.radar_y.value);
	GetVal(settings[section]["Radar Size"], &gRadar.radar_size.value);
	GetVal(settings[section]["Radar Range"], &gRadar.radar_range.value);
	GetVal(settings[section]["Radar Alpha"], &gRadar.radar_alpha.value);
	GetVal(settings[section]["Radar Style"], (int*)&gRadar.radar_style.value);


	section = "HvH";
	GetVal(settings[section]["Anti-Aim"], &gAntiAim.aa_active.value);
	GetVal(settings[section]["AA Pitch Fake"], (int*)&gAntiAim.aa_pitch_fake.value);
	GetVal(settings[section]["AA Pitch Real"], (int*)&gAntiAim.aa_pitch_real.value);
	GetVal(settings[section]["AA Yaw"], (int*)&gAntiAim.aa_yaw.value);
	GetVal(settings[section]["AA Spin Speed"], &gAntiAim.aa_spin_speed.value);
	GetVal(settings[section]["Resolver"], (int*)&gAntiAim.aa_resolver.value);


	section = "Misc";
	GetVal(settings[section]["Bunny Hop"], &gMisc.misc_bhop.value);
	GetVal(settings[section]["Auto Strafe"], &gMisc.misc_astrafe.value);
	GetVal(settings[section]["Third Person"], &gMisc.misc_tp.value);
	GetVal(settings[section]["Cheats bypass"], &gMisc.misc_cheats.value);
	GetVal(settings[section]["Pure bypass"], &gMisc.misc_pure.value);
	GetVal(settings[section]["Anti-SMAC"], &gMisc.misc_smac.value);
	GetVal(settings[section]["Noisemaker Spam"], &gMisc.misc_noisem.value);
	GetVal(settings[section]["Tournament Spam"], &gMisc.misc_tourn.value);
	GetVal(settings[section]["Time Shift"], &gMisc.misc_ts.value);
	//GetVal(settings[section]["Time Shift Key"], (char*)&gMisc.misc_ts_key.key);
	GetVal(settings[section]["Time Shift Value"], &gMisc.misc_ts_value.value);
	GetVal(settings[section]["Insta Decloak"], &gMisc.misc_decloak.value);
	GetVal(settings[section]["Double Shoot"], &gMisc.misc_dshoot.value);
	GetVal(settings[section]["Sticky Spam"], &gMisc.misc_sticky.value);
	//GetVal(settings[section]["Lag Exploit"], &gMisc.misc_lag_exploit);
	//GetVal(settings[section]["Lag Exploit Key"], &gMisc.misc_lag_exploit_key);
	GetVal(settings[section]["Chat Spam"], &gMisc.misc_chatspam.value);
	GetVal(settings[section]["Newline Amt"], &gMisc.misc_newlines.value);
	//GetVal(settings[section]["Backtracking"], &gMisc.misc_backtracking);
	//GetVal(settings[section]["Backtracking Ticks"], &gMisc.bt_maxticks);
	return true;
}

bool GSettings::SaveSettings(const char* path)
{
	try
	{
		Json::Value settings;
		Json::StyledWriter styledWriter;

		//Settings
		auto section = "Aimbot";
		settings[section]["Active"] = gAim.aimbot_active.value;
		settings[section]["FOV"] = gAim.aimbot_fov.value;
		settings[section]["Key"] = gAim.aimbot_key.key;
		settings[section]["Hitscan"] = gAim.aimbot_hitscan.value;
		//settings[section]["Projectile"]= &gCvars.aimbot_projectiles);
		settings[section]["Hitbox"] = (int)gAim.aimbot_position.value;
		settings[section]["Smooth"] = gAim.aimbot_slowaim.value;
		settings[section]["Smooth amt"] = gAim.aimbot_slow_amt.value;
		settings[section]["Autoshoot"] = gAim.aimbot_autoshoot.value;
		settings[section]["Silent"] = gAim.aimbot_silent.value;
		settings[section]["Minigun Toggle"] = gAim.aimbot_minigun_toggle.value;
		settings[section]["Ignore Cloak"]= gAim.aimbot_ignore_cloak.value;
		settings[section]["Advanced Feet Aim"]= gAim.aimbot_adv_feet_aim.value;

		section = "Triggerbot";
		settings[section]["Active"]= gTrigger.triggerbot_active.value;
		settings[section]["Key"]= (char)gTrigger.triggerbot_key.key;
		settings[section]["Head Only"]= gTrigger.triggerbot_headonly.value;

		section = "Visuals";
		settings[section]["Active"]= gESP.esp_enabled.value;
		settings[section]["Enemy Only"]= gESP.esp_enemy_only.value;
		settings[section]["Visible Only"]= gESP.esp_visible_only.value;
		settings[section]["Visibility Check"]= gESP.esp_vis_check.value;
		settings[section]["Name"]= gESP.esp_name.value;
		settings[section]["Class"]= gESP.esp_class.value;
		settings[section]["Boxes"]= gESP.esp_boxes.value;
		settings[section]["Bones"]= (int)gESP.esp_bones.value;
		settings[section]["Health"]= (int)gESP.esp_health.value;
		settings[section]["Snaplines"]= (int)gESP.esp_snaplines.value;
		settings[section]["Anti-Disguise"]= gESP.esp_disguise.value;
		settings[section]["Buildings"]= gCvars.esp_buildings;
		settings[section]["Items"]= gESP.esp_items.value;
		settings[section]["No Zoom"]= (int)gESP.visual_nozoom.value;
		//settings[section]n]["No Scope Crosshair"]= &gCvars.visual_noscope_crosshair);
		settings[section]["Rainbow Menu"]= gCvars.visual_rainbow;
		settings[section]["Color Style"]= (int)gESP.visual_colors.value;
		settings[section]["Color Menu"]= gCvars.visual_menu_hue;
		settings[section]["Color Red"]= gCvars.visual_red_hue;
		settings[section]["Color Blu"]= gCvars.visual_blu_hue;
		settings[section]["Color Aim"]= gCvars.visual_aim_hue;
		settings[section]["Color Friend"]= gCvars.visual_friend_hue;
		settings[section]["Color Rage"]= gCvars.visual_rage_hue;

		section = "Radar";
		settings[section]["Active"]= gRadar.radar_active.value;
		settings[section]["Enemy Only"]= gRadar.radar_enemy_only.value;
		settings[section]["Radar X"]= gRadar.radar_x.value;
		settings[section]["Radar Y"]= gRadar.radar_y.value;
		settings[section]["Radar Size"]= gRadar.radar_size.value;
		settings[section]["Radar Range"]= gRadar.radar_range.value;
		settings[section]["Radar Alpha"]= gRadar.radar_alpha.value;
		settings[section]["Radar Style"]= (int)gRadar.radar_style.value;

		section = "HvH";
		settings[section]["Anti-Aim"]= gAntiAim.aa_active.value;
		settings[section]["AA Pitch Fake"] = (int)gAntiAim.aa_pitch_fake.value;
		settings[section]["AA Pitch Real"] = (int)gAntiAim.aa_pitch_real.value;
		settings[section]["AA Yaw"]= (int)gAntiAim.aa_yaw.value;
		settings[section]["AA Spin Speed"]= gAntiAim.aa_spin_speed.value;
		settings[section]["Resolver"]= (int)gAntiAim.aa_resolver.value;

		section = "Misc";
		settings[section]["Bunny Hop"]= gMisc.misc_bhop.value;
		settings[section]["Auto Strafe"]= gMisc.misc_astrafe.value;
		settings[section]["Third Person"]= gMisc.misc_tp.value;
		settings[section]["Cheats bypass"]= gMisc.misc_cheats.value;
		settings[section]["Pure bypass"]= gMisc.misc_pure.value;
		settings[section]["Anti-SMAC"]= gMisc.misc_smac.value;
		settings[section]["Noisemaker Spam"]= gMisc.misc_noisem.value;
		settings[section]["Tournament Spam"]= gMisc.misc_tourn.value;
		settings[section]["Time Shift"]= gMisc.misc_ts.value;
		//settings[section]["Time Shift Key"]= (char)gMisc.misc_ts_key.key;
		settings[section]["Time Shift Value"]= gMisc.misc_ts_value.value;
		settings[section]["Insta Decloak"]= gMisc.misc_decloak.value;
		settings[section]["Double Shoot"]= gMisc.misc_dshoot.value;
		settings[section]["Sticky Spam"]= gMisc.misc_sticky.value;
		//settings[section]n]["Lag Exploit"]= &gMisc.misc_lag_exploit);
		//settings[section]n]["Lag Exploit Key"]= &gMisc.misc_lag_exploit_key);
		settings[section]["Chat Spam"]= gMisc.misc_chatspam.value;
		settings[section]["Newline Amt"]= gMisc.misc_newlines.value;
		//settings[section]n]["Backtracking"]= &gMisc.misc_backtracking);
		//settings[section]n]["Backtracking Ticks"]= &gMisc.bt_maxticks);

		std::ofstream(path) << styledWriter.write(settings);
		return true;
	}
	catch (...)
	{
		return false;
	}
}