#include "AutoAirblast.h"
#include "Panels.h"
#include "Aimbot.h"
CAutoAirblast gAutoAirblast;
#include "gayniggers.h"

void CAutoAirblast::Run(CBaseEntity* pLocal, CUserCmd* pCommand)
{
	if (!gCvars.triggershoot_airblast_enable) return;

	if (pLocal->GetLifeState() != LIFE_ALIVE) return;

	auto pClass = pLocal->GetActiveWeapon()->GetItemDefinitionIndex();
	if (pClass == pyroweapons::WPN_Backburner
		|| pClass == pyroweapons::WPN_Degreaser
		|| pClass == pyroweapons::WPN_FestiveBackburner
		|| pClass == pyroweapons::WPN_FestiveFlamethrower
		|| pClass == pyroweapons::WPN_Flamethrower
		|| pClass == pyroweapons::WPN_Rainblower
		|| pClass == engineerweapons::WPN_ShortCircut)
	{

		/*auto pWep = pLocal->GetActiveWeapon();
		if(pLocal->szGetClass() == "Pyro")
		{
		if(pWep->GetItemDefinitionIndex() == pyroweapons::WPN_Phlogistinator) return;
		if(pWep->GetSlot() != 0) return;
		}
		else if(pLocal->szGetClass() == "Engineer")
		{
		if(pWep->GetItemDefinitionIndex() != engineerweapons::WPN_ShortCircut) return;
		if(pWep->GetSlot() != 2) return;
		}*/

		for (int i = 1; i <= gInts.EntList->GetHighestEntityIndex(); i++)
		{
			CBaseEntity* pEnt = gInts.EntList->GetClientEntity(i);
			if (!pEnt)
				continue;

			if (pEnt->GetTeamNum() == pLocal->GetTeamNum()) continue;

			CBaseCombatWeapon* wep = pLocal->GetActiveWeapon();
			if (!wep) continue;

			if (!strcmp(pEnt->GetClientClass()->chName, "CTFProjectile_SentryRocket")
				|| !strcmp(pEnt->GetClientClass()->chName, "CTFProjectile_Arrow")
				|| !strcmp(pEnt->GetClientClass()->chName, "CTFProjectile_HealingBolt")
				|| !strcmp(pEnt->GetClientClass()->chName, "CTFProjectile_EnergyBall")
				|| !strcmp(pEnt->GetClientClass()->chName, "CTFProjectile_Flare")
				|| !strcmp(pEnt->GetClientClass()->chName, "CTFProjectile_Jar")
				|| !strcmp(pEnt->GetClientClass()->chName, "CTFProjectile_JarMilk")
				|| !strcmp(pEnt->GetClientClass()->chName, "CTFProjectile_Cleaver")
				|| !strcmp(pEnt->GetClientClass()->chName, "CTFProjectile_Throwable")
				|| !strcmp(pEnt->GetClientClass()->chName, "CTFProjectile_Rocket")
				|| !strcmp(pEnt->GetClientClass()->chName, "CTFGrenadePipebombProjectile")
				|| !strcmp(pEnt->GetClientClass()->chName, "CTFProjectile_ThrowableRepel")
				|| !strcmp(pEnt->GetClientClass()->chName, "CTFProjectile_ThrowableBrick")
				|| !strcmp(pEnt->GetClientClass()->chName, "CTFProjectile_ThrowableBreadMonster"))
			{

				Vector fClientEyePosition = pLocal->GetEyePosition();

				Vector entityLocation;
				pEnt->GetWorldSpaceCenter(entityLocation);

				Vector vecSpeed = Util->EstimateAbsVelocity(pEnt);

				nhStored.angles = pCommand->viewangles;

				INetChannel *pNet = (INetChannel*)gInts.Engine->GetNetChannelInfo();
				if (pNet)
				{
					float lInterp = gInts.Cvar->FindVar("cl_interp_ratio")->GetFloat() / gInts.Cvar->FindVar("cl_updaterate")->GetFloat() * gInts.g_pGlobals->interval_per_tick;
					float scale = lInterp + pNet->GetAvgLatency(FLOW_OUTGOING) + pNet->GetAvgLatency(FLOW_INCOMING);
					VectorMA(entityLocation, scale, vecSpeed, entityLocation);
				}
				float distance = Util->getVectorDistance(fClientEyePosition, entityLocation);
				if (distance > gCvars.triggershoot_airblast_distance) // If distance to rocket is more than distance in menu then return
					continue;

				/*if (!Util->visi(pEnt))*/ continue;

				//if (gCvars.triggershoot_airblast_fov != 0.0 && gCvars.triggershoot_airblast_fov != 180.0)
				//	if (gAimbot.GetFOV(pLocal->GetAbsAngles(), fClientEyePosition, entityLocation) >= gCvars.triggershoot_airblast_fov)
				//		return;

				if (distance > gCvars.triggershoot_airblast_distance) // If distance to rocket is more than distance in menu then return
					continue;

				Vector tr = (entityLocation - pLocal->GetEyePosition());
				Vector angles;

				VectorAngles(tr, angles);
				if (gCvars.triggershoot_airblast_silent)
				{
					ClampAngle(angles);
					Util->SilentMovementFix(pCommand, angles);

					pCommand->viewangles = angles;
					AngleNormalize(angles);
				}
				else
				{
					pCommand->viewangles = angles;
					gInts.Engine->SetViewAngles(pCommand->viewangles);
				}

			//	appLog.AddLog("[AUTO-AIRBLAST] Airblasting rocket at %f distance\n", distance);

				pCommand->buttons |= IN_ATTACK2;

			}
		}
	}
}

void CAutoAirblast::FireGameEvent(IGameEvent* event)
{
	if (gCvars.triggershoot_airblast_idk /*> 0*/ && gCvars.triggershoot_airblast_enable)
	{
		//CBaseEntity* pLocal = GetBaseEntity(me);

		/*	if(pLocal)
		{*/
		int vid = event->GetInt("userid");
		if (gInts.Engine->GetPlayerForUserID(vid) != gInts.Engine->GetLocalPlayer()) return;


		/*int playerWhoDeflected = event->GetInt("userid", -1);

		if(!playerWhoDeflected) return;

		if(playerWhoDeflected == me)
		{*/
		gInts.Engine->ClientCmd("say fuck");
		//}

	}
}

//}

void CAutoAirblast::InitEvent()
{
	gInts.EventManager->AddListener(this, "object_deflected", false);
}