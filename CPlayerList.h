#pragma once
#include "SDK.h"
#include <vector>
#include <Windows.h>

enum class plist_e_mb
{
	null,
	lclick,
	rclick,
	ldown,
	rdown
};

class CPlayerList
{
public:
	bool enabled = false;

	void draw();
	void createGUI();
	WNDPROC windowProc;

	plist_e_mb mb = plist_e_mb::null;

private:
	void getInput();
	bool mouseOver(int x, int y, int w, int h);
	POINT mouse{ 0, 0 }, pmouse{ 0, 0 }, pPlayerlistPos{ 1000, 200 };

	// Just a quick and sloppy control struct
	enum class e_type
	{
		null,
		integer
	};
	struct Item
	{
		const char* name;
		e_type type = e_type::null;

		bool* bValue = nullptr;
		int* iValue = nullptr;
		int pIndex = 0;
		int min = 0, max = 1, step = 1;
	};
	vector<Item> itemlist;

	void addInt(const char* name, int &value, int min, int max, int step);
	void addPlayer(int pIndex, int &value, int min, int max, int step);
};
extern CPlayerList gPlayerList;
LRESULT __stdcall Hooked_WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);