#pragma once
#include "SDK.h"

struct BacktrackData
{
	float tickcount;
	Vector hitboxpos;
};

class CBacktracking
{
public:
	void Run(CUserCmd* cmd);

private:

};

extern BacktrackData btHeadPositions[24][14];
extern CBacktracking gBacktrack;