#include "SDK.h"
#include "Panels.h"
#include "CDrawManager.h"
#include "Util.h"
#include "ESP.h"
#include "CPlayerList.h"
#include "InfoPanel.h"
#include "Radar.h"
#include "NetVars.h"
#include "GUI.h"
#include "Menu.h"
#include "Misc.h"
CScreenSize gScreenSize;
bool bSkyNeedsUpdate = true;
//===================================================================================

string Switch(float item) 
{
	return item ? "On" : "Off";
}

void DrawGayMenu() 
{
	gDraw.DrawString(5, 5, Color(255, 0, 255, 255), "m4rida.jar v4.2.0 | MarkTF2 | " __TIMESTAMP__);
	gDraw.DrawString(5, 5 + 10, Color(0, 255, 255, 255), ("Aimbot: " + Switch(gCvars.aimbot_active)).c_str()); //god
	gDraw.DrawString(5, 5 + 20, Color(0, 255, 255, 255), ("Trigger: " + Switch(gCvars.triggerbot_active)).c_str());
	gDraw.DrawString(5, 5 + 30, Color(0, 255, 255, 255), ("ESP: " + Switch(gCvars.esp_active)).c_str());
	gDraw.DrawString(5, 5 + 40, Color(0, 255, 255, 255), ("Bunny Hop: " + Switch(gCvars.misc_bunnyhop)).c_str());
	gDraw.DrawString(5, 5 + 50, Color(0, 255, 255, 255), ("Cvar Bypass: " + Switch(gCvars.misc_sv_cheats)).c_str());
	gDraw.DrawString(5, 5 + 60, Color(0, 255, 255, 255), ("Game events: " + Switch(gCvars.misc_autostrafe)).c_str());
	gDraw.DrawString(5, 5 + 70, Color(0, 255, 255, 255), ("Third Person: " + Switch(gCvars.misc_thirdperson)).c_str());
	gDraw.DrawString(5, 5 + 80, Color(0, 255, 255, 255), ("Anti-Aim: " + Switch(gCvars.hvh_aa_active)).c_str());
	gDraw.DrawString(5, 5 + 90, Color(0, 255, 255, 255), ("Chat Spam: " + Switch(gCvars.misc_autostrafe)).c_str());
	gDraw.DrawString(5, 5 + 100, Color(0, 255, 255, 255), ("Tournament Spam: " + Switch(gCvars.misc_tournament_spam)).c_str());
}

void __fastcall Hooked_PaintTraverse( PVOID pPanels, int edx, unsigned int vguiPanel, bool forceRepaint, bool allowForce )
{
	
	try
	{
		if (!strcmp("HudScope", gInts.Panels->GetName(vguiPanel)) && (gESP.visual_nozoom.value == 1 || gESP.visual_nozoom.value == 3))
		{
			return;
		}

		VMTManager& hook = VMTManager::GetHook(pPanels);
		hook.GetMethod<void(__thiscall*)(PVOID, unsigned int, bool, bool)>(gOffsets.iPaintTraverseOffset)(pPanels, vguiPanel, forceRepaint, allowForce); //Call the original.

		static unsigned int vguiFocusOverlayPanel;

		if (vguiFocusOverlayPanel == NULL)
		{											//FocusOverlayPanel
			const char* szName = gInts.Panels->GetName(vguiPanel);
			if (szName[0] == 'F' && szName[5] == 'O' &&  szName[12] == 'P')
			{
				vguiFocusOverlayPanel = vguiPanel;
				Intro();
			}
		}

		if (vguiFocusOverlayPanel)
			gInts.Panels->SetTopmostPopup(vguiFocusOverlayPanel, true);

		if (vguiFocusOverlayPanel == vguiPanel)
		{
			//Resolution fix, so this can work in Fullscreen
			gInts.Engine->GetScreenSize(gCvars.iWidth, gCvars.iHeight);
			if (gScreenSize.iScreenWidth != gCvars.iWidth || gScreenSize.iScreenHeight != gCvars.iHeight)
				gInts.Engine->GetScreenSize(gScreenSize.iScreenWidth, gScreenSize.iScreenHeight);

			//if( gInts.Engine->IsDrawingLoadingImage() /*gInts.Engine->IsDrawingLoadingImage() || !gInts.Engine->IsInGame( ) || !gInts.Engine->IsConnected() || gInts.Engine->Con_IsVisible( ) */)
			//{
			//	if (gCvars.m4rida_menu)
			//		DrawGayMenu();
			//	return; //No need to draw the rest.
			//}

			//Pure bypass
			if (gCvars.misc_sv_pure && !gInts.Engine->IsInGame())
			{
				static DWORD dwPure = *(PDWORD)(gSignatures.GetEngineSignature("A1 ? ? ? ? 56 33 F6 85 C0") + 0x1); //thanks wheaties
				if (*(PDWORD)dwPure)
					*(PDWORD)dwPure = NULL;
			}

			if (gCvars.visual_sky_changer && bSkyNeedsUpdate && gInts.Engine->IsInGame())
			{
				///TODO: Make it so disabling it actually returns the original sky, and add more skyboxes
				if (gInts.Cvar->FindVar("sv_skyname")->GetString() != "sky_night_01")
				{
					typedef bool(_cdecl* LoadNamedSkysFn)(const char*);
					static LoadNamedSkysFn LoadSkys = (LoadNamedSkysFn)gSignatures.GetEngineSignature("55 8B EC 81 EC ? ? ? ? 8B 0D ? ? ? ? 53 56 57 8B 01 C7 45");
					if (!LoadSkys) gInts.Engine->ClientCmd("echo LoadSky signature unsuccessful");
					else LoadSkys("sky_night_01");
				}
				bSkyNeedsUpdate = false;
			}

			if (gMisc.spec_list.value)
			{
				CBaseEntity* pLocal = GetBaseEntity(me);
				int gamers = 1;
				for (int i = 1; i <= gInts.Engine->GetMaxClients(); i++)
				{
					CBaseEntity* pEntity = GetBaseEntity(i);
					if (!pEntity) continue;
					if (pEntity == pLocal) continue;
					if (pEntity->IsDormant()) continue;
					if ((pEntity->m_hObserverTarget() & 0xFFF) != pLocal->GetIndex()) continue;
					if (pEntity->m_iObserverMode() < 4) continue;
					player_info_t pInfo;
					if (!gInts.Engine->GetPlayerInfo(pEntity->GetIndex(), &pInfo)) continue;

					char* observermode = "N/A";
					Color clr;
					switch (pEntity->m_iObserverMode())
					{
					case 4:
						observermode = "Firstperson";
						clr = Color::Red();
						break;
					case 5:
						observermode = "Thirdperson";
						clr = Color::Green();
						break;
					case 7:
						observermode = "Freecam";
						clr = Color::White();
						break;
					}

					char str[255];
					strcpy(str, "(");
					strcat(str, observermode);
					strcat(str, ") - ");
					strcat(str, pInfo.name);
					puts(str);

					gDraw.DrawStringCenterFont(gScreenSize.iScreenWidth / 2, 40 + (14 * gamers), clr, str, gDraw.Consolas16);

					gamers++;
				}
				gamers = 1;
			}

//#ifndef DEV_BUILD
//			if (!gCvars.m4rida_menu)
//				gDrawManager.DrawString(5,5, rainbowCurrent(), "Iridium");
//#else
//			//if (!gCvars.m4rida_menu)
//			//	gDrawManager.DrawString(5, 5, rainbowCurrent(), "m4rida.jar");
//#endif

			//if (gCvars.misc_lag_exploit && Util->IsKeyPressed(gCvars.misc_lag_exploit_key))
			//	gDraw.DrawString((gScreenSize.iScreenWidth / 2) - 40, (gScreenSize.iScreenHeight / 2) - 10, rainbowCurrent(), "LAG EXPLOIT ON");

			if (gCvars.m4rida_menu)
				DrawGayMenu();

			if (gMisc.misc_icontest.value)
			{
				static int h = 32;
				static int w = 32;
				int y = 10;
				for (int i = 0; i < gDraw.g_vRankFiles.size(); i++) {
					gDraw.DrawTexturedRect(gDraw.g_vRankTextures[i], 20, y += h + 10, w, h);
				}
			}

			//
			gESP.Run();
			gRadar.Run();
			if (!gCvars.cade_menu)
			gGUI.Draw();
			//

			if (!gCvars.cade_menu)
			gInts.Panels->SetMouseInputEnabled(vguiPanel, gGUI.bMenuActive);

			if (gCvars.cade_menu)
			{
				gMenu.GetInput();
				gMenu.Draw();
				gInts.Panels->SetMouseInputEnabled(vguiPanel, gMenu.enabled);
				gMenu.EndInput();
			}

			gPlayerList.draw(); //needs to be after menu
			//gInfoPanel.draw();
			//gInts.Panels->SetMouseInputEnabled(vguiPanel, gInfoPanel.enabled);


			//if (gCheatMenu.bMenuActive)
			//{
			//	gCheatMenu.DrawMenu();
			//	gCheatMenu.Render();
			//}
		}
	}
	catch(...)
	{
		Log::Fatal("Failed PaintTraverse");
	}
}
//===================================================================================
void Intro( void )
{
	try
	{
		gDraw.Initialize(); //Initalize the drawing class.

		if (gCvars.cade_menu)
		{
			gMenu.CreateGUI();
			InitTextures();
		}


		gNetVars.Initialize();
		nPlayer.init();
		nWeapon.init();

		//typedef bool(_cdecl* LoadNamedSkysFn)(const char*);
		//static LoadNamedSkysFn LoadSkys = (LoadNamedSkysFn)gSignatures.GetEngineSignature("55 8B EC 81 EC ? ? ? ? 8B 0D ? ? ? ? 53 56 57 8B 01 C7 45");
	}
	catch(...)
	{
		Log::Fatal("Failed Intro");
	}
}