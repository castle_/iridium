#pragma once

class Vector;
class QAngle;
class C_BaseEntity;

#include <Windows.h>


#include "SDK.h"

#include "CDrawManager.h"

#include "QAngle.h"


class CRadar
{

public:
	Checkbox	radar_active		= Checkbox("Active");
	Checkbox	radar_enemy_only	= Checkbox("Enemy Only");
	Slider		radar_x				= Slider("Radar X", 140, 0, 1920, 20); //use another resolution AND have radar on the right if gay
	Slider		radar_y				= Slider("Radar Y", 140, 0, 1080, 20);
	Slider		radar_size			= Slider("Radar Size", 110, 0, 1000, 10);
	Slider		radar_range			= Slider("Radar Range", 800, 0, 10000, 50);
	Slider		radar_alpha			= Slider("Radar Alpha", 165, 0, 255, 15);
	Listbox		radar_style			= Listbox("Style", { "rDS", "Lithium", "LMAOBox", "Null Core" }, 1);
	Checkbox	radar_health		= Checkbox("Health Bars", true);
	Checkbox	radar_portraits		= Checkbox("Class Portraits", true);
	Listbox		radar_icon_bg		= Listbox("Icon Background", { "OFF", "Black", "Team" }, 1);
	Slider		radar_icn_size		= Slider("Icon Size", 24, 16, 64, 2);
	//float m_flx = 201;
	//float m_fly = 201;
	//float m_flw = 200;

	//float m_flx = gCvars.radar_x;
	//float m_fly = gCvars.radar_y;
	//float m_flw = gCvars.radar_size;

	/*	float m_flx = 101;
	float m_fly = 101;
	float m_flw = 200;*/

	void Run();

	void DrawRadarPoint(Vector vOriginX, Vector vOriginY, QAngle qAngle, CBaseEntity * pBaseEntity, Color dwTeamColor, bool entity = false, char * textIfEntity = "");
	void DrawRadarBack(void);
};
extern CRadar gRadar;