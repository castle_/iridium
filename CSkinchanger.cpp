#include "CSkinchanger.h"

CSkinchanger gSkins;
//CAttribute gAttr;
//CAttributeList gAttrList;
//class CAttributeList asdasd;
//class CAttribute dsadsadsa;

void CSkinchanger::FSN(ClientFrameStage_t stage)
{
	if (!gCvars.misc_skinchanger)
		return;

	if (stage != FRAME_NET_UPDATE_POSTDATAUPDATE_START)
		return;

	CBaseEntity* pLocal = gInts.EntList->GetClientEntity(me);

	if (!pLocal || pLocal->GetLifeState() != LIFE_ALIVE)
		return;

	Original_FrameStageNotify(stage);

	//if (pLocal->GetClassNum() == TF2_Sniper)
	//{
	//	size_t handle = *pLocal->MyWeapons();
	//	CBaseCombatWeapon* wep = (CBaseCombatWeapon*)gInts.EntList->GetClientEntityFromHandle(handle);
	//	if (wep)
	//		*wep->pItemDefinitionIndex() = WPN_AWP;
	//}

	size_t* handle = pLocal->MyOtherWeapons();

	for (int i = 0; handle[i]; i++)
	{
		CBaseCombatWeapon* pWeapon = (CBaseCombatWeapon*)gInts.EntList->GetClientEntityFromHandle(handle[i]);

		if (!pWeapon)
			continue;

		int* id = pWeapon->pItemID(); //0x938

		CAttributeList* list = (CAttributeList*)pWeapon + 0x9C0;
		if (!list)
			return;

		switch (*id)
		{
		case WPN_SniperRifle:
			*id = WPN_NewSniperRifle;
			list->AddAttribute(AT_is_australium_item, true);
			list->AddAttribute(AT_loot_rarity, true);
			list->AddAttribute(AT_item_style_override, true);
			break;
		//case WPN_Minigun:
			//*id = WPN_NewMinigun; //CAT minigun!
		//	list->AddAttribute(AT_unusual_effect, WE_cool); //Regular attribute used for unusuals
		//	list->AddAttribute(AT_particle_effect, WE_isotope);  //Static particle attribute, works as well, giving us 2 effects!
		//	list->AddAttribute(AT_sheen, 1); //Team colored sheen
			break;
			//more cases
		}
	}

}

CAttribute::CAttribute()
{
}
