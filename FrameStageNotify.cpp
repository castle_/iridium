#include "FrameStageNotify.h"
#include "SDK.h"
#include "Misc.h"
#include "Aimbot.h"
#include "CSkinchanger.h"

void __fastcall Hooked_FrameStageNotify(void* _this, void* _edx, ClientFrameStage_t curStage)
{
	//auto &hook = VMTManager::GetHook(gInts.Client);

	try
	{
		CBaseEntity* pLocal = GetBaseEntity(me);
		if (!pLocal)
			return Original_FrameStageNotify(curStage);

		gMisc.FSN(pLocal, curStage); //resolver
		gSkins.FSN(curStage); //skinchanger

		Original_FrameStageNotify(curStage);
	}
	catch (...)
	{
		MessageBox(NULL, "Failed Hooked_FrameStageNotify()", "ERROR", MB_OK);
	}
}

void Original_FrameStageNotify(ClientFrameStage_t curStage)
{
	auto &hook = VMTManager::GetHook(gInts.Client);
	hook.GetMethod<FrameStageNotifyFn>((int)gOffsets.iFrameStageNotify)(gInts.Client, curStage);
}