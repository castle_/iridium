#pragma once
#include "SDK.h"

class CUtil
{
public:
	void VectorTransform(const Vector& vSome, const matrix3x4& vMatrix, Vector& vOut);

	bool IsVisible(void* pLocal, void* pEntity, Vector vStart, Vector vEnd);
	bool IsVisible2(Vector & vecAbsStart, Vector & vecAbsEnd, CBaseEntity * pLocal, CBaseEntity * pEntity);
	bool IsKeyPressed(int i);
	DWORD killCvars();
	void ReplaceString(std::string & input, const std::string & what, const std::string & with_what);
	bool IsHeadshotWeapon(CBaseEntity* pLocal, CBaseCombatWeapon* pWep);
	float getVectorLength(Vector vector);
	float getVectorDistance(Vector vec1, Vector vec2);
	bool AssumeVis(CBaseEntity* pLocal, Vector vStart, Vector vEnd);
	bool visi(CBaseEntity * pBaseEntity);
	PVOID InitKeyValue();

	bool DoSwingTrace(trace_t &trace, CBaseEntity* pEntity);

	Vector EstimateAbsVelocity(CBaseEntity *ent);
	void SilentMovementFix(CUserCmd *pUserCmd, Vector angles);
	bool IsThirdPerson(CBaseEntity *ent);
	bool CanShoot(CBaseEntity* pLocal, CBaseCombatWeapon* pWep);
};

extern CUtil* Util;
#define VectorMA(v, s, b, o)    ((o)[0] = (v)[0] + (b)[0] * (s), (o)[1] = (v)[1] + (b)[1] * (s), (o)[2] = (v)[2] + (b)[2] * (s))