#include "GUI.h"

GUI gGUI;

#define keyDown GetAsyncKeyState
#define keyPress(x) keyDown(x) & 1
#define boolStr(boolean) string(boolean? "ON" : "OFF")



void GUI::createGUI()
{
	static bool
		aim = true,
		trigger = false,
		esp = false,
		visual = false,
		radar = false,
		playerlist = false,
		menu = false,
		hvh = false,
		misc = false,
		dev = false; //not used

	if (addTab("Aimbot", aim))
	{
		addBool("Active", gCvars.aimbot_active);
		addEmptyL();
		addInt("Aim FOV", gCvars.aimbot_fov, 0, 180, 1);
		addBool("Draw Aim Fov", gCvars.DrawAimFov);	
		addInt("Aim Key", gCvars.aimbot_key, 0, 8, 1);
		addBool("Aim Hitscan", gCvars.aimbot_hitscan);
		addBool("CanShoot check", gCvars.aimbot_canshoot);
#ifdef DEV_BUILD
		//addBool("Projectile Aimbot", gCvars.aimbot_projectiles); //fuck
#endif
		addInt("Aim Position", gCvars.aimbot_hitbox, 0, 18, 1);
		addBool("Slow Aim", gCvars.aimbot_smooth);
		addInt("Slow Amt", gCvars.aimbot_smooth_amt, 0, 40, 1);
		addBool("Auto Shoot", gCvars.aimbot_autoshoot);
		addBool("Aim Silent", gCvars.aimbot_silent);
		addBool("Minigun Toggle", gCvars.aimbot_minigun_toggle);

		addRightBool("Ignore Cloak", gCvars.aimbot_ignore_cloak);
		addRightBool("Advanced Feet Aim", gCvars.aimbot_advanced_feet_aim);
		addRightBool("Ghetto wait for HS", gCvars.aimbot_wait_for_headshot);
		//addRightBool("Latency Prediction", gCvars.aimbot_latency_pred, alwaystrue); //todo config
	}
	if (addTab("Trigger", trigger))
	{	
		addBool("Trigger Bot", gCvars.triggerbot_active);
		addEmptyL();
		addInt("Trigger Key", gCvars.triggerbot_key, 0, 8, 1);
		addBool("Head Only", gCvars.triggerbot_headonly);

		addRightBool("Trigger Shoot", gCvars.triggershoot_active);
		addEmptyR();
		addRightInt("Trigger Key", gCvars.triggershoot_key, 0, 8, 1);
		addRightBool("Auto-Backstab", gCvars.triggershoot_backstab);
		//addRightBool("Auto Airblast", gCvars.triggershoot_airblast_enable);//todo read if gay /lenny
		//addRightBool("idk ", gCvars.triggershoot_airblast_idk);//todo read if gay /lenny
		//addRightBool("Silent ", gCvars.triggershoot_airblast_silent);//todo read if gay /lenny
		//addRightInt("Airblast Distance", gCvars.triggershoot_airblast_distance, 90, 400, 1);//todo read if gay /lenny
		//addRightInt("Airblast Fov", gCvars.triggershoot_airblast_fov, 0, 180, 1);//todo read if gay /lenny
		// ngl it took me like 4 hours to add auto airblast feelsbadman
	}
	if (addTab("Visuals", visual))
	{
		addBool("Chams", gCvars.visual_chams);
		addEmptyL();
		addInt("Hands", gCvars.visual_hands, 0, 3, 1);
		addBool("Shine Hands", gCvars.visual_arms_shine);
		//addInt("Weapons", gCvars.visual_weapons, 0, 3, 1);
		addBool("Wireframe Players", gCvars.visual_wireframe);
		addBool("Enemy Only", gCvars.visual_enemyonly);
		addEmptyL();
	//	addRightBool("Custom Skybox", gCvars.esp_skybox);
		addRightBool("No Scope", gCvars.visual_noscope);
		addRightBool("No Zoom", gCvars.visual_nozoom); //todo config
		addRightBool("No Scope Crosshair", gCvars.visual_noscope_crosshair);
		addRightBool("Sky Changer", gCvars.visual_sky_changer); //todo config
		addEmptyR();
		addRightBool("Rainbow Menu", gCvars.visual_rainbow);
		addRightInt("Color Style", gCvars.visual_colors, 0, 3, 1);
		addRightInt("Color Menu", gCvars.visual_menu_hue, 0, 255, 10); 
		addRightInt("Color Hands", gCvars.visual_arms_hue, 0, 255, 10);
		addRightInt("Color Red", gCvars.visual_red_hue, 0, 255, 10); 
		addRightInt("Color Blu", gCvars.visual_blu_hue, 0, 255, 10); 
		addRightInt("Color Aim Target", gCvars.visual_aim_hue, 0, 255, 10);
		addRightInt("Color Friend", gCvars.visual_friend_hue, 0, 255, 10);
		addRightInt("Color Rage", gCvars.visual_rage_hue, 0, 255, 10); 
	}
	if (addTab("ESP", esp))
	{
		addBool("Active", gCvars.esp_active);
		addEmptyL();
		addInt("Position", gCvars.esp_pos, 0, 2, 1); //todo config
		addBool("Enemy Only", gCvars.esp_enemyonly);
		addBool("Visible Only", gCvars.esp_visonly);
		addBool("Visibility Check", gCvars.esp_vischeck);
		addInt("Name", gCvars.esp_name, 0, 2, 1);
		addBool("Class", gCvars.esp_class);
		addInt("Boxes", gCvars.esp_box, 0, 2, 1);
		addInt("Bones", gCvars.esp_bones, 0, 3, 1);
		addInt("Health", gCvars.esp_health, 0, 3, 1);
		addBool("Conditions", gCvars.esp_conds); //todo config
		addBool("Snaplines", gCvars.esp_snapline);
		addBool("Anti-Disguise", gCvars.anti_disguise);

		addRightBool("Buildings", gCvars.esp_buildings);
		addRightBool("Enemy Only", gCvars.esp_buildings_enemyonly); //todo config
		addRightBool("Name", gCvars.esp_buildings_name); //todo config
		addRightInt("Boxes", gCvars.esp_buildings_box, 0, 3, 1); //todo config
		addRightInt("Health", gCvars.esp_buildings_health, 0, 3, 1); //todo config
		addEmptyR();
		addRightBool("Items", gCvars.esp_items);
		addEmptyR();

		addRightBool("dx9 Glow", gCvars.esp_glow); //todo config
		addRightBool("Glow Enemy Only", gCvars.esp_glow_enemy_only); //todo config
		addRightBool("Glow Buildings", gCvars.esp_glow_buildings); //todo config
		addRightBool("Glow Items", gCvars.esp_glow_items); //todo config

		addEmptyR();
	}
	if (addTab("Radar", radar))
	{
		addBool("Active", gCvars.radar_active);
		addEmptyL();
		addBool("Enemy Only", gCvars.radar_enemyonly);
		addInt("Radar X", gCvars.radar_x, 0, 2000, 20);
		addInt("Radar Y", gCvars.radar_y, 0, 2000, 20);
		addInt("Radar Size", gCvars.radar_size, 0, 1000, 10);
		addInt("Radar Range", gCvars.radar_range, 0, 10000, 50);
		addInt("Radar Alpha", gCvars.radar_alpha, 0, 255, 15);
		addInt("Radar Style", gCvars.radar_style, 0, 3, 1);
		addRightBool("Health Bars", gCvars.radar_health); //todo config
		addRightBool("Class Portraits", gCvars.radar_class_portraits); //todo config
		addRightInt("Icon Background", gCvars.radar_icon_background, 0, 2, 1); //todo config
		addRightInt("Icon Size", gCvars.radar_icon_size, 16, 64, 2); //todo config
		//addBool("Health Debug", gCvars.radar_health_debug); //todo config
	}
	if (addTab("HvH", hvh))
	{
		addBool("Anti-Aim", gCvars.hvh_aa_active);
		addEmptyL();
		addInt("AA Pitch", gCvars.hvh_aa_pitch, 0, 3, 1);
		addInt("AA Yaw", gCvars.hvh_aa_yaw, 0, 6, 1);
		if (gCvars.hvh_aa_yaw == 4)
			addInt("- Speed", gCvars.hvh_aa_spin_speed, 0, 180, 1);
		addEmptyL();
		addInt("Resolver", gCvars.hvh_resolver, 0, 3, 1);
		addBool("Resolver Rage Only", gCvars.hvh_resolver_rageonly); //todo config
		addBool("Show real local angles", gCvars.hvh_resolver_realangles); //todo config

		addRightBool("Speed Hack", gCvars.hvh_speedhack); //todo config
		addEmptyR();
		addRightInt("Speed Key", gCvars.hvh_speedhack_key, 0, 8, 1); //todo config
		addRightInt("Speed Amount", gCvars.hvh_speedhack_amt, 0, 300, 10); //todo config
	}
	if (addTab("Misc", misc))
	{
		addBool("Bunny Hop", gCvars.misc_bunnyhop);
		addBool("Auto Strafe", gCvars.misc_autostrafe);
		addBool("Third Person", gCvars.misc_thirdperson);
		addBool("Cheats bypass", gCvars.misc_sv_cheats);
		addBool("Pure bypass", gCvars.misc_sv_pure);
		addBool("Anti-SMAC", gCvars.misc_antismac);
		addBool("Noisemaker Spam", gCvars.misc_noisemaker_spam);
		addBool("Tournament Spam", gCvars.misc_tournament_spam);
		addBool("Time Shift", gCvars.misc_remove_cond);
		addInt("Time Shift Key", gCvars.misc_remove_cond_key, 0, 8, 1);
		addInt("Time Shift Value", gCvars.misc_remove_cond_amt, 0, 5000, 50);
		addInt("Custom Fov", gCvars.misc_customfov, 90, 140, 1);// todo config
		addEmptyL();
		addBool("Insta Decloak", gCvars.misc_insta_decloak);
		addBool("Double Shoot", gCvars.misc_double_shoot);
		addBool("Sticky Spam", gCvars.misc_sticky_spam);

		addRightBool("Chat Spam", gCvars.misc_chat_spam);
		addRightInt("Newline Amt", gCvars.misc_chat_spam_newline_amt, 0, 32, 1);
		//addRightBool("Killsay", gCvars.misc_killsay_enabled);
		addRightInt("Killsay", gCvars.misc_killsay_type, 0, 3, 1); // todo config
		addRightInt("Newline Amt", gCvars.misc_killsay_newline_amt, 0, 32, 1); // todo config
		addEmptyR();
		addRightBool("Server Lag Exploit", gCvars.misc_lag_exploit);
		addRightInt("Key", gCvars.misc_lag_exploit_key, 0, 8, 1);
		addRightInt("Value", gCvars.misc_lag_exploit_value, 0, 150, 5);
		addRightInt("Method", gCvars.misc_lag_exploit_method, 0, 1, 1);
		addEmptyR();
		addRightBool("Backtracking", gCvars.misc_backtracking);
		addRightInt("Backtrack Ticks", gCvars.bt_maxticks, 1, 14, 1);
		addEmptyR();
		addRightBool("Skinchanger", gCvars.misc_skinchanger); //todo config

		addRightBool("Icon Test", gCvars.drawicontest); //todo config
	}
}

void GUI::Draw()
{
#ifndef DEV_BUILD
	gDraw.DrawStringFont(5, 5, Color::White(), "Iridium, Public Build rekt", gDraw.Verdana14);
#endif // DEV_BUILD

	//if (keyPress(VK_NUMPAD0))
	if (!gInts.Engine->IsInGame() && keyPress(VK_INSERT))
	{
		bMenuActive = !bMenuActive;
		gSettings.SaveSettings(".\\Iridium.json");
	}
	if (keyPress(VK_F11))
	{
		bMenuActive = !bMenuActive;
		gSettings.SaveSettings(".\\Iridium.json");
	}

	if (!bMenuActive)
		return;

#ifdef  DEV_BUILD
	gDraw.DrawStringFont(5, 5, Color::White(), /*XorString(*/"Iridium Developer Build :)"/*)*/, gDraw.Verdana14);
#endif //  DEV_BUILD

	gDraw.DrawStringFont(5, 5 + 14, Color::White(), __TIMESTAMP__, gDraw.Verdana14);
	gDraw.DrawStringFont(5, 5 + (14 * 2), Color::White(), "Press 'INSERT' or 'F11' to open the menu", gDraw.Verdana14);
	gDraw.DrawStringFont(5, 5 + (14 * 3), Color::White(), "Use mouse to navitage the menu", gDraw.Verdana14);

	getInput();

	static bool dragging = false;
	if (mb == MouseBtn::lclick && mouseOver(menupos.x, menupos.y, Proportions.x, 15))
		dragging = true;
	else if (mb != MouseBtn::ldown)
		dragging = false;

	if (dragging)
	{
		menupos.x += mouse.x - pmouse.x;
		menupos.y += mouse.y - pmouse.y;
	}

	createGUI();
	Color clrColor;
	if (gCvars.visual_rainbow)
	{
		clrColor = rainbowCurrent();
	}
	else
	{
		if (gCvars.visual_colors == 3)
		{
			clrColor = HueToRGB(gCvars.visual_menu_hue);
		}
		else
		{
			CBaseEntity *pLocal = GetBaseEntity(me);
			if (pLocal)
				clrColor = gDraw.GetPlayerColor(pLocal, 255, true);
			else
				clrColor = Color(255, 0, 0, 255);
		}
	}

	gDraw.DrawRect(menupos.x, menupos.y, Proportions.x, Proportions.y, Color::Black());
	gDraw.DrawRect(menupos.x, menupos.y, Proportions.x, 15, clrColor);
	gDraw.DrawStringCenterFont(menupos.x + (Proportions.x/2), menupos.y + 1, Color::White(), "Iridium", gDraw.Verdana14);

	int tabx = menupos.x + 20, taby = menupos.y + 32;
	int itemx = menupos.x + 170, itemy = menupos.y + 32;
	int ritemx = menupos.x + 470, ritemy = menupos.y + 32;
	for (size_t i = 0; i < itemlist.size(); i++)
	{
		if (itemlist[i].type == e_type::null)
			continue;

		// Check for tab clicks
		if (mouseOver(tabx, taby, iTabWidth, iTabHeight) && itemlist[i].type == e_type::tab)
		{
			gDraw.DrawRect(tabx, taby, iTabWidth, iTabHeight, Color(255, 255, 255, 60));
			if (mb == MouseBtn::lclick)
			{
				for (size_t j = 0; j < itemlist.size(); j++)
				{
					if (itemlist[j].type == e_type::tab && *itemlist[j].bValue)
						*itemlist[j].bValue = false;
				}
				*itemlist[i].bValue = true;
			}
		}

		//if (itemlist[i].type == e_type::rightbool || itemlist[i].type == e_type::rightint)
		//{
		//	itemy = menupos.y + 32; //resets the item y position
		//	itemx = menupos.x + 470; //offsets the item x value by 300
		//}

		// Check for item clicks
		if (mouseOver(itemx - 2, itemy, optionspacing + 40, gDraw.GetESPHeight())
			&& itemlist[i].type != e_type::tab
			&& itemlist[i].type != e_type::emptyR
			&& itemlist[i].type != e_type::emptyL)
		{
			//if (itemlist[i].type == e_type::rightbool || itemlist[i].type == e_type::rightint)
			//	gDrawManager.DrawRect(ritemx - 2, ritemy - 2, optionspacing + 40, gDrawManager.GetESPHeight() + 4, clrColor);
			//else
			gDraw.DrawRect(itemx - 2, itemy - 2, optionspacing + 40, gDraw.GetESPHeight() + 4, clrColor);

			if (mb == MouseBtn::lclick || mb == MouseBtn::rclick)
			{
				bool add = mb == MouseBtn::rclick;
				switch (itemlist[i].type)
				{
				case e_type::boolean:
					*itemlist[i].bValue = !*itemlist[i].bValue; break;
				case e_type::integer:
					*itemlist[i].iValue += add ? itemlist[i].step : -itemlist[i].step;
					if (*itemlist[i].iValue > itemlist[i].max)
						*itemlist[i].iValue = itemlist[i].min;
					else if (*itemlist[i].iValue < itemlist[i].min)
						*itemlist[i].iValue = itemlist[i].max;
				}
			}
		}
		if (mouseOver(ritemx - 2, ritemy, optionspacing + 4, gDraw.GetESPHeight()) && itemlist[i].type != e_type::tab)
		{
			if (itemlist[i].type == e_type::rightbool || itemlist[i].type == e_type::rightint)
			{
				gDraw.DrawRect(ritemx - 2, ritemy - 2, optionspacing + 40, gDraw.GetESPHeight() + 4, clrColor);

				if (mb == MouseBtn::lclick || mb == MouseBtn::rclick)
				{
					bool add = mb == MouseBtn::rclick;
					switch (itemlist[i].type)
					{
					case e_type::boolean:
					case e_type::rightbool:
						*itemlist[i].bValue = !*itemlist[i].bValue; break;
					case e_type::integer:
					case e_type::rightint:
						*itemlist[i].iValue += add ? itemlist[i].step : -itemlist[i].step;
						if (*itemlist[i].iValue > itemlist[i].max)
							*itemlist[i].iValue = itemlist[i].min;
						else if (*itemlist[i].iValue < itemlist[i].min)
							*itemlist[i].iValue = itemlist[i].max;
					}
				}
			}
		}

		//Draw Tabs
		if (itemlist[i].type == e_type::tab)
		{
			gDraw.OutlineRect(tabx, taby, iTabWidth, iTabHeight, clrColor);
			gDraw.DrawStringCenterFont(tabx + iTabWidth / 2, taby + (iTabHeight / 2 - 9), *itemlist[i].bValue ? clrColor : Color(255, 255, 255, 255), itemlist[i].name, gDraw.Verdana14);
			taby += (iTabHeight + 5);
		}

		//Draw Items
		if (itemlist[i].type == e_type::emptyL)
		{
			itemy += iItemSpacing;
		}
		else if (itemlist[i].type == e_type::emptyR)
		{
			ritemy += iItemSpacing;
		}
		else if (itemlist[i].type != e_type::tab)
		{
			//if (itemlist[i].type != e_type::rightbool && itemlist[i].type != e_type::rightint)
			if (itemlist[i].type == e_type::boolean || itemlist[i].type == e_type::integer)
			{
				//I know this shit is messy but: if the type is a boolean, it performs another check for the value, and if the value is false,
				//sets the color to gray, if all else fails it sets the color to white
				gDraw.DrawString(itemx, itemy, itemlist[i].type == e_type::boolean ? (*itemlist[i].bValue ? Color::White() : Color(128, 128, 128, 255)) : Color::White(), itemlist[i].name);

				string value = itemlist[i].type == e_type::boolean ? boolStr(*itemlist[i].bValue) : to_string(*itemlist[i].iValue);
				if (itemlist[i].type == e_type::integer)
				{
					value = ReplaceInts(itemlist[i]);
				}
				gDraw.DrawStringCenter(itemx + 170, itemy, itemlist[i].type == e_type::boolean ? (*itemlist[i].bValue ? Color::White() : Color(128, 128, 128, 255)) : Color::White(), value.c_str());
				itemy += iItemSpacing;

				//FOV CIRCLE when hovered over FOV
				if (itemlist[i].max == 180)
				{
					// TODO its either 16/9 or 9/16
					//float mon_fov = (float(gScreenSize.iScreenWidth) / float(gScreenSize.iScreenHeight) / (4.0f / 3.0f));
					//float actualFov = 2 * atan(mon_fov * tan(DEG2RAD(gCvars.misc_customfov / 2.0f)));
					//float radius = tan(DEG2RAD(gCvars.aimbot_fov / 2)) / tan(DEG2RAD(actualFov / 2));
					//gDrawManager.DrawCircle(gScreenSize.iScreenWidth / 2, gScreenSize.iScreenHeight / 2, radius, 20, Color(255, 255, 255, 255));
				
					if (gCvars.DrawAimFov)
					{
						float pxR = tan(DEG2RAD(gCvars.aimbot_fov)) / tan(DEG2RAD(106.3f) / 2) * (gScreenSize.iScreenWidth / 2);
						gDraw.DrawCircle(gScreenSize.iScreenWidth / 2, gScreenSize.iScreenHeight / 2, pxR, 50, Color::White());
					}
				}

			}
			else if (itemlist[i].type == e_type::rightbool || itemlist[i].type == e_type::rightint)
			{
				//I know this shit is messy but: if the type is a boolean, it performs another check for the value, and if the value is false,
				//sets the color to gray, if all else fails it sets the color to white
				gDraw.DrawString(ritemx, ritemy, itemlist[i].type == e_type::rightbool ? (*itemlist[i].bValue ? Color::White() : Color(128, 128, 128, 255)) : Color::White(), itemlist[i].name);

				string value = itemlist[i].type == e_type::rightbool ? boolStr(*itemlist[i].bValue) : to_string(*itemlist[i].iValue);
				if (itemlist[i].type == e_type::rightint)
				{
					value = ReplaceInts(itemlist[i]);
				}
				gDraw.DrawStringCenter(ritemx + optionspacing, ritemy, itemlist[i].type == e_type::rightbool ? (*itemlist[i].bValue ? Color::White() : Color(128, 128, 128, 255)) : Color::White(), value.c_str());
				ritemy += iItemSpacing;
			}
		}
	}
	itemlist.clear();
}

//===========================================================================================================================================================================================
//===========================================================================================================================================================================================
//===========================================================================================================================================================================================

void GUI::getInput()
{
	int mx = 0, my = 0;
	gInts.Surface->GetCursorPosition(mx, my);

	pmouse = mouse;
	mouse = { mx, my };

	if (keyDown(VK_LBUTTON))
	{
		if (mb == MouseBtn::lclick || mb == MouseBtn::ldown)
			mb = MouseBtn::ldown;
		else
			mb = MouseBtn::lclick;
	}
	else if (keyDown(VK_RBUTTON))
	{
		if (mb == MouseBtn::rclick || mb == MouseBtn::rdown)
			mb = MouseBtn::rdown;
		else
			mb = MouseBtn::rclick;
	}
	else
		mb = MouseBtn::null;
}

//===========================================================================================================================

bool GUI::mouseOver(int x, int y, int w, int h)
{
	return mouse.x >= x && mouse.x <= x + w && mouse.y >= y && mouse.y <= y + h;
}

bool GUI::addTab(const char* name, bool &value)
{
	Item temp;
	temp.name = name;
	temp.type = e_type::tab;
	temp.bValue = &value;

	itemlist.push_back(temp);
	return value;
}

void GUI::addBool(const char* name, bool &value)
{
	Item temp;
	temp.name = name;
	temp.type = e_type::boolean;
	temp.bValue = &value;

	itemlist.push_back(temp);
}

void GUI::addInt(const char* name, int &value, int min, int max, int step)
{
	Item temp;
	temp.name = name;
	temp.type = e_type::integer;
	temp.iValue = &value;
	temp.min = min, temp.max = max, temp.step = step;

	itemlist.push_back(temp);
}
void GUI::addRightBool(const char* name, bool &value)
{
	bool right = true;
	Item temp;
	temp.name = name;
	temp.type = e_type::rightbool;
	temp.bValue = &value;
	temp.bRight = &right;

	itemlist.push_back(temp);
}

void GUI::addRightInt(const char* name, int &value, int min, int max, int step)
{
	bool right = true;
	Item temp;
	temp.name = name;
	temp.type = e_type::rightint;
	temp.iValue = &value;
	temp.min = min, temp.max = max, temp.step = step;
	temp.bRight = &right;

	itemlist.push_back(temp);
}

void GUI::addEmptyL()
{
	Item temp;
	temp.type = e_type::emptyL;
	itemlist.push_back(temp);
}

void GUI::addEmptyR()
{
	Item temp;
	temp.type = e_type::emptyR;
	itemlist.push_back(temp);
}
//=======================================================================================================================================
string GUI::ReplaceInts(Item itemlist)
{
	string value = to_string(*itemlist.iValue);
	if (itemlist.name == "Aim Key" || itemlist.name == "Trigger Key" || itemlist.name == "Key" || itemlist.name == "Time Shift Key" || itemlist.name == "Speed Key")
	{
		switch (*itemlist.iValue)
		{
		case 0: value = "None"; break;
		case 1: value = "Mouse1"; break;
		case 2: value = "Mouse2"; break;
		case 3: value = "Mouse3"; break;
		case 4: value = "Mouse4"; break;
		case 5: value = "Mouse5"; break;
		case 6: value = "Shift"; break;
		case 7: value = "Alt"; break;
		case 8: value = "F"; break;
		default:
			break;
		}
	}
	else if (itemlist.name == " - Target")
	{
		switch (*itemlist.iValue)
		{
		case 0: value = "Enemy"; break;
		case 1: value = "Team"; break;
		case 2: value = "Both"; break;
		default:
			break;
		}
	}
	else if (itemlist.name == "Position")
	{
		switch (*itemlist.iValue)
		{
		case 0: value = "Default"; break;
		case 1: value = "Bottom"; break;
		default:
			break;
		}
	}
	else if (itemlist.name == "Aim Position")
	{
		switch (*itemlist.iValue)
		{
		case 0: value = "Auto"; break;
		default:
			break;
		}
	}
	else if (itemlist.name == "Boxes")
	{
		switch (*itemlist.iValue)
		{
		case 0: value = "OFF"; break;
		case 1: value = "Normal"; break;
		case 2: value = "Corners"; break;
		default:
			break;
		}
	}
	else if (itemlist.name == "Bones")
	{
		switch (*itemlist.iValue)
		{
		case 0: value = "OFF"; break;
		case 1: value = "White"; break;
		case 2: value = "Health"; break;
		default:
			break;
		}
	}
	else if (itemlist.name == "Health")
	{
		switch (*itemlist.iValue)
		{
		case 0: value = "OFF"; break;
		case 1: value = "Value"; break;
		case 2: value = "Bar"; break;
		case 3: value = "Both"; break;
		default:
			break;
		}
	}
	else if (itemlist.name == "Color Style")
	{
		switch (*itemlist.iValue)
		{
		case 0: value = "m4risa.cfg"; break;
		case 1: value = "TF2"; break;
		case 2: value = "Null Core"; break;
		case 3: value = "CUSTOM"; break;
		default:
			break;
		}
	}
	else if (itemlist.name == "Hands")
	{
		switch (*itemlist.iValue)
		{
		case 0: value = "OFF"; break;
		case 1: value = "No Hands"; break;
		case 2: value = "Wireframe"; break;
		case 3: value = "Flat"; break;
		default:
			break;
		}
	}
	else if (itemlist.name == "Icon Background")
	{
		switch (*itemlist.iValue)
		{
		case 0: value = "OFF"; break;
		case 1: value = "Black"; break;
		case 2: value = "Team"; break;
		default:
			break;
		}
	}
	else if (itemlist.name == "Weapons")
	{
		switch (*itemlist.iValue)
		{
		case 0: value = "OFF"; break;
		case 1: value = "No Weapon"; break;
		case 2: value = "Wireframe"; break;
		default:
			break;
		}
	}
	else if (itemlist.name == "Players")
	{
		switch (*itemlist.iValue)
		{
		case 0: value = "OFF"; break;
		default:
			break;
		}
	}
	else if (itemlist.name == "Radar Style")
	{
		switch (*itemlist.iValue)
		{
		case 0: value = "DarkStorm"; break;
		case 1: value = "Lithium"; break;
		case 2: value = "LMAOBox"; break;
		case 3: value = "Null Core"; break;
		default:
			break;
		}
	}
	else if (itemlist.name == "AA Pitch")
	{
		switch (*itemlist.iValue)
		{
		case 0: value = "OFF"; break;
		case 1: value = "Fakedown"; break;
		case 2: value = "Fakeup"; break;
		case 3: value = "Half up"; break;
		default:
			break;
		}
	}
	else if (itemlist.name == "AA Yaw")
	{
		switch (*itemlist.iValue)
		{
		case 0: value = "OFF"; break;
		case 1: value = "Left"; break;
		case 2: value = "Right"; break;
		case 3: value = "Jitter"; break;
		case 4: value = "Spin"; break;
		case 5: value = "Fake Side 1"; break;
		case 6: value = "Fake Side 2"; break;
		default:
			break;
		}
	}
	else if (itemlist.name == "Resolver")
	{
		switch (*itemlist.iValue)
		{
		case 0: value = "OFF"; break;
		case 1: value = "Auto"; break;
		case 2: value = "Up"; break;
		case 3: value = "Down"; break;
		default:
			break;
		}
	}
	else if (itemlist.name == "Method")
	{
		switch (*itemlist.iValue)
		{
		case 0: value = "SAY"; break;
		case 1: value = "VOICEMENU"; break;
		default:
			break;
		}
	}
	else if (itemlist.name == "Killsay")
	{
		switch (*itemlist.iValue)
		{
		case 0: value = "OFF"; break;
		case 1: value = "Default"; break;
		case 2: value = "NCC"; break;
		case 3: value = "Chill"; break;
		default:
			break;
		}
	}
	return value;
}
