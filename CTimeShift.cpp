#include "CTimeShift.h"
#include "Misc.h"

CTimeShift gTShift;

void CTimeShift::Run(CBaseEntity * pLocal, CUserCmd * pCmd)
{


	//TIME SHIFT MEMES
	//if (gMisc.misc_ts.value && gMisc.misc_ts_key.KeyDown())
	//	gTShift.TimeShift(gCvars.misc_remove_cond_amt);

	//if (gMisc.misc_decloak.value)
	//	gTShift.InstaDecloak(pLocal, pCmd);

	//if (gMisc.misc_dshoot.value)
	//	gTShift.DoubleShoot(pLocal, pCmd);

	//if (gMisc.misc_instarev.value)
	//	gTShift.InstaRev(pLocal, pCmd);

	//gTShift.StickySpam(pLocal, pCmd);


}

void CTimeShift::Regular(int amount)
{
	TimeShift(amount);
}

void CTimeShift::InstaDecloak(CBaseEntity * pLocal, CUserCmd * pCmd)
{
	//float fCurTime = gInts.g_pGlobals->curtime;
	//int amt = 0;

	//if (pLocal->szGetClass() != "Spy")
	//	return;

	//if (pLocal->GetCond() & TFCond_Cloaked && (fLastTime + 3 < fCurTime) && (pCmd->buttons & IN_ATTACK || pCmd->buttons & IN_ATTACK2))
	if (pLocal->GetCond() & TFCond_Cloaked && (pCmd->buttons & IN_ATTACK || pCmd->buttons & IN_ATTACK2))
	{
		pCmd->buttons &= ~IN_ATTACK;
		pCmd->buttons |= IN_ATTACK2;
		TimeShift(180);
	}
}

void CTimeShift::DoubleShoot(CBaseEntity * pLocal, CUserCmd * pCmd)
{
	int amount = 0;
	static bool bWasHolding = false;
	CBaseCombatWeapon *wep = pLocal->GetActiveWeapon();
	if (!wep)
		return;

	bool bIsHolding = ((pCmd->buttons & IN_ATTACK) ||
		(pCmd->buttons & IN_ATTACK2));
	if (Util->CanShoot(pLocal, wep) && bIsHolding && !bWasHolding &&
		*wep->pItemID() != WPN_Flaregun)
		amount = 1 * 90;
	else if (Util->CanShoot(pLocal, wep) && bIsHolding && !bWasHolding &&
		*wep->pItemID() == WPN_Flaregun)
		amount = 2 * 90;
	else if (bWasHolding && !bIsHolding)
		amount = 1 * 90;

	bWasHolding = (pCmd->buttons & IN_ATTACK) ||
		(pCmd->buttons & IN_ATTACK2);

	TimeShift(amount);
}

void CTimeShift::InstaRev(CBaseEntity * pLocal, CUserCmd * pCmd)
{
	if (pLocal->szGetClass() != "Heavy")
		return;

	int amount = 0;
	static bool bWasHolding = false;
	CBaseCombatWeapon *wep = pLocal->GetActiveWeapon();
	if (!wep)
		return;

	if (wep->GetSlot() != 1)
		return;

	bool bIsHolding = ((pCmd->buttons & IN_ATTACK) ||
		(pCmd->buttons & IN_ATTACK2));
	if (Util->CanShoot(pLocal, wep) && bIsHolding && !bWasHolding)
		amount = 1 * 90;
	else if (bWasHolding && !bIsHolding)
		amount = 1 * 90;

	bWasHolding = (pCmd->buttons & IN_ATTACK) ||
		(pCmd->buttons & IN_ATTACK2);

	TimeShift(amount);	
}

void CTimeShift::StickySpam(CBaseEntity * pLocal, CUserCmd * pCmd)
{
	if (!gMisc.misc_sticky.value)
		return;
	if (pLocal->szGetClass() != "Demoman")
		return;

	CBaseCombatWeapon *wep = pLocal->GetActiveWeapon(); //(CBaseCombatWeapon*)gInts.EntList->GetClientEntityFromHandle(pLocal->MyOtherWeapons()[1]);
	if (!wep)
		return;

	if (*wep->pItemID() != WPN_StickyLauncher
		&& *wep->pItemID() != WPN_ScottishResistance
		&& *wep->pItemID() != WPN_StickyJumper
		&& *wep->pItemID() != WPN_LoooseCannon)
		return;

	static bool bSwitch = false;
	if ((pCmd->buttons & IN_ATTACK) && !bSwitch)
	{
		bSwitch = true;
	}
	else if (bSwitch)
	{
		TimeShift(50);
		pCmd->buttons &= ~IN_ATTACK;
		bSwitch = false;
	}
}

void CTimeShift::TimeShift(int amount)
{
	INetChannel* ch = (INetChannel*)gInts.Engine->GetNetChannelInfo();
	int& m_nOutSequenceNr = *(int*)((unsigned)ch + 8);
	m_nOutSequenceNr += amount;
}
