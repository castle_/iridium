#include "CPlayerList.h"
#include "CDrawManager.h"
#include <string>
#include "SDK.h"
#include "CDrawManager.h"
#include "Util.h"
#include "GUI.h"
#include "Menu.h"
CPlayerList gPlayerList;

#define keyDown GetAsyncKeyState
#define keyPress(x) keyDown(x) & 1

#define MENU_WIDTH 300

void CPlayerList::createGUI()
{
		for (int p = 1; p <= gInts.Engine->GetMaxClients(); p++)
		{
			if (p == me)
				continue;

			CBaseEntity *pPlayer = GetBaseEntity(p);

			if (!pPlayer)
				continue;

			player_info_t pInfo;
			gInts.Engine->GetPlayerInfo(p, &pInfo);

			//string pName(pInfo.name);

			//char* ee = &pInfo.name[0];
			//addInt(ee, gCvars.PlayerMode[pPlayer->GetIndex()], 0, 2, 1); 
			addPlayer(p, gCvars.PlayerMode[pPlayer->GetIndex()], 0, 2, 1);
		}
}

void CPlayerList::draw()
{
	if (gCvars.cade_menu)
	{
		if (!gMenu.enabled)
			return;
	}
	else
	{
		if (!gGUI.bMenuActive)
			return;
	}

	getInput();

	static bool dragging = false;
	if (mb == plist_e_mb::lclick && mouseOver(pPlayerlistPos.x, pPlayerlistPos.y, MENU_WIDTH, 15))
		dragging = true;
	else if (mb != plist_e_mb::ldown)
		dragging = false;

	if (dragging)
	{
		pPlayerlistPos.x += mouse.x - pmouse.x;
		pPlayerlistPos.y += mouse.y - pmouse.y;
	}

	CBaseEntity *pLocal = GetBaseEntity(me);
	Color clrColor;
	if (pLocal)
		clrColor = gDraw.GetPlayerColor(pLocal, 255, true);
	else
		clrColor = Color(255, 0, 0, 255);

	if (gCvars.visual_colors == 3)
		clrColor = HueToRGB(gCvars.visual_menu_hue);

	createGUI();
	gDraw.DrawRect(pPlayerlistPos.x, pPlayerlistPos.y, MENU_WIDTH, (itemlist.size() + 1) * 14 + 3, Color(0, 0, 0, 255));
	//gDrawManager.OutlineRect(pmenu.x, pmenu.y, MENU_WIDTH, (itemlist.size() + 1) * 14 + 3, clrColor);
	gDraw.DrawRect(pPlayerlistPos.x, pPlayerlistPos.y, MENU_WIDTH, 15, clrColor);
	gDraw.DrawStringCenterFont(pPlayerlistPos.x + MENU_WIDTH/2, pPlayerlistPos.y + 1, /*Color(255, 255, 255, 255)*/ Color::White(), "Playerlist", gFonts.consolas);

	int x = pPlayerlistPos.x + 5, y = pPlayerlistPos.y + 16;
	for (size_t i = 0; i < itemlist.size(); i++)
	{
		if (itemlist[i].type == e_type::null)
			continue;

		if (!itemlist[i].pIndex)
			continue;

		if (itemlist[i].pIndex == me)
			continue;

		CBaseEntity *pPlayer = GetBaseEntity(itemlist[i].pIndex);
		if (!pPlayer) continue;

		player_info_t pInfo;
		gInts.Engine->GetPlayerInfo(itemlist[i].pIndex, &pInfo);

		if (mouseOver(x, y, MENU_WIDTH - 10, 13))
		{
			gDraw.DrawRect(x, y, MENU_WIDTH - 10, 13, Color(255, 255, 255, 255));
			if (mb == plist_e_mb::lclick || mb == plist_e_mb::rclick)
			{
				bool add = mb == plist_e_mb::rclick;
				switch (itemlist[i].type)
				{
				case e_type::integer:
					*itemlist[i].iValue += add ? itemlist[i].step : -itemlist[i].step;
					if (*itemlist[i].iValue > itemlist[i].max)
						*itemlist[i].iValue = itemlist[i].min;
					else if (*itemlist[i].iValue < itemlist[i].min)
						*itemlist[i].iValue = itemlist[i].max;
				}
			}
		}
			gDraw.DrawString(x, y, gDraw.GetPlayerColor(pPlayer), pInfo.name);

			string value = to_string(*itemlist[i].iValue);
			if (itemlist[i].type == e_type::integer)
			{
					switch (*itemlist[i].iValue)
					{
					case 0: value = "Friend"; break;
					case 1: value = "Normal"; break;
					case 2: value = "Rage"; break;
					default:
						break;
					}
			}

			gDraw.DrawString(x + MENU_WIDTH - 50, y, gDraw.GetPlayerColor(pPlayer), value.c_str());
		
		y += 14;
	}
	itemlist.clear();
}

bool CPlayerList::mouseOver(int x, int y, int w, int h)
{
	return mouse.x >= x && mouse.x <= x + w && mouse.y >= y && mouse.y <= y + h;
}

void CPlayerList::getInput()
{
	int mx = 0, my = 0;
	gInts.Surface->GetCursorPosition(mx, my);

	pmouse = mouse;
	mouse = { mx, my };

	if (keyDown(VK_LBUTTON))
	{
		if (mb == plist_e_mb::lclick || mb == plist_e_mb::ldown)
			mb = plist_e_mb::ldown;
		else
			mb = plist_e_mb::lclick;
	}
	else if (keyDown(VK_RBUTTON))
	{
		if (mb == plist_e_mb::rclick || mb == plist_e_mb::rdown)
			mb = plist_e_mb::rdown;
		else
			mb = plist_e_mb::rclick;
	}
	else
		mb = plist_e_mb::null;
}

void CPlayerList::addInt(const char* name, int &value, int min, int max, int step)
{
	Item temp;
	temp.name = name;
	temp.type = e_type::integer;
	temp.iValue = &value;
	temp.min = min, temp.max = max, temp.step = step;

	itemlist.push_back(temp);
}

void CPlayerList::addPlayer(int pIndex, int &value, int min, int max, int step)
{
	Item temp;
	temp.pIndex = pIndex;
	temp.type = e_type::integer;
	temp.iValue = &value;
	temp.min = min, temp.max = max, temp.step = step;

	itemlist.push_back(temp);
}