#pragma once
//===================================================================================
#include "SDK.h"
#include "Panels.h"
#include "ESP.h"
#include "DrawUtils.h"
#include <map>

#define RED(COLORCODE)	((int) ( COLORCODE >> 24) )
#define BLUE(COLORCODE)	((int) ( COLORCODE >> 8 ) & 0xFF )
#define GREEN(COLORCODE)	((int) ( COLORCODE >> 16 ) & 0xFF )
#define ALPHA(COLORCODE)	((int) COLORCODE & 0xFF )
#define COLORCODE(r,g,b,a)((DWORD)((((r)&0xff)<<24)|(((g)&0xff)<<16)|(((b)&0xff)<<8)|((a)&0xff)))
//===================================================================================
class CDrawManager
{
public:
	void Initialize( );
	void Reload( );
	void DrawString( int x, int y, Color clrColor, const wchar_t *pszText);
	void DrawString(int x, int y, Color color, const char *pszText, HFont font, bool bCenter = false);
	void DrawString( int x, int y, Color clrColor, const char *pszText, ... );
	void DrawStringCenter(int x, int y, Color clrColor, const char * szText, ...);
	void DrawStringFont(int x, int y, Color clrColor, const char *pszText, int font, ...);
	void DrawStringCenterFont(int x, int y, Color clrColor, const char * szText, int font, ...);
	byte GetESPHeight( );
	void DrawLine(int x0, int y0, int x1, int y1, Color clrColor);
	void DrawLineEx(int x, int y, int w, int h, Color clrColor);
	void DrawBox( Vector vOrigin, int r, int g, int b, int alpha, int box_width, int radius );
	bool GetEntityBounds(CBaseEntity* pEntity, float& x, float& y, float& w, float& h);
	void DrawBoxOnEntity(int mode, CBaseEntity* entity, Color color);
	void DrawCornerBox(int x, int y, int w, int h, int cx, int cy, Color Col);
	void Draw3DBox(const Vector& mins, const Vector& maxs, Color Col);
	void DrawRect( int x, int y, int w, int h, Color clrColor);
	void DrawPolygon(int count, AVertex_t* verts, HTexture texture);
	void DrawCircle(int x, int y, int radius, int segments, Color clrColor);
	void DrawFilledCircle(int centerx, int centery, float radius, Color clrColor);
	void OutlineRect( int x, int y, int w, int h, Color clrColor);
	void DrawTexturedRect(int textureId, int x, int y, int w, int h);
	bool WorldToScreen( Vector &vOrigin, Vector &vScreen );
	int GetTextureIDFromEntity(CBaseEntity* pEntity, bool portraits = false);
	Color GetPlayerColor(CBaseEntity* pPlayer, int alpha = 255, bool isMenu = false);
	//Custom Fonts
	unsigned long Consolas14, Consolas16, Verdana14;

	//std::map<int, std::string> g_vClassIcons = {
	//{ 0, "white" },
	//{ 1, "materials/vgui/class_portraits/scout" },
	//{ 2, "tf2_textures_dir\\materials\\vgui\\class_portraits\\scout_blue" },
	//{ 3, "tf2_textures_dir\\materials\\vgui\\class_portraits\\soldier" },
	//{ 4, "tf2_textures_dir\\materials\\vgui\\class_portraits\\soldier_blue" },
	//{ 5, "tf2_textures_dir\\materials\\vgui\\class_portraits\\pyro" },
	//{ 6, "tf2_textures_dir\\materials\\vgui\\class_portraits\\pyro_blue" },
	//{ 7, "tf2_textures_dir\\materials\\vgui\\class_portraits\\demoman" },
	//{ 8, "tf2_textures_dir\\materials\\vgui\\class_portraits\\demoman_blue" },
	//{ 9, "tf2_textures_dir\\materials\\vgui\\class_portraits\\heavy" },
	//{ 10, "tf2_textures_dir\\materials\\vgui\\class_portraits\\heavy_blue" },
	//{ 11, "tf2_textures_dir\\materials\\vgui\\class_portraits\\engineer" },
	//{ 12, "tf2_textures_dir\\materials\\vgui\\class_portraits\\engineer_blue" },
	//{ 13, "tf2_textures_dir\\materials\\vgui\\class_portraits\\medic" },
	//{ 14, "materials\\vgui\\class_portraits\\medic_blue.vtf" },
	//{ 15, "materials\\vgui\\class_portraits\\sniper" },
	//{ 16, "tf\\materials\\vgui\\class_portraits\\sniper_blue.vtf" },
	//{ 17, "tf\\materials\\vgui\\class_portraits\\spy" },
	//{ 18, "tf2_textures_dir\\materials\\vgui\\class_portraits\\spy_blue.vtf" }
	//};

	enum class_icons_t
	{
		ICON_SCOUT = 0,
		ICON_SCOUT_BLUE,
		ICON_SOLDIER,
		ICON_SOLDIER_BLUE,
		ICON_PYRO,
		ICON_PYRO_BLUE,
		ICON_DEMOMAN,
		ICON_DEMOMAN_BLUE,
		ICON_HEAVY,
		ICON_HEAVY_BLUE,
		ICON_ENGINEER,
		ICON_ENGINEER_BLUE,
		ICON_MEDIC,
		ICON_MEDIC_BLUE,
		ICON_SNIPER,
		ICON_SNIPER_BLUE,
		ICON_SPY,
		ICON_SPY_BLUE,
		ICON_LEADERBOARD_SCOUT,
		ICON_LEADERBOARD_SOLDIER,
		ICON_LEADERBOARD_PYRO,
		ICON_LEADERBOARD_DEMOMAN,
		ICON_LEADERBOARD_HEAVY,
		ICON_LEADERBOARD_ENGINEER,
		ICON_LEADERBOARD_MEDIC,
		ICON_LEADERBOARD_SNIPER,
		ICON_LEADERBOARD_SPY,
	};

	int g_vRankTextures[27];
	std::vector<std::string> g_vRankFiles = {
		"vgui/class_portraits/scout_alpha",			//0
		"vgui/class_portraits/scout_blue_alpha",	//1
		"vgui/class_portraits/soldier_alpha",		//2
		"vgui/class_portraits/soldier_blue_alpha",	//3
		"vgui/class_portraits/pyro_alpha",			//4
		"vgui/class_portraits/pyro_blue_alpha",		//5
		"vgui/class_portraits/demoman_alpha",		//6
		"vgui/class_portraits/demoman_blue_alpha",	//7
		"vgui/class_portraits/heavy_alpha",			//8
		"vgui/class_portraits/heavy_blue_alpha",	//9
		"vgui/class_portraits/engineer_alpha",		//10
		"vgui/class_portraits/engineer_blue_alpha",	//11
		"vgui/class_portraits/medic_alpha",			//12
		"vgui/class_portraits/medic_blue_alpha",	//13
		"vgui/class_portraits/sniper_alpha",		//14
		"vgui/class_portraits/sniper_blue_alpha",	//15
		"vgui/class_portraits/spy_alpha",			//16
		"vgui/class_portraits/spy_blue_alpha",		//17
		"hud/leaderboard_class_scout",				//18
		"hud/leaderboard_class_soldier",			//19
		"hud/leaderboard_class_pyro",				//20
		"hud/leaderboard_class_demo",				//21
		"hud/leaderboard_class_heavy",				//22
		"hud/leaderboard_class_engineer",			//23
		"hud/leaderboard_class_medic",				//24
		"hud/leaderboard_class_sniper",				//25
		"hud/leaderboard_class_spy",				//26
	};

	int iClassIconTextureID[18];
private:
	//Default Font
	unsigned long Tahoma12;
};
//===================================================================================
extern CDrawManager gDraw;
//===================================================================================
inline Color redGreenGradiant(float percent, int alpha = 255)
{
	if (percent < 0 || percent > 1) { return Color(0, 128, 0, alpha); } //overheal

	int r = 0, g = 0;
	if (percent < 0.5)
	{
		r = 255;
		g = (int)(255 * percent / 0.5);  //closer to 0.5, closer to yellow (255,255,0)
	}
	else
	{
		g = 255;
		r = 255 - (int)(255 * (percent - 0.5) / 0.5); //closer to 1.0, closer to green (0,255,0)
	}
	if (r == 0 && g == 0)
		g = 128; //another overheal check just in case

	return Color(r, g, 0, alpha);
}

inline Color redGreenGradiant(int i, int max, int alpha = 255)
{
	float percent = float(i) / float(max);
	return redGreenGradiant(percent);
}
inline Color rainbowCurrent()
{
	float R = sin(gInts.g_pGlobals->curtime * 4) * 127 + 128;
	float G = sin(gInts.g_pGlobals->curtime * 4 + 2) * 127 + 128;
	float B = sin(gInts.g_pGlobals->curtime * 4 + 4) * 127 + 128;

	return Color((int)R, (int)G, (int)B, 255);
}



/*

TODO: Find a HSV to RGB function that works :))

*/

inline Color ColorChange()
{
	float R = sin((float)gCvars.visual_menu_hue) * 127 + 128;
	float G = sin((float)gCvars.visual_menu_hue + 2) * 127 + 128;
	float B = sin((float)gCvars.visual_menu_hue + 4) * 127 + 128;

	return Color((int)R, (int)G, (int)B, 255);
}

inline Color HSVToRGB(int h, int s, int v) //hehe https://stackoverflow.com/questions/8208905/hsv-0-255-to-rgb-0-255
{
	int r, g, b;
	int i = (int)h * 6;
	//int i = floor(h * 6);
	int f = h * 6 - i;
	int p = v * (1 - s);
	int q = v * (1 - f * s);
	int t = v * (1 - (1 - f) * s);

	switch (i % 6) 
	{
	case 0: r = v, g = t, b = p; break;
	case 1: r = q, g = v, b = p; break;
	case 2: r = p, g = v, b = t; break;
	case 3: r = p, g = q, b = v; break;
	case 4: r = t, g = p, b = v; break;
	case 5: r = v, g = p, b = q; break;
	}

	return Color(r * 255, g * 255, b * 255, 255);
}
inline Color HSVToRGB2(float fH, float fS, float fV) //credits https://gist.github.com/fairlight1337/4935ae72bcbcc1ba5c72
{
	float fC = fV * fS; // Chroma
	float fHPrime = fmod(fH / 60.0, 6);
	float fX = fC * (1 - fabs(fmod(fHPrime, 2) - 1));
	float fM = fV - fC;
	float fR, fG, fB;

	if (0 <= fHPrime && fHPrime < 1) {
		fR = fC;
		fG = fX;
		fB = 0;
	}
	else if (1 <= fHPrime && fHPrime < 2) {
		fR = fX;
		fG = fC;
		fB = 0;
	}
	else if (2 <= fHPrime && fHPrime < 3) {
		fR = 0;
		fG = fC;
		fB = fX;
	}
	else if (3 <= fHPrime && fHPrime < 4) {
		fR = 0;
		fG = fX;
		fB = fC;
	}
	else if (4 <= fHPrime && fHPrime < 5) {
		fR = fX;
		fG = 0;
		fB = fC;
	}
	else if (5 <= fHPrime && fHPrime < 6) {
		fR = fC;
		fG = 0;
		fB = fX;
	}
	else {
		fR = 0;
		fG = 0;
		fB = 0;
	}

	fR += fM;
	fG += fM;
	fB += fM;

	return Color(fR, fG, fB, 255);
}

/*
function hsvToRgb(h, s, v){
var r, g, b;

var i = Math.floor(h * 6);
var f = h * 6 - i;
var p = v * (1 - s);
var q = v * (1 - f * s);
var t = v * (1 - (1 - f) * s);

switch(i % 6){
case 0: r = v, g = t, b = p; break;
case 1: r = q, g = v, b = p; break;
case 2: r = p, g = v, b = t; break;
case 3: r = p, g = q, b = v; break;
case 4: r = t, g = p, b = v; break;
case 5: r = v, g = p, b = q; break;
}

return [r * 255, g * 255, b * 255];
}
*/

/*
void HSVtoRGB(float& fR, float& fG, float& fB, float& fH, float& fS, float& fV) {
float fC = fV * fS; // Chroma
float fHPrime = fmod(fH / 60.0, 6);
float fX = fC * (1 - fabs(fmod(fHPrime, 2) - 1));
float fM = fV - fC;

if(0 <= fHPrime && fHPrime < 1) {
fR = fC;
fG = fX;
fB = 0;
} else if(1 <= fHPrime && fHPrime < 2) {
fR = fX;
fG = fC;
fB = 0;
} else if(2 <= fHPrime && fHPrime < 3) {
fR = 0;
fG = fC;
fB = fX;
} else if(3 <= fHPrime && fHPrime < 4) {
fR = 0;
fG = fX;
fB = fC;
} else if(4 <= fHPrime && fHPrime < 5) {
fR = fX;
fG = 0;
fB = fC;
} else if(5 <= fHPrime && fHPrime < 6) {
fR = fC;
fG = 0;
fB = fX;
} else {
fR = 0;
fG = 0;
fB = 0;
}

fR += fM;
fG += fM;
fB += fM;
}
*/

inline Color HueToRGB(int h)
{
	int s = 255, v = 255;
	int r, g, b;
	unsigned char region, remainder, p, q, t;

	//if (s == 0)
	//{
	//	r = v;
	//	g = v;
	//	b = v;
	//	return Color(r, g, b, a);
	//}

	region = h / 43;
	remainder = (h - (region * 43)) * 6;

	p = (v * (255 - s)) >> 8;
	q = (v * (255 - ((s * remainder) >> 8))) >> 8;
	t = (v * (255 - ((s * (255 - remainder)) >> 8))) >> 8;

	switch (region)
	{
	case 0:
		r = v; g = t; b = p;
		break;
	case 1:
		r = q; g = v; b = p;
		break;
	case 2:
		r = p; g = v; b = t;
		break;
	case 3:
		r = p; g = q; b = v;
		break;
	case 4:
		r = t; g = p; b = v;
		break;
	default:
		r = v; g = p; b = q;
		break;
	}

	return Color(r, g, b, 255);
}