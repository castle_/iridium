//#include "SuperProjectile.h"
//
//void SuperProjectile::Predict(float projvel)
//{
//
//	if (!gCvars.aimbot_projectiles)
//		return;
//
//	CBaseEntity* pLocal = gInts.EntList->GetClientEntity(me);
//	Vector myHead = pLocal->GetEyePosition();
//
//	projvel /= 66.666666666f;
//
//	for (int num = 1; num <= gInts.EntList->GetHighestEntityIndex(); num++) 
//	{
//		CBaseEntity *pEntity = gInts.EntList->GetClientEntity(num);
//
//		if (!pEntity || pEntity->IsDormant() || pEntity->GetLifeState() != LIFE_ALIVE || pEntity->GetTeamNum() == pLocal->GetTeamNum()/* || pEntity == lp*/)
//			continue;
//
//		int conds = pEntity->GetCond();
//		//int conds2 = pEntity->GetCondEx();
//
//		if (conds & TFCond_IgnoreStates/* || conds2 & TFCondEx_IgnoreStates*/)
//			continue;
//
//		classId id = (classId)pEntity->GetClientClass()->iClassID;
//
//		Vector currTarget, oldPos, oldVel;
//
//		if (id == classId::CTFPlayer)
//		{
//			//Vector currPos = gEntityCache.GetWSC(num, 0);
//			Vector currPos = pEntity->GetWorldSpaceCenter;
//
//			Vector Offsets[6];
//
//			Offsets[0] = Vector(0, 0, -20.0f);
//
//			Offsets[1]/* = nullvec*/;
//
//			for (int i = 2; i < 6; i++)
//				Offsets[i] = gEntityCache.GetTickBone(num, 0, i - 2) - currPos;
//
//			Vector vel = Vector(pEntity->GetVelocityX(), pEntity->GetVelocityY(), pEntity->GetVelocityZ()) / 66.666666666f;
//
//			float currGravity = gravity;
//
//			//int curCondEx2 = pEntity->GetCondEx2();
//				
//			//if (curCondEx2 & TFCondEx2_Parachute)
//			//	currGravity *= 0.02; //TODO: get real nu	mber
//
//			float maxGrav = currGravity*291.666666f;
//
//			for (int i = 0, total = 0; i < 170 && total < 172; i++, total++)
//			{
//				ticksToHit = (1 + gAngle.VecDistance(myWeapon, currPos) / projvel) + gHelper.GetPing() / 0.015; //1 is the minimal amount of lerp which is what I use, TODO: make REAL lerp detection
//
//																												//what we're doing here is making expensive tick calculations factoring in gravity and distance to ground and the like, but when we get within a tick of simulated accuracy
//																												//we use the last velocity we used to get a fairly reasonable exact position, then we vischeck it to see if we should progress further (like doing FOV checks)
//
//				if ((ticksToHit - i) > -1.0f && (ticksToHit - i) < 1.0f)
//				{
//					Vector absolutePos = currPos + (vel*(ticksToHit - i)); //
//
//					for (int b = 0; b < 6; b++)
//					{
//
//						Vector currBonePos = absolutePos + Offsets[b];
//
//						Ray.Init(myWeapon, (currBonePos + (currBonePos - myWeapon)*100.0f));
//						gInts.EngineTrace->TraceRay(Ray, MASK_AIMBOT | CONTENTS_HITBOX, &filterNoPlayer, &tr);
//
//						if (gAngle.VecDistance(tr.end, myWeapon) >= gAngle.VecDistance(currBonePos, myWeapon))
//						{
//							currTarget = currBonePos;
//							goto fin; //easy way to break two nested loops :/, as an alternative this part could be a function
//						}
//					}
//				}
//				Ray.Init(currPos, Vector(currPos.x, currPos.y, currPos.z - 10000.0f));
//				gInts.EngineTrace->TraceRay(Ray, MASK_PLAYERSOLID, &filterNoPlayer, &tr);
//
//				float distanceGround = currPos.z - tr.end.z;
//
//				if (distanceGround > 40.0f)
//					vel.z -= currGravity;
//				else if (distanceGround < 40.0f)
//				{
//					currPos.z = tr.end.z + 40.0f; //TODO: fix this shit
//					vel.z = 0;
//				}
//
//
//
//				if (vel.z < -maxGrav)
//					vel.z = -maxGrav;
//
//				//if (oldPos != nullvec)
//				if (/*oldPos*/ true)
//				{
//					Ray.Init(oldPos, (currPos + (currPos - oldPos)*100.0f));
//					gInts.EngineTrace->TraceRay(Ray, MASK_PLAYERSOLID, &filterNoPlayer, &tr);
//
//					if (gAngle.VecDistance(oldPos, tr.end) < gAngle.VecDistance(oldPos, currPos))
//					{ //we predicted into a wall, so we need to calculate a new velocity and recalculate the current tick again
//					  //this is a failed NCC wall prediction thing
//
//					  /* //pretty sure im supposed to be using 4 points not 2... TODO: fix
//					  Vector posDiff = currPos - oldPos;
//
//					  Vector x = posDiff;
//
//					  x.x = pow(x.x, 0.1);
//					  x.y = pow(x.y, 0.1);
//
//					  Vector y = posDiff;
//
//					  y.x = pow(y.x, 10);
//					  y.y = pow(y.y, 10);
//
//					  Ray.Init(oldPos, oldPos + (x*10000));
//					  gInts.Trace->TraceRay(Ray, MASK_PLAYERSOLID, &filterNoPlayer, &tr);
//
//
//					  Vector xEnd = tr.end;
//					  float xDist = gAngle.VecDistance(oldPos, xEnd);
//
//
//					  Ray.Init(oldPos, oldPos + (y*10000));
//					  gInts.Trace->TraceRay(Ray, MASK_PLAYERSOLID, &filterNoPlayer, &tr);
//
//					  Vector yEnd = tr.end;
//					  float yDist = gAngle.VecDistance(oldPos, yEnd);
//
//					  if (xDist > yDist)
//					  {
//					  x = (currPos - xEnd);
//					  vel = Vector(x.x, x.y, vel.z);
//					  }
//					  else
//					  {
//					  y = (currPos - yEnd);
//					  vel = Vector(y.x, y.y, vel.z);
//					  }
//
//					  float oldX = vel.x/100;
//					  float oldY = vel.y/100;
//
//					  while ( (vel.x + vel.y) > (oldVel.x + oldVel.y) )
//					  {
//					  vel.x -= oldX;
//					  vel.y -= oldY;
//					  }
//
//					  while ( (vel.x + vel.y) < (oldVel.x + oldVel.y) )
//					  {
//					  vel.x += oldX;
//					  vel.y += oldY;
//					  }*/
//
//						vel.x = 0;
//						vel.y = 0;
//
//						currPos = oldPos;
//						i--;
//					}
//				}
//
//
//				predictedTicks[num][i] = currPos;
//				oldPos = currPos;
//				oldVel = vel;
//				currPos += vel;
//			}
//		}
//		else if (id == classId::CObjectSentrygun || id == classId::CObjectDispenser || id == classId::CObjectTeleporter)
//		{
//			Vector min, max, origin;
//			min = pEntity->GetCollideableMins(); //BROKEN fix this shit
//			max = pEntity->GetCollideableMaxs();
//
//			origin = pEntity->GetAbsOrigin();
//
//			min /= 3;
//			max /= 3;
//
//			min += origin;
//			max += origin;
//
//
//			for (int i = 0; i < 8; i++)
//			{
//				filter.SetIgnoreSelf(lp);
//				filter.SetIgnoreEntity(pEntity);
//
//				switch (i)
//				{
//				case 0:
//					currTarget = Vector(min.x, min.y, min.z);
//					break;
//				case 1:
//					currTarget = Vector(min.x, min.y, max.z);
//					break;
//				case 2:
//					currTarget = Vector(min.x, max.y, min.z);
//					break;
//				case 3:
//					currTarget = Vector(min.x, max.y, max.z);
//					break;
//				case 4:
//					currTarget = Vector(max.x, min.y, min.z);
//					break;
//				case 5:
//					currTarget = Vector(max.x, min.y, max.z);
//					break;
//				case 6:
//					currTarget = Vector(max.x, max.y, min.z);
//					break;
//				case 7:
//					currTarget = Vector(max.x, max.y, max.z);
//					break;
//				}
//				Ray_t Ray;
//				trace_t tr;
//				Ray.Init(myHead, (currTarget + (currTarget - myHead) * 1000));
//				gInts.EngineTrace->TraceRay(Ray, MASK_AIMBOT | CONTENTS_HITBOX, &filter, &tr);
//
//				if (gAngle.VecDistance(tr.end, myHead) > (gAngle.VecDistance(currTarget, myHead)))
//					break;
//
//				currTarget = /*nullvec*/ Vector(0,0,0);
//			}
//		}
//
//		if (currTarget == Vector(0, 0, 0))
//			continue;
//
//	fin:
//
//		float currFov = gAngle.GetFov(myAngles, myHead, currTarget);
//
//		if (currFov < fov)
//		{
//			fov = currFov;
//			predictedTickCount = ticksToHit;
//			target = currTarget;
//			targEnt = pEntity;
//		}
//
//	}
//}