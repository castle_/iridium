#pragma once
#include "SDK.h"
#include "ccolor.h"

struct IntRect
{
	int x0;
	int y0;
	int x1;
	int y1;
	IntRect(int X0, int Y0, int X1, int Y1)
	{
		x0 = X0;
		y0 = Y0;
		x1 = X1;
		y0 = Y0;
	}
};
//struct Vertex_t
//{
//	Vertex_t() {}
//	Vertex_t(const Vector2D &pos, const Vector2D &coord = Vector2D(0, 0))
//	{
//		m_Position = pos;
//		m_TexCoord = coord;
//	}
//	void Init(const Vector2D &pos, const Vector2D &coord = Vector2D(0, 0))
//	{
//		m_Position = pos;
//		m_TexCoord = coord;
//	}
//
//	Vector2D	m_Position;
//	Vector2D	m_TexCoord;
//};

namespace Utils
{
	template<class T>
	inline constexpr const T& clamp(const T& v, const T& lo, const T& hi)
	{
		return (v >= lo && v <= hi) ? v : (v < lo ? lo : hi);
	}
	inline bool is_point_in_range(Vector2D point, Vector2D position, Vector2D size)
	{
		return (point.x >= position.x && point.x <= position.x + size.x)
			&& (point.y >= position.y && point.y <= position.y + size.y);
	}
	inline float GetFraction(float value, float min, float max)
	{
		float ratio = 1.f / (max - min);
		return ratio * (value - min);
	}
	inline float GetValueFromFraction(float fraction, float min, float max)
	{
		return ((max - min) * fraction) + min;
	}
	inline unsigned int GetNumberOfDigits(int i)
	{
		i = i < 0 ? -(i * 10) : i;
		return i > 0 ? (int)log10((double)i) + 1 : 1;
	}
	inline float RoundToDecimalPlaces(float value, int decimals)
	{
		float yeet = pow(10.f, decimals);
		return roundf(value * yeet) / yeet;
	}
	inline std::string FloatToString(float value, int decimals) // this shit is ugly as fOK
	{
		std::string ret = std::to_string(value);

		ret.erase(ret.begin() + clamp<unsigned int>(GetNumberOfDigits(value)
			+ decimals + 1, 0, ret.size()), ret.end());

		if (decimals <= 0)
			ret.pop_back();

		return ret;
	}
}

namespace SDK
{
	class IMaterial;
}
namespace RENDER
{
	inline unsigned int CreateF(std::string font_name, int size, int weight, int blur, int scanlines, int flags)
	{

	}
	inline void DrawF(int X, int Y, unsigned int Font, bool center_width, bool center_height, HFColor coloure, std::string Input)
	{
		Color clrxd = Color((int)coloure.RGBA[0], (int)coloure.RGBA[1], (int)coloure.RGBA[2], (int)coloure.RGBA[3]);
		gDrawManager.DrawStringFont(X, Y, clrxd, Input.c_str(), Font);
	}
	inline void DrawWF(int X, int Y, unsigned int Font, HFColor Color, const wchar_t* Input)
	{

	}
	inline Vector2D GetTextSize(unsigned int Font, std::string Input)
	{
		Vector2D aaaa = Vector2D(14, 14);
		return aaaa;
	}

	inline void DrawLine(int x1, int y1, int x2, int y2, HFColor color)
	{
		Color clrxd = Color((int)color.RGBA[0], (int)color.RGBA[1], (int)color.RGBA[2], (int)color.RGBA[3]);
		gDrawManager.DrawLine(x1, y1, x2, y2, clrxd);
	}

	inline void DrawGradientRectangle(Vector2D Position, Vector2D Size, HFColor Top, HFColor Bottom)
	{
		Color clrxd = Color((int)Bottom.RGBA[0], (int)Bottom.RGBA[1], (int)Bottom.RGBA[2], (int)Bottom.RGBA[3]);
		gDrawManager.DrawRect(Position.x, Position.y, Size.x, Size.y, clrxd);
	}

	inline void DrawEmptyRect(int x1, int y1, int x2, int y2, HFColor color, unsigned char = 0)
	{
		Color clrxd = Color((int)color.RGBA[0], (int)color.RGBA[1], (int)color.RGBA[2], (int)color.RGBA[3]);
		gInts.Surface->DrawSetColor(clrxd.r(), clrxd.g(), clrxd.b(), clrxd.a());
		gInts.Surface->DrawFilledRect(x1, y1, x2, y2);
	}
	inline void DrawFilledRect(int x1, int y1, int x2, int y2, HFColor color)
	{
		Color clrxd = Color((int)color.RGBA[0], (int)color.RGBA[1], (int)color.RGBA[2], (int)color.RGBA[3]);
		gInts.Surface->DrawSetColor(clrxd.r(), clrxd.g(), clrxd.b(), clrxd.a());
		gInts.Surface->DrawFilledRect(x1, y1, x2, y2);
	}
	inline void FillRectangle(int x1, int y2, int width, int height, HFColor color)
	{
		Color clrxd = Color((int)color.RGBA[0], (int)color.RGBA[1], (int)color.RGBA[2], (int)color.RGBA[3]);
		gDrawManager.DrawRect(x1, y2, width, height, clrxd);
	}
	inline void DrawFilledRectOutline(int x1, int y1, int x2, int y2, HFColor color)
	{
		Color clrxd = Color((int)color.RGBA[0], (int)color.RGBA[1], (int)color.RGBA[2], (int)color.RGBA[3]);
		gInts.Surface->DrawSetColor(clrxd.r(), clrxd.g(), clrxd.b(), clrxd.a());
		gInts.Surface->DrawFilledRect(x1, y1, x2, y2);
	}
	inline void DrawFilledRectArray(IntRect* rects, int rect_amount, HFColor color)
	{

	}
	inline void DrawCornerRect(const int32_t x, const int32_t y, const int32_t w, const int32_t h, const bool outlined, const HFColor& color, const HFColor& outlined_color)
	{

	}
	inline void DrawEdges(float topX, float topY, float bottomX, float bottomY, float length, HFColor color)
	{

	}

	inline void DrawCircle(int x, int y, int radius, int segments, HFColor color)
	{

	}
	inline void DrawFilledCircle(int x, int y, int radius, int segments, HFColor color)
	{

	}
	//void DrawFilledCircle(Vector2D center, CColor color, CColor outline, float radius, float points);

	inline void TexturedPolygon(int n, std::vector<Vertex_t> vertice, HFColor color)
	{

	}
	inline void DrawSomething()
	{

	}

	inline bool WorldToScreen(Vector world, Vector &screen)
	{
		return gDrawManager.WorldToScreen(world, screen);
	}
}