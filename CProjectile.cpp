#include "CProjectile.h"

CProjectile gProj;

Vector CProjectile::ProjectilePrediction(CBaseEntity* ent, CBaseEntity* local, int hb, float gravitymod, float entgmod) 
{
	float speed;
	CBaseCombatWeapon *wep = local->GetActiveWeapon();
	classId id = (classId)wep->GetClientClass()->iClassID;

	float BeginCharge;
	float Charge;

	//return flSpeed;
	switch (id)
	{
	case classId::CTFGrenadeLauncher:
		speed = 840.0f;
	case classId::CTFRocketLauncher_DirectHit:
		speed = 1980.0f;
	case classId::CTFRocketLauncher:
	case classId::CTFRocketLauncher_Mortar:
		speed = 1100.0f;
	case classId::CTFFlareGun:
		speed = 1450.0f;
	case classId::CTFBat_Wood:
		speed = 1940.0f;
	case classId::CTFSyringeGun:
		speed = 990.0f;
	case classId::CTFCompoundBow:
	{
		BeginCharge = wep->GetDemoChargeTime();
		Charge = BeginCharge == 0.0f ? 0.0f : gInts.g_pGlobals->curtime - BeginCharge;
		if (Charge > 1.0f)
			Charge = 1.0f;
		speed = (800 * Charge + 1800);
	}
	default:
		speed = -1.0f;
	}

	if (!ent) return Vector();
	//Vector result = SimpleLatencyPrediction(ent, hb);
	Vector result = ent->GetHitboxPosition(hb);
	if (gCvars.aimbot_advanced_feet_aim)
		result = ent->GetAbsOrigin();
	if (speed == 0.0f) return Vector();
	float dtg = DistanceToGround(ent);
	Vector vel = Util->EstimateAbsVelocity(ent);
	float medianTime = local->GetEyePosition().DistTo(result) / speed;
	float range = 1.5f;
	float currenttime = medianTime - range;
	if (currenttime <= 0.0f) currenttime = 0.01f;
	float besttime = currenttime;
	float mindelta = 65536.0f;
	Vector bestpos = result;
	int maxsteps = 300;
	for (int steps = 0; steps < maxsteps; steps++, currenttime += ((float)(2 * range) / (float)maxsteps))
	{
		Vector curpos = result;
		curpos += vel * currenttime;
		if (dtg > 0.0f)
		{
			curpos.z -= currenttime * currenttime * 400 * entgmod;
			if (curpos.z < result.z - dtg) curpos.z = result.z - dtg;
		}
		float rockettime = local->GetEyePosition().DistTo(curpos) / speed;
		if (fabs(rockettime - currenttime) < mindelta)
		{
			besttime = currenttime;
			bestpos = curpos;
			mindelta = fabs(rockettime - currenttime);
		}
	}
	bestpos.z += (400 * besttime * besttime * gravitymod);
	return bestpos;
}

Vector CProjectile::SimpleLatencyPrediction(CBaseEntity* ent, int hb)
{
	if (!ent) return Vector();
	Vector result;
	if (gCvars.aimbot_advanced_feet_aim)
		result = ent->GetAbsOrigin();
	else
		result = ent->GetHitboxPosition(hb);
	//float latency = gInts.Engine->GetNetChannelInfo()
	//float latency = gInts.Engine->GetNetChannelInfo()->GetLatency(FLOW_OUTGOING) +
		//gInts.Engine->GetNetChannelInfo()->GetLatency(FLOW_INCOMING);
	Vector vel = Util->EstimateAbsVelocity(ent);
	//result += vel * latency;
	return result += vel;
}

//Vector CProjectile::EstimateAbsVelocity(CBaseEntity *ent)
//{
//	typedef void(__thiscall * EstimateAbsVelocityFn)(CBaseEntity*, Vector &);
//
//	static DWORD dwFn = gSignatures.GetEngineSignature("E8 ? ? ? ? F3 0F 10 4D ? 8D 85 ? ? ? ? F3 0F 10 45 ? F3 0F 59 C9 56 F3 0F 59 C0 F3 0F 58 C8 0F 2F 0D ? ? ? ? 76 07") + 0x1;
//
//	static DWORD dwEstimate = ((*(PDWORD)(dwFn)) + dwFn + 4);
//
//	EstimateAbsVelocityFn vel = (EstimateAbsVelocityFn)dwEstimate;
//
//	Vector v;
//
//	vel(ent, v);
//
//	return v;
//}

float CProjectile::DistanceToGround(CBaseEntity* ent)
{
	if (ent->GetFlags() & FL_ONGROUND) return 0;

	Vector& origin = ent->GetVecOrigin();
	float v1 = VectorialDistanceToGround(origin + Vector(10.0f, 10.0f, 0.0f));
	float v2 = VectorialDistanceToGround(origin + Vector(-10.0f, 10.0f, 0.0f));
	float v3 = VectorialDistanceToGround(origin + Vector(10.0f, -10.0f, 0.0f));
	float v4 = VectorialDistanceToGround(origin + Vector(-10.0f, -10.0f, 0.0f));
	return MIN(v1, MIN(v2, MIN(v3, v4)));
}

float CProjectile::VectorialDistanceToGround(Vector origin)
{
	static trace_t* ground_trace = new trace_t();
	Ray_t ray;
	Vector endpos = origin;
	endpos.z -= 8192;
	ray.Init(origin, endpos);
	gInts.EngineTrace->TraceRay(ray, MASK_PLAYERSOLID, NULL, ground_trace);
	return 8192.0f * ground_trace->fraction;
}
//yes!!!