#include "SDK.h"
#include "Color.h"
#include "ccolor.h"
//class CPlayerVariables
//{
//public:
//	void find();
//
//	DWORD m_lifeState;
//	DWORD m_fFlags;
//	DWORD m_iHealth;
//	DWORD m_iTeamNum;
//	DWORD m_iClass;
//	DWORD m_nPlayerCond;
//	DWORD m_Shared;
//	DWORD m_Collision;
//	DWORD m_hActiveWeapon;
//	DWORD m_iWeaponState;
//	DWORD m_vecViewOffset;
//	DWORD m_vecOrigin;
//	DWORD m_nForceTauntCam;
//	DWORD deadflag;
//	DWORD m_bReadyToBackstab;
//};

class CGlobalVariables
{
public:
	const bool cade_menu = true;

	int iAimbotIndex = -1;
	Color clrRadar_Cross = Color::Black();
	HFColor menu_color = HFColor(20, 20, 20, 255);

	float settings_switch;
	float iMenu_Pos_X = 300;
	float iMenu_Pos_Y = 75;
	float szsubstest;

	bool m4rida_menu = 0;
	
	bool aimbot_active;
	int aimbot_fov = 180;
	int aimbot_key;
	int aimbot_target;
	bool aimbot_silent;
	bool aimbot_smooth;
	int aimbot_smooth_amt;
	bool aimbot_canshoot;
	bool aimbot_hitscan;
	bool aimbot_projectiles;
	bool aimbot_ignore_cloak;
	int aimbot_hitbox;
	bool aimbot_autoshoot;
	bool aimbot_minigun_toggle = 1;
	bool aimbot_latency_pred;
	bool aimbot_advanced_feet_aim;
	bool aimbot_wait_for_headshot = 1;

	bool triggerbot_active;
	int triggerbot_key;
	bool triggerbot_headonly;
	//
	bool triggershoot_active;
	int triggershoot_key;
	bool triggershoot_backstab;
	bool triggershoot_airblast_enable;
	bool triggershoot_airblast_idk;// dont config this
	int triggershoot_airblast_fov = 180;
	int triggershoot_airblast_distance = 90;
	bool triggershoot_airblast_silent;




	bool esp_switch;
	bool esp_active;
	int esp_pos;
	bool esp_enemyonly;
	bool esp_visonly;
	bool esp_vischeck;
	bool esp_snapline;
	int esp_box = 1;
	int esp_name = 1;
	bool steamid;
	bool friendsid;
	bool friendsname;
	bool esp_class = 1;
	int esp_health = 2;
	bool esp_conds;
	int esp_bones = 1;
	bool esp_skybox;
	bool anti_disguise;
	//
	bool esp_buildings;
	bool esp_buildings_enemyonly;
	bool esp_buildings_name;
	bool esp_buildings_level;
	int esp_buildings_box;
	int esp_buildings_health;

	bool esp_glow;
	bool esp_glow_enemy_only;
	bool esp_glow_buildings;
	bool esp_glow_items;
	//
	bool esp_items;
	bool DrawAimFov;
	bool visual_chams; 
	bool visual_wireframe;
	bool visual_backtrack_chams;
	int visual_hands;
	int visual_weapons;
	int visual_players;
	bool visual_enemyonly;
	bool visual_noscope;
	bool visual_nozoom;
	bool visual_noscope_crosshair;
	//
	bool visual_rainbow;
	bool visual_custom_menu_color;
	bool visual_sky_changer;
	int visual_colors = 0;
	int visual_menu_hue = 0;
	int visual_red_hue = 0;
	int visual_blu_hue = 150;
	int visual_aim_hue = 90;
	int visual_friend_hue = 105;
	int visual_rage_hue = 60;
	int visual_arms_hue = 0;
	bool visual_arms_shine;
	bool radar_active;
	bool radar_enemyonly;
	int radar_x = 120;
	int radar_y = 120;
	int radar_size = 120;
	int radar_range = 1500;
	int radar_alpha = 128;
	int radar_style = 1;
	bool radar_health = 1;
	bool radar_class_portraits = 1;
	int radar_icon_background = 1;
	int radar_icon_size = 24;
	bool radar_health_debug;

	bool hvh_aa_active;
	int hvh_aa_pitch;
	int hvh_aa_yaw;
	int hvh_aa_spin_speed = 10;
	int hvh_resolver;
	bool hvh_resolver_rageonly;
	bool hvh_resolver_realangles;
	bool hvh_speedhack;
	int hvh_speedhack_key;
	int hvh_speedhack_amt;

	bool misc_switch;
	bool misc_bunnyhop;
	bool misc_thirdperson;
	bool misc_sv_cheats;
	bool misc_autostrafe;
	bool misc_noisemaker_spam;
	bool misc_tournament_spam;
	bool misc_remove_cond;
	int misc_remove_cond_key;
	int misc_remove_cond_amt = 700;
	bool misc_insta_decloak;
	bool misc_double_shoot;
	bool misc_sticky_spam;
	int misc_customfov = 90; //todo config
	bool misc_lag_exploit;
	bool misc_lag_exploit2;
	int misc_lag_exploit_key;
	int misc_lag_exploit_value = 50;
	int misc_lag_exploit_method;
	bool misc_sv_pure = 1;
	bool misc_antismac;
	bool misc_chat_spam;
	int misc_chat_spam_newline_amt;
	bool misc_backtracking;
	int bt_maxticks = 14;
	int backtrack_tick = 0;
	bool misc_skinchanger;
	bool drawicontest;
	bool misc_killsay_enabled;
	int  misc_killsay_type;
	int  misc_killsay_newline_amt;

	bool gay_menu_open;

	int iWidth, iHeight;

	float playerlist_switch;
	int PlayerMode[64] = {
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	};

	char* CHATSPAM[5] = {
		"Iridium - VAC undetected TF2 hack!",
		"Iridium - ez 1taps",
		"Iridium - Never lose again!",
		"Iridium - Owning servers!",
		"Iridium - High demand!",
	};

	Vector vLastPrediction;
};

class COffsets
{
public:
	int iKeyEventOffset = 20, iCreateMoveOffset = 21, iPaintTraverseOffset = 41, iFrameStageNotify = 35, iDrawModelExecuteOffset = 19, iOverrideViewOffset = 16;
};