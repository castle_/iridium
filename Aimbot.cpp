#include "Aimbot.h"
#include "CProjAim.h"
#include "Util.h"

CAimbot gAim;
CUtil gUtil;

Vector CAimbot::calc_angle(Vector src, Vector dst)
{
	Vector AimAngles, delta;
	float hyp;
	delta = src - dst;
	hyp = sqrtf((delta.x * delta.x) + (delta.y * delta.y));
	AimAngles.x = atanf(delta.z / hyp) * RADPI;
	AimAngles.y = atanf(delta.y / delta.x) * RADPI;
	AimAngles.z = 0.0f;
	if (delta.x >= 0.0)
		AimAngles.y += 180.0f;
	return AimAngles;
}
void CAimbot::MakeVector(Vector angle, Vector& vector)
{
	float pitch, yaw, tmp;
	pitch = float(angle[0] * PI / 180);
	yaw = float(angle[1] * PI / 180);
	tmp = float(cos(pitch));
	vector[0] = float(-tmp * -cos(yaw));
	vector[1] = float(sin(yaw)*tmp);
	vector[2] = float(-sin(pitch));
}
float CAimbot::GetFOV(Vector angle, Vector src, Vector dst)
{
	Vector ang, aim;
	float mag, u_dot_v;
	ang = calc_angle(src, dst);


	MakeVector(angle, aim);
	MakeVector(ang, ang);

	mag = sqrtf(pow(aim.x, 2) + pow(aim.y, 2) + pow(aim.z, 2));
	u_dot_v = aim.Dot(ang);

	return RAD2DEG(acos(u_dot_v / (pow(mag, 2))));
}

void CAimbot::Run(CBaseEntity* pLocal, CUserCmd* pCommand)
{
	gCvars.iAimbotIndex = -1;

	CBaseEntity* pEntity = GetBaseEntity(GetBestTarget(pLocal, pCommand));

	if (!pLocal)
		return;	 

	if (!pEntity)
		return;

	CBaseCombatWeapon* pLocalWep = pLocal->GetActiveWeapon();

	if (!pLocalWep)
		return;

	int iItemDef = pLocalWep->GetItemDefinitionIndex();

	if (!aimbot_active.value)
		return;

	if (!aimbot_key.KeyDown(aimbot_key_alt.value))
		return;

	int iBestHitbox = GetBestHitbox(pLocal, pEntity);

	if (iBestHitbox == -1)
		return;

	if (pLocal->GetClassNum() == TF2_Sniper && pLocal->GetActiveWeapon()->GetSlot() == 0)
	{
		if (aimbot_scoped_only.value && !(pLocalWep->GetSniperChargeDamage() > 0))
			return;

		if (aimbot_wait_for_charge.value && Util->IsHeadshotWeapon(pLocal, pLocal->GetActiveWeapon()))
		{
			if (pEntity->GetHealth() > 150)
			{
				if ((pLocalWep->GetSniperChargeDamage() * 3) < pEntity->GetHealth())
					return;
			}
		}
	}

	Vector vEntity;
	Vector vAngs = pCommand->viewangles;
	Vector vLocal = pLocal->GetEyePosition();

	if (Projectile::GetProjectileSpeed(pLocal->GetActiveWeapon()) != -1.f)
	{
		vEntity = Projectile::ProjectilePrediction(pEntity, pLocal, aimbot_position.value - 1);

		if (!Util->AssumeVis(pLocal, pLocal->GetVecOrigin() + pLocal->GetAbsEyePosition(), vEntity))
			return;
	} else
	{
		vEntity = (pEntity->GetHitboxPosition(iBestHitbox) + gUtil.EstimateAbsVelocity(pEntity) * gInts.g_pGlobals->interval_per_tick) - (gUtil.EstimateAbsVelocity(pLocal) * gInts.g_pGlobals->interval_per_tick);
	}

	VectorAngles((vEntity - vLocal), vAngs);
	ClampAngle(vAngs);
	gUtil.SilentMovementFix(pCommand, vAngs);
	pCommand->viewangles = vAngs;

	gCvars.iAimbotIndex = pEntity->GetIndex();

	if (aimbot_slowaim.value && !aimbot_silent.value)
	{
		Vector vDelta(pCommand->viewangles - vAngs);
		AngleNormalize(vDelta);
		vAngs = pCommand->viewangles - vDelta / aimbot_slow_amt.value;
	}

	if (aimbot_autoshoot.value)
	{
		if (iItemDef == (int)sniperweapons::WPN_Huntsman ||
			iItemDef == (int)sniperweapons::WPN_FestiveHuntsman ||
			iItemDef == (int)sniperweapons::WPN_CompoundBow)
			pCommand->buttons &= ~IN_ATTACK;
		else
			pCommand->buttons |= IN_ATTACK;
	}

	if (!aimbot_silent.value)
		gInts.Engine->SetViewAngles(vAngs);
}

int CAimbot::GetBestTarget(CBaseEntity* pLocal, CUserCmd* pCommand)
{
	int iBestTarget = -1;
	int bestTarget = -1;
	float flDistToBest = 99999.f;

	Vector vLocal = pLocal->GetEyePosition();

	for (int i = 1; i <= gInts.Engine->GetMaxClients(); i++)
	{
		CBaseEntity* pEntity = GetBaseEntity(i);

		if (!pEntity)
			continue;

		if (pEntity == pLocal)
			continue;

		if (pEntity->IsDormant())
			continue;

		if (pEntity->GetLifeState() != LIFE_ALIVE)
			continue;

		if (pEntity->GetTeamNum() == pLocal->GetTeamNum())
			continue;

		if (pEntity->IsDev())
			continue;

		if (pEntity->GetCond() & TFCond_Ubercharged ||
			pEntity->GetCond() & TFCond_UberchargeFading ||
			pEntity->GetCond() & TFCond_Bonked)
			continue;

		if (pEntity->GetCond() & TFCond_Cloaked && aimbot_ignore_cloak.value)
			continue;

		Vector vEntity = pEntity->GetVecOrigin();

		float flFOV = GetFOV(pCommand->viewangles, vLocal, vEntity);

		if (flFOV < flDistToBest && flFOV < aimbot_fov.value)
		{
			flDistToBest = flFOV;
			gCvars.iAimbotIndex = i;
			bestTarget = i;
		}

	}

	return bestTarget;
}

int CAimbot::GetBestHitbox(CBaseEntity* pLocal, CBaseEntity* pEntity)
{
	int iBestHitbox = -1;

	if (!gAim.aimbot_position.value)
	{
		if (Util->IsHeadshotWeapon(pLocal, pLocal->GetActiveWeapon()))
			iBestHitbox = 0;
		else
			iBestHitbox = 4;
	}
	else
	{
		iBestHitbox = gAim.aimbot_position.value - 1;
	}

	if (gAim.aimbot_hitscan.value)
	{
		for (int i = 0; i < 17; i++)
		{
			if (Util->IsVisible(pLocal, pEntity, pLocal->GetEyePosition(), pEntity->GetHitboxPosition(i)))
				return i;
		}
	}

	if (pEntity->GetHitboxPosition(iBestHitbox).IsZero())
		return -1;

	if (!Util->IsVisible(pLocal, pEntity, pLocal->GetEyePosition(), pEntity->GetHitboxPosition(iBestHitbox)))
		return -1;

	return iBestHitbox;
}