#include "Radar.h"
#include "Util.h"
#include "Aimbot.h"
#include "Triggerbot.h"
void CRadar::Run()
{
	if (!radar_active.value)
		return;

	DrawRadarBack();

	CBaseEntity* pLocal = gInts.EntList->GetClientEntity(me);

	for (int i = 1; i <= gInts.Engine->GetMaxClients(); i++)
	{
		CBaseEntity* pEntity = GetBaseEntity(i);

		if (radar_active.value)
		{
			if (!pEntity)
				continue;

			//if (pEntity == pLocal)
			//	continue;

			if (pEntity->IsDormant())
				continue;

			if (pEntity->GetLifeState() != LIFE_ALIVE)
				continue;

			if (radar_enemy_only.value && pEntity != pLocal && pEntity->GetTeamNum() == pLocal->GetTeamNum() && gCvars.PlayerMode[pEntity->GetIndex()])
				continue;

			Vector b = pLocal->GetAbsAngles();
			QAngle a = QAngle(b.x, b.y, b.z);
			gRadar.DrawRadarPoint(pEntity->GetAbsOrigin(), pLocal->GetAbsOrigin(), a, pEntity, gDraw.GetPlayerColor(pEntity, 255));
		}
	}

	/// Soon:tm:
	//for (int i = 0; i <= gInts.EntList->GetHighestEntityIndex(); i++)
	//{
	//	CBaseEntity* pEntity = GetBaseEntity(i);
	//	if (!pEntity) continue;
	//	if (pEntity == pLocal) continue;
	//	if (pEntity->IsDormant()) continue;
	//	if (gCvars.radar_enemyonly && pEntity != pLocal && pEntity->GetTeamNum() == pLocal->GetTeamNum()) continue;

	//}
}

void CRadar::DrawRadarPoint(Vector vOriginX, Vector vOriginY, QAngle qAngle, CBaseEntity *pBaseEntity, Color dwTeamColor, bool entity, char* textIfEntity)
{
	CBaseEntity* pLocalEntity = GetBaseEntity(me);

	if (pLocalEntity == NULL)
		return;

	//if (gCvars.radar_style == 3)
	//{
	//	if (pBaseEntity->GetTeamNum() == 2) //RED
	//		dwTeamColor = 
	//}


	float flDeltaX = vOriginX.x - vOriginY.x;
	float flDeltaY = vOriginX.y - vOriginY.y;

	float flAngle = qAngle.y;

	float flYaw = (flAngle)* (3.14159265358979323846 / 180.0);
	float flMainViewAngles_CosYaw = cos(flYaw);
	float flMainViewAngles_SinYaw = sin(flYaw);

	// rotate
	float x = flDeltaY * (-flMainViewAngles_CosYaw) + flDeltaX * flMainViewAngles_SinYaw;
	float y = flDeltaX * (-flMainViewAngles_CosYaw) - flDeltaY * flMainViewAngles_SinYaw;

	float flRange = radar_range.value;

	if (fabs(x) > flRange || fabs(y) > flRange)
	{
		// clipping
		if (y > x)
		{
			if (y > -x)
			{
				x = flRange * x / y;
				y = flRange;
			}
			else
			{
				y = -flRange * y / x;
				x = -flRange;
			}
		}
		else
		{
			if (y > -x)
			{
				y = flRange * y / x;
				x = flRange;
			}
			else
			{
				x = -flRange * x / y;
				y = -flRange;
			}
		}
	}

	int	iScreenX = radar_x.value + int(x / flRange * radar_size.value);
	int iScreenY = radar_y.value + int(y / flRange * radar_size.value);

	int sz = radar_icn_size.value;
	//gDrawManager.DrawRect(iScreenX - 3, iScreenY - 3, 7, 7, Color(0, 0, 0, 255));
	if (entity)
		gDraw.DrawStringCenter(iScreenX - 3, iScreenY - 3, dwTeamColor, textIfEntity);
	else
	{
		if (radar_icon_bg.value)
		{
			Color clrIconBackground;

			if (radar_icon_bg.value == 1) clrIconBackground = Color::Black();
			else clrIconBackground = dwTeamColor;

			gDraw.DrawRect(iScreenX - sz / 2, iScreenY - sz / 2, sz, sz, clrIconBackground);

		}

		gDraw.DrawTexturedRect(gDraw.GetTextureIDFromEntity(pBaseEntity, radar_portraits.value), iScreenX - sz/2, iScreenY - sz/2, sz, sz);

		if (pBaseEntity == pLocalEntity)
			gDraw.OutlineRect(iScreenX - sz / 2, iScreenY - sz / 2, sz, sz, gCvars.clrRadar_Cross);
		else if (radar_icon_bg.value == 1)
			gDraw.OutlineRect(iScreenX - sz / 2, iScreenY - sz / 2, sz, sz, gDraw.GetPlayerColor(pBaseEntity));

		if (radar_health.value)
		{
			int iHp = pBaseEntity->GetHealth(), iMaxHp = pBaseEntity->GetMaxHealth();
			if (iHp > iMaxHp)
				iHp = iMaxHp;

			gDraw.OutlineRect(iScreenX - (sz/2) - 1, iScreenY + sz/2 - 1, sz, 5, Color::Black());
			float lmao = float(sz) * float(float(iHp) / float(iMaxHp));
			gDraw.DrawRect(iScreenX - (sz / 2), iScreenY + sz/2, lmao, 3, redGreenGradiant(pBaseEntity->GetHealth(), pBaseEntity->GetMaxHealth(), 255));
		}
	}
}
//===================================================================================
void CRadar::DrawRadarBack()
{
	int iCenterX = radar_x.value;
	int iCenterY = radar_y.value;
	int iSize = radar_size.value;
	int iSubstract = 0;

	Color clrCross, clrBorder;

	switch (radar_style.value)
	{
	case 0: //DARKSTORM
	{
		clrCross = Color(255, 120, 0, 255);
		clrBorder = Color(0, 0, 0, 0);
		iSubstract = 10;
	}
		break;
	case 1: //LITHIUM
	{
		CBaseEntity *pLocal = GetBaseEntity(me);
		if (pLocal)
			clrBorder = clrCross = gDraw.GetPlayerColor(pLocal, 255, true);
		else
			clrBorder = clrCross = Color(255, 0, 0, 255);
		iSubstract = 0;
	}
		break;
	case 2: //LMAOBOX
	{
		CBaseEntity *pLocal = GetBaseEntity(me); //I THINK THERE'S A BUG WITH THE COLOR ON THIS ONE BUT I'M NOT SURE IT ONLY HAPPENED ONCE
		if (pLocal)
		{
			if (pLocal->GetTeamNum() == 2) //RED
			{
				clrBorder = Color(211, 70, 70, 255);
				clrCross = Color(221, 80, 80, 255);
			}
			else if (pLocal->GetTeamNum() == 3) //BLU
			{
				clrBorder = Color(0, 128, 255, 255);
				clrCross = Color(10, 138, 255, 255);
			}
		}
		else
		{
			clrBorder = Color(211, 70, 70, 255);
			clrCross = Color(221, 80, 80, 255);
		}
		iSubstract = 30;
	}
	break;
	case 3: //NULLCORE
	{
		if (gAim.aimbot_active.value && gAim.aimbot_key.KeyDown())
			clrBorder = Color(200, 40, 200, 255); //AIMKEY COLOR
		else
			clrBorder = Color(15, 150, 150, 255);

		if (gTrigger.triggerbot_active.value && gTrigger.triggerbot_key.KeyDown())
			clrCross = Color(200, 40, 200, 255); //TRIGGERKEY COLOR
		else
			clrCross = Color(15, 150, 150, 255);
		iSubstract = 0;
	}
	break;
	default:
		break;
	}
	if (gCvars.visual_colors == 3)
		clrCross = clrBorder = HueToRGB(gCvars.visual_menu_hue);

	gCvars.clrRadar_Cross = clrCross;

	gDraw.DrawRect(iCenterX - iSize, iCenterY - iSize, 2 * iSize + 2, 2 * iSize + 2, Color(30, 30, 30, radar_alpha.value));
	gDraw.OutlineRect(iCenterX - iSize, iCenterY - iSize, 2 * iSize + 2, 2 * iSize + 2, clrBorder);
	//iSize -= iSubstract;

	gDraw.DrawRect(iCenterX, iCenterY - (iSize - iSubstract), 1, 2 * (iSize - iSubstract), clrCross);
	gDraw.DrawRect(iCenterX - (iSize - iSubstract), iCenterY, 2 * (iSize - iSubstract), 1, clrCross);

}


CRadar gRadar;