#pragma once
#include "SDK.h"
#include "Controls.h"

class CTriggerbot
{
public:
	Checkbox triggerbot_active = Checkbox("Active");
	Checkbox triggerbot_headonly = Checkbox("Head Only");
	Checkbox triggershoot_active = Checkbox("Active");
	Checkbox triggershoot_autobackstab = Checkbox("Auto Backstab");

	KeyBind triggerbot_key = KeyBind("Trigger Bot Key", VK_XBUTTON1);
	Listbox triggerbot_key_alt = Listbox("Trigger Bot Key", { "None", "Mouse1", "Mouse2", "Mouse3", "Mouse4", "Mouse5", "Shift", "Alt", "F" }, VK_XBUTTON1);

	void Triggerbot_Run(CBaseEntity* pLocal, CUserCmd* pCommand);
	void Triggershoot_Run(CBaseEntity* pLocal, CUserCmd* pCommand);

};

extern CTriggerbot gTrigger; 