#include "NetVars.h"
#include "SDK.h"
#include <utility>

NetPlayer nPlayer;
NetWeapon nWeapon;

void NetPlayer::init()
{
	netvar_tree nv;

	m_nForceTauntCam = nv.get_offset("DT_TFPlayer", "m_nForceTauntCam");
	m_bReadyToBackstab = nv.get_offset("DT_TFWeaponKnife", "m_bReadyToBackstab");
	m_iFOV = nv.get_offset("DT_BasePlayer", "m_iFOV");
	m_flFOVTime = nv.get_offset("DT_BasePlayer", "m_flFOVTime");

	m_vecViewOffset = nv.get_offset("DT_BasePlayer", "localdata", "m_vecViewOffset[0]");
	m_vecOrigin = nv.get_offset("DT_BaseEntity", "m_vecOrigin");
	deadflag = nv.get_offset("DT_BasePlayer", "pl", "deadflag");

	m_hActiveWeapon = nv.get_offset("DT_BaseCombatCharacter", "m_hActiveWeapon");
	m_hMyWeapons = nv.get_offset("DT_BaseCombatCharacter", "m_hMyWeapons");

	m_angEyeAngles = nv.get_offset("DT_TFPlayer", "tfnonlocaldata", "m_angEyeAngles[0]");
}

void NetWeapon::init()
{
	netvar_tree nv;

	m_iItemDefinitionIndex = nv.get_offset("DT_EconEntity", "m_AttributeManager", "m_Item", "m_iItemDefinitionIndex");
}

/**
* netvar_tree - Constructor
*
* Call populate_nodes on every RecvTable under client->GetAllClasses()
*/
netvar_tree::netvar_tree()
{
	const auto *client_class = gInts.Client->GetAllClasses();
	while (client_class != nullptr) {
		const auto class_info = std::make_shared<node>(0);
		auto *recv_table = client_class->Table;
		populate_nodes(recv_table, &class_info->nodes);
		nodes.emplace(recv_table->GetName(), class_info);

		client_class = client_class->pNextClass;
	}
}

/**
* populate_nodes - Populate a node map with brances
* @recv_table:	Table the map corresponds to
* @map:	Map pointer
*
* Add info for every prop in the recv table to the node map. If a prop is a
* datatable itself, initiate a recursive call to create more branches.
*/
void netvar_tree::populate_nodes(RecvTable *recv_table, map_type *map)
{
	for (auto i = 0; i < recv_table->GetNumProps(); i++) {
		const auto *prop = recv_table->GetProp(i);
		const auto prop_info = std::make_shared<node>(prop->GetOffset());

		if (prop->GetType() == DPT_DataTable)
			populate_nodes(prop->GetDataTable(), &prop_info->nodes);

		map->emplace(prop->GetName(), prop_info);
	}
}