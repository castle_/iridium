#include "Misc.h"
#include "Util.h"
#include "ConVar.h"
#include "FrameStageNotify.h"
#include "CBacktracking.h"
#include "CTimeShift.h"
#include "CAntiAim.h"
#include "CDrawManager.h"
#include "netmessage.h"
#include "Aimbot.h"
#include "ESP.h"

CAntiAim gAntiAim;

float jitterangs = 0;
bool bFlip = 0;
int teamname = 0;
CMisc gMisc;
float lastTime;
int iSpamIndex;

void CMisc::Run(CBaseEntity* pLocal, CUserCmd* pCommand, bool* bSendPacket)
{
	//Bhop
	if (!(pLocal->GetFlags() & FL_ONGROUND) && pCommand->buttons & IN_JUMP)
	{
		//Autostrafe	
		if (misc_astrafe.value)
			if (pCommand->mousedx > 1 || pCommand->mousedx < -1)  //> 1 < -1 so we have some wiggle room
				pCommand->sidemove = pCommand->mousedx > 1 ? 450.f : -450.f;

		//Bunnyhop
		if (misc_bhop.value)
			pCommand->buttons &= ~IN_JUMP;
	}

	//Time Shift
	//gTShift.Run(pLocal, pCommand);

	//Dark sky or whatever
	static auto sv_skyname = gInts.Cvar->FindVar("sv_skyname");
	if (gCvars.esp_skybox)
	{
		sv_skyname->SetValue("sky_night_01");
	}

	//Chat Spam
	if (misc_chatspam.value &&
		lastTime + 0.5f < gInts.g_pGlobals->curtime)
	{
		if (iSpamIndex < 4)
			iSpamIndex += 1;
		else
			iSpamIndex = 0;

		char cmd[256];
		strcpy(cmd, "say \xE0\xB9\x8A ");
		for (int i = 0; i < misc_newlines.value; i++)
			strcat(cmd, "\x0D");
		strcat(cmd, gCvars.CHATSPAM[iSpamIndex]);
		lastTime = gInts.g_pGlobals->curtime;
		gInts.Engine->ClientCmd_Unrestricted(cmd);
	}

	//Anti-Aim
	gAntiAim.Run(pLocal, pCommand, bSendPacket);

	//minigun toggle
	if (gAim.aimbot_minigun_toggle.value && gAim.aimbot_active.value && pLocal->szGetClass() == "Heavy" && gAim.aimbot_key.KeyDown())
		pCommand->buttons |= IN_ATTACK2;

	//thirdperson
	*(bool*)(pLocal + nPlayer.m_nForceTauntCam) = misc_tp.value;

	//sv_cheats
	if (misc_cheats.value)
	{
		ConVar* sv_cheats = gInts.Cvar->FindVar("sv_cheats");
		if (sv_cheats->GetInt() == 0) sv_cheats->SetValue(1);
	}

	//tournament spam
	if (misc_tourn.value)
	{
		///This will get you kicked for command spamming, add proper delay later lo
		//if (pCommand->tick_count % 2)
		//	gInts.Engine->ClientCmd("tournament_teamname ez");
		//else
		//	gInts.Engine->ClientCmd("tournament_teamname pz");
	}

	//lag exploit
	//if (gCvars.misc_lag_exploit && Util->IsKeyPressed(gCvars.misc_lag_exploit_key))
	//{
	//	char* cmd;
	//	gCvars.misc_lag_exploit_method ? cmd = "voicemenu 0 0" : cmd = "use";
	//	for (int i = 0; i < gCvars.misc_lag_exploit_value; i++) 
	//		gInts.Engine->ServerCmd(cmd);
	//}

	//Anti-SMAC
	if (misc_smac.value && !(pCommand->buttons & IN_ATTACK))
	{
		VectorClear(pCommand->viewangles);
		Util->SilentMovementFix(pCommand, pCommand->viewangles);
	}

	//noisemaker spam
	if (misc_noisem.value)
	{
		PVOID kv = Util->InitKeyValue();
		if (kv != NULL)
		{
			NoisemakerSpam(kv);
			gInts.Engine->ServerCmdKeyValues(kv);
		}
	}
}
		  //Could be much simpler, but I don't want keyvals class
void CMisc::NoisemakerSpam(PVOID kv) //Credits gir https://www.unknowncheats.me/forum/team-fortress-2-a/141108-infinite-noisemakers.html
{
	char chCommand[30] = "use_action_slot_item_server";
	typedef int(__cdecl* HashFunc_t)(const char*, bool);
	static DWORD dwHashFunctionLocation = gSignatures.GetClientSignature("FF 15 ? ? ? ? 83 C4 08 89 06 8B C6");
	static HashFunc_t s_pfGetSymbolForString = (HashFunc_t)**(PDWORD*)(dwHashFunctionLocation + 0x2);
	*(PDWORD)((DWORD)kv + 0x4) = 0;
	*(PDWORD)((DWORD)kv + 0x8) = 0;
	*(PDWORD)((DWORD)kv + 0xC) = 0;
	*(PDWORD)((DWORD)kv + 0x10) = /*0x10000*/0xDEADBEEF;
	*(PDWORD)((DWORD)kv + 0x14) = 0;
	*(PDWORD)((DWORD)kv + 0x18) = 0; //Extra one the game isn't doing, but if you don't zero this out, the game crashes.
	*(PDWORD)((DWORD)kv + 0x1C) = 0;
	*(PDWORD)((DWORD)kv + 0) = s_pfGetSymbolForString(chCommand, 1);
}

void CMisc::FSN(CBaseEntity* you, ClientFrameStage_t curStage)
{
	CBaseEntity* pLocal = GetBaseEntity(me);
	bool bIsThirdPerson = *(bool*)(pLocal + nPlayer.m_nForceTauntCam);

	if (bIsThirdPerson && curStage == FRAME_RENDER_START)
	{
			// Basic clamping to show what you look like to others
			// Remove or properly clamp to show real head pos
		if (!gAntiAim.aa_real_local_angs.value)
		{
			if (lastAngles.x > 89)
				lastAngles.x = 89;
			else if (lastAngles.x < -89)
				lastAngles.x = -89;
		}
			*(Vector*)(you + nPlayer.deadflag + 0x4) = lastAngles;
	}

	if ((gESP.visual_nozoom.value > 1) 
		&& curStage == FRAME_RENDER_START)
	{
		int m_flFOVRate = 0xE5C; //:thought:
		int &fovPtr = *(int*)(pLocal + gNetVars.get_offset("DT_BasePlayer", "m_iFOV")), defaultFov = *(int*)(pLocal + gNetVars.get_offset("DT_BasePlayer", "m_iDefaultFOV"));
		if (gMisc.misc_fov.value > 90)
			defaultFov = gMisc.misc_fov.value;
		fovPtr = defaultFov;
		*(float*)(pLocal + m_flFOVRate) = 0;
	}

	if (curStage == FRAME_RENDER_START) //https://aixxe.net/2017/01/source-internal-glow
	{
		for (int index = 0; index < gInts.glowobjectmanager->m_GlowObjectDefinitions.m_Size; index++) {
			GlowObjectDefinition_t& glowobject = gInts.glowobjectmanager->m_GlowObjectDefinitions[index];

			if (glowobject.m_nNextFreeSlot != ENTRY_IN_USE)
				continue;

			//CBaseEntity* entity = glowobject.m_hEntity;
			CBaseEntity* entity = static_cast<CBaseEntity*>(gInts.EntList->GetClientEntityFromHandle(glowobject.m_hEntity));

			switch (entity->GetClientClass()->iClassID)
			{
				case (int)classId::CTFPlayer:
				{
						Color clrEntity = gDraw.GetPlayerColor(entity);
						glowobject.m_vGlowColor = Vector((float)clrEntity.r() / 255.f, (float)clrEntity.g() / 255.f, (float)clrEntity.b() / 255.f);
				}
				break;
				case (int)classId::CObjectSentrygun:
				case (int)classId::CObjectTeleporter:
				case (int)classId::CObjectDispenser:
				case (int)classId::CBaseAnimating:
					glowobject.m_vGlowColor = Vector(1.f, 1.f, 1.f);
					break;
			}
		}

		for (int index = 1; index < gInts.EntList->GetHighestEntityIndex(); index++) {
			CBaseEntity* entity = static_cast<CBaseEntity*>(gInts.EntList->GetClientEntity(index));

			if (!entity || index == gInts.Engine->GetLocalPlayer() || entity->IsDormant())
				continue;

			bool skip = gESP.esp_glow_enemy_only.value && entity->GetTeamNum() == pLocal->GetTeamNum();

			bool bMeme = false;
			if (gESP.esp_glow_items.value) // WARNING: idk wtf this is
			{
				const char* pszModelName = gInts.ModelInfo->GetModelName(entity->GetModel());
				if (streql(pszModelName, "models/items/medkit_small.mdl") || streql(pszModelName, "models/items/medkit_small_bday.mdl") || streql(pszModelName, "models/props_halloween/halloween_medkit_small.mdl")
					|| streql(pszModelName, "models/items/ammopack_small.mdl") || streql(pszModelName, "models/items/ammopack_small_bday.mdl")
					|| streql(pszModelName, "models/items/medkit_medium.mdl") || streql(pszModelName, "models/items/medkit_medium_bday.mdl") || streql(pszModelName, "models/props_halloween/halloween_medkit_medium.mdl")
					|| streql(pszModelName, "models/items/ammopack_medium.mdl") || streql(pszModelName, "models/items/ammopack_medium_bday.mdl")
					|| streql(pszModelName, "models/items/medkit_large.mdl") || streql(pszModelName, "models/items/medkit_large_bday.mdl") || streql(pszModelName, "models/props_halloween/halloween_medkit_large.mdl")
					|| streql(pszModelName, "models/items/ammopack_large.mdl") || streql(pszModelName, "models/items/ammopack_large_bday.mdl")
					|| streql(pszModelName, "models/items/plate.mdl") || streql(pszModelName, "models/items/plate_steak.mdl") || streql(pszModelName, "models/items/plate_robo_sandwich.mdl")
					|| streql(pszModelName, "models/items/currencypack_small.mdl") || streql(pszModelName, "models/items/currencypack_medium.mdl") || streql(pszModelName, "models/items/currencypack_large.mdl"))
					bMeme = true;
			}

			switch (entity->GetClientClass()->iClassID) {
			case (int)classId::CTFPlayer:
				entity->SetGlowEnabled(!entity->IsDormant() && entity->GetLifeState() == LIFE_ALIVE && gESP.esp_glow.value && !skip);
			//case (int)classId::CObjectSentrygun:
			//case (int)classId::CObjectTeleporter:
			//case (int)classId::CObjectDispenser:
			//	entity->SetGlowEnabled(!entity->IsDormant() && entity->GetLifeState() == LIFE_ALIVE && gESP.esp_glow.value && gCvars.esp_glow_buildings && !skip);
			//case (int)classId::CBaseAnimating:
			//	entity->SetGlowEnabled(!entity->IsDormant() && entity->GetLifeState() == LIFE_ALIVE && bMeme);
				break;
			}


		}
	}


	//Resolver!!
	if (gAntiAim.aa_resolver.value && curStage == FRAME_NET_UPDATE_POSTDATAUPDATE_START)
	{

		for (int i = 1; i <= gInts.Engine->GetMaxClients(); i++)
		{
			if (i == me)
				continue;

			CBaseEntity* current = GetBaseEntity(i);

			if (!current || current->IsDormant())
				continue;

			if (pLocal->GetTeamNum() == current->GetTeamNum())
				continue;

			if (gAntiAim.aa_resolver_rageonly.value && gCvars.PlayerMode[current->GetIndex()] != 2)
				continue;

			Vector* angles = (Vector*)(current + nPlayer.m_angEyeAngles);
			switch (gAntiAim.aa_resolver.value)
			{
			case 1: //Auto
			{
				if (angles->x >= 90)
					angles->x = -88;
				else if (angles->x <= -90)
					angles->x = 89;
			}
			break;
			case 2: //Force Up
				angles->x = -89;
				break;
			case 3: //Force Down
				angles->x = 89;
				break;
			default:
				break;
			}
		}
	}

}