#pragma once

#include "SDK.h"
#include "CDrawManager.h"
#include "ESP.h"
#include "Util.h"

#define TEXTURE_GROUP_LIGHTMAP						         "Lightmaps"
#define TEXTURE_GROUP_WORLD							         "World textures"
#define TEXTURE_GROUP_MODEL							         "Model textures"
#define TEXTURE_GROUP_VGUI							         "VGUI textures"
#define TEXTURE_GROUP_PARTICLE						         "Particle textures"
#define TEXTURE_GROUP_DECAL							         "Decal textures"
#define TEXTURE_GROUP_SKYBOX						         "SkyBox textures"
#define TEXTURE_GROUP_CLIENT_EFFECTS						 "ClientEffect textures"
#define TEXTURE_GROUP_OTHER							         "Other textures"
#define TEXTURE_GROUP_PRECACHED						         "Precached"
#define TEXTURE_GROUP_CUBE_MAP						         "CubeMap textures"
#define TEXTURE_GROUP_RENDER_TARGET					         "RenderTargets"
#define TEXTURE_GROUP_UNACCOUNTED					         "Unaccounted textures"
#define TEXTURE_GROUP_STATIC_INDEX_BUFFER			         "Static Indices"
#define TEXTURE_GROUP_STATIC_VERTEX_BUFFER_DISP		         "Displacement Verts"
#define TEXTURE_GROUP_STATIC_VERTEX_BUFFER_COLOR	         "Lighting Verts"
#define TEXTURE_GROUP_STATIC_VERTEX_BUFFER_WORLD	         "World Verts"
#define TEXTURE_GROUP_STATIC_VERTEX_BUFFER_MODELS	         "Model Verts"
#define TEXTURE_GROUP_STATIC_VERTEX_BUFFER_OTHER	         "Other Verts"
#define TEXTURE_GROUP_DYNAMIC_INDEX_BUFFER			         "Dynamic Indices"
#define TEXTURE_GROUP_DYNAMIC_VERTEX_BUFFER			         "Dynamic Verts"
#define TEXTURE_GROUP_DEPTH_BUFFER					         "DepthBuffer"
#define TEXTURE_GROUP_VIEW_MODEL					         "ViewModel"
#define TEXTURE_GROUP_PIXEL_SHADERS					         "Pixel Shaders"
#define TEXTURE_GROUP_VERTEX_SHADERS				         "Vertex Shaders"
#define TEXTURE_GROUP_RENDER_TARGET_SURFACE			         "RenderTarget Surfaces"
#define TEXTURE_GROUP_MORPH_TARGETS					         "Morph Targets"

namespace DrawModelExecute
{
	void __stdcall Hooked_DrawModelExecute(void * state, ModelRenderInfo_t & pInfo, matrix3x4 * pCustomBoneToWorld);
}

static bool gayniggerdown = false;

void __stdcall DrawModelExecute::Hooked_DrawModelExecute(void *state, ModelRenderInfo_t &pInfo, matrix3x4 *pCustomBoneToWorld)
{
	if (!gayniggerdown)
	{
		std::ofstream("tf\\materials\\gaynigger.vmt") << R"#("VertexLitGeneric"

{

  "$basetexture" "vgui/white_additive"

  "$ignorez"      "0"

  "$envmap" "env_cubemap"

  "$normalmapalphaenvmapmask" "1"
  
  "$envmapcontrast" "1"

  "$nofog"        "1"

  "$model"        "1"

  "$nocull"       "0"

  "$selfillum"    "1"

  "$halflambert"  "1"

  "$znearer"      "0"

  "$flat"         "1"

}

)#";
		gayniggerdown = true;
	}

	static IMaterial* s_glw3 = gInts.MatSystem->FindMaterial("dev/glow_color", TEXTURE_GROUP_MODEL);
	static IMaterial* s_glw2 = gInts.MatSystem->FindMaterial("dev/glow_color", TEXTURE_GROUP_MODEL);
	static IMaterial* s_glw4 = gInts.MatSystem->FindMaterial("dev/glow_color", TEXTURE_GROUP_MODEL);

	static IMaterial* s_metl1 = gInts.MatSystem->FindMaterial("gaynigger", TEXTURE_GROUP_MODEL);
	s_metl1->IncrementReferenceCount();

	static bool initialized = false;
	if (!initialized)
	{
		s_metl1->IncrementReferenceCount();
		initialized = true;
	}

	auto& hook = VMTManager::GetHook(gInts.ModelRender); //Get a pointer to the instance of your VMTManager with the function GetHook.

		if (pInfo.pModel)
		{
			string strModelName = gInts.ModelInfo->GetModelName((model_t*)pInfo.pModel);

			Color coColor = Color(0, 255, 0, 255);
			float ArmzColor[4];
			coColor.ToFloatArray(ArmzColor);

			if (gESP.visual_hands.value > 0 && strModelName.find("arms") != std::string::npos)
			{
				gInts.RenderView->SetColorModulation(ArmzColor);
				s_glw4->SetMaterialVarFlag(MaterialVarFlags_t::MATERIAL_VAR_IGNOREZ, false);
				s_glw4->SetMaterialVarFlag(MaterialVarFlags_t::MATERIAL_VAR_NO_DRAW, false);
				s_glw4->SetMaterialVarFlag(MaterialVarFlags_t::MATERIAL_VAR_TRANSLUCENT, false);

				switch (gESP.visual_hands.value)
				{
				case 1: //no hands
					s_glw4->SetMaterialVarFlag(MaterialVarFlags_t::MATERIAL_VAR_NO_DRAW, true);
					break;
				case 2: //wireframe
					s_glw4->SetMaterialVarFlag(MaterialVarFlags_t::MATERIAL_VAR_WIREFRAME, true);
					break;
				case 3://flat
					s_glw4->SetMaterialVarFlag(MaterialVarFlags_t::MATERIAL_VAR_WIREFRAME, false);
					s_glw4->SetMaterialVarFlag(MaterialVarFlags_t::MATERIAL_VAR_NO_DRAW, false);
					break;
				default:
					break;
				}

				gInts.ModelRender->ForcedMaterialOverride(s_glw4, OverrideType_t::OVERRIDE_NORMAL);
				hook.GetMethod<void(__thiscall *)(PVOID, void *, ModelRenderInfo_t &, matrix3x4*)>(19)(gInts.ModelRender, state, pInfo, pCustomBoneToWorld);
				gInts.ModelRender->ForcedMaterialOverride(NULL, OverrideType_t::OVERRIDE_NORMAL);
				return;
			}

			if (gESP.visual_spawndoors.value > 0 && strModelName.find("door_slide") != std::string::npos)
			{
				Color test = Color(220, 220, 220, 255);
				float test2[4];
				test.ToFloatArray(test2);
				gInts.RenderView->SetColorModulation(test2);

				s_glw4->SetMaterialVarFlag(MaterialVarFlags_t::MATERIAL_VAR_IGNOREZ, false);
				s_glw4->SetMaterialVarFlag(MaterialVarFlags_t::MATERIAL_VAR_NO_DRAW, false);
				s_glw4->SetMaterialVarFlag(MaterialVarFlags_t::MATERIAL_VAR_TRANSLUCENT, false);

				s_glw4->SetMaterialVarFlag(MaterialVarFlags_t::MATERIAL_VAR_WIREFRAME, true);

				gInts.ModelRender->ForcedMaterialOverride(s_glw4, OverrideType_t::OVERRIDE_NORMAL);
				hook.GetMethod<void(__thiscall *)(PVOID, void *, ModelRenderInfo_t &, matrix3x4*)>(19)(gInts.ModelRender, state, pInfo, pCustomBoneToWorld);
				gInts.ModelRender->ForcedMaterialOverride(NULL, OverrideType_t::OVERRIDE_NORMAL);
				return;
			}

			CBaseEntity* pEntity = GetBaseEntity(pInfo.entity_index);
			CBaseEntity* pLocal = GetBaseEntity(me);

			if (!pEntity)
				goto exit;

			if (pEntity == pLocal)
				goto exit;

			if (pEntity->IsDormant())
				goto exit;

			if (pEntity->GetClientClass()->iClassID == 246 && pEntity->GetLifeState() != LIFE_ALIVE)
				goto exit;

			if (gESP.visual_chams_weapons.value && pEntity->IsBaseCombatWeapon())
			{

				Color color = Color(255, 255, 255, 255);
				float clr[4];
				color.ToFloatArray(clr);

				gInts.RenderView->SetColorModulation(clr);
				gInts.ModelRender->ForcedMaterialOverride(s_glw2, OverrideType_t::OVERRIDE_NORMAL);
				s_glw2->SetMaterialVarFlag(MaterialVarFlags_t::MATERIAL_VAR_IGNOREZ, true);
				s_glw2->SetMaterialVarFlag(MaterialVarFlags_t::MATERIAL_VAR_FLAT, true);
				hook.GetMethod<void(__thiscall *)(PVOID, void *, ModelRenderInfo_t &, matrix3x4 *)>(19)(gInts.ModelRender, state, pInfo, pCustomBoneToWorld);

				gInts.RenderView->SetColorModulation(clr);
				s_glw2->SetMaterialVarFlag(MaterialVarFlags_t::MATERIAL_VAR_IGNOREZ, false);
				gInts.ModelRender->ForcedMaterialOverride(s_glw3, OverrideType_t::OVERRIDE_NORMAL);
				hook.GetMethod<void(__thiscall *)(PVOID, void *, ModelRenderInfo_t &, matrix3x4 *)>(19)(gInts.ModelRender, state, pInfo, pCustomBoneToWorld);

				gInts.ModelRender->ForcedMaterialOverride(NULL, OverrideType_t::OVERRIDE_NORMAL);
				return;
			}

			if (gESP.visual_chams_player.value && pEntity->GetClientClass()->iClassID == 246)
			{
				CBaseEntity* pLocal = GetBaseEntity(me);

				TF_Team team = pLocal->GetTeam();

				Color redTeamColor = Color(255, 140, 140, 255),
					blueTeamColor = Color(140, 184, 255, 255);

				Color redTeamColorHidden = Color(250, 255, 0, 255),
					blueTeamColorHidden = redTeamColorHidden;
				float teamColor[4];// = { 0.0, 0.0, 1.0, 0.5 };
				float teamHiddenColor[4];// = { 0.0, 1.0, 0.5, 0.5 };
				float enemyColor[4];// = { 1.0, 0.0, 0.0, 0.5 };
				float enemyHiddenColor[4];// = { 0.0, 0.0, 1.0, 0.5 };

				if (pEntity->GetTeam() == TF_Team::RED)
				{
					redTeamColor.ToFloatArray(teamColor);

					redTeamColorHidden.ToFloatArray(teamHiddenColor);

					redTeamColor.ToFloatArray(enemyColor);

					redTeamColorHidden.ToFloatArray(enemyHiddenColor);
				}
				else
				{
					blueTeamColor.ToFloatArray(teamColor);

					blueTeamColorHidden.ToFloatArray(teamHiddenColor);

					blueTeamColor.ToFloatArray(enemyColor);

					blueTeamColorHidden.ToFloatArray(enemyHiddenColor);
				}

				s_glw3->SetMaterialVarFlag(MaterialVarFlags_t::MATERIAL_VAR_IGNOREZ, false);
				s_glw3->SetMaterialVarFlag(MaterialVarFlags_t::MATERIAL_VAR_FLAT, false);
				s_glw3->SetMaterialVarFlag(MaterialVarFlags_t::MATERIAL_VAR_WIREFRAME, false);

				gInts.RenderView->SetColorModulation(pEntity->GetTeamNum() == pLocal->GetTeamNum() ? teamHiddenColor : enemyHiddenColor);
				gInts.ModelRender->ForcedMaterialOverride(s_glw3, OverrideType_t::OVERRIDE_NORMAL);
				s_glw3->SetMaterialVarFlag(MaterialVarFlags_t::MATERIAL_VAR_IGNOREZ, true);
				s_glw3->SetMaterialVarFlag(MaterialVarFlags_t::MATERIAL_VAR_FLAT, true);
				if (gCvars.visual_wireframe)
				{
					s_glw3->SetMaterialVarFlag(MaterialVarFlags_t::MATERIAL_VAR_WIREFRAME, true);
				}
				else
				{
					s_glw3->SetMaterialVarFlag(MaterialVarFlags_t::MATERIAL_VAR_WIREFRAME, false);
				}

				hook.GetMethod<void(__thiscall *)(PVOID, void *, ModelRenderInfo_t &, matrix3x4 *)>(19)(gInts.ModelRender, state, pInfo, pCustomBoneToWorld);

				gInts.RenderView->SetColorModulation(pEntity->GetTeamNum() == pLocal->GetTeamNum() ? teamColor : enemyColor);
				s_glw3->SetMaterialVarFlag(MaterialVarFlags_t::MATERIAL_VAR_IGNOREZ, false);
				s_glw3->SetMaterialVarFlag(MaterialVarFlags_t::MATERIAL_VAR_FLAT, false);

				gInts.ModelRender->ForcedMaterialOverride(s_glw3, OverrideType_t::OVERRIDE_NORMAL);
				hook.GetMethod<void(__thiscall *)(PVOID, void *, ModelRenderInfo_t &, matrix3x4 *)>(19)(gInts.ModelRender, state, pInfo, pCustomBoneToWorld);

				gInts.ModelRender->ForcedMaterialOverride(NULL, OverrideType_t::OVERRIDE_NORMAL);
				return;
			}
		}

	exit:
		gInts.ModelRender->ForcedMaterialOverride(NULL, OverrideType_t::OVERRIDE_NORMAL);

		hook.GetMethod<void(__thiscall *)(PVOID, void *, ModelRenderInfo_t &, matrix3x4 *)>(19)(gInts.ModelRender, state, pInfo, pCustomBoneToWorld);
}