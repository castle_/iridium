#pragma once
#include "CDrawManager.h"
#include "SDK.h"
#include <string>
#include <vector>
#include <Windows.h>
#include "Settings.h"
#include "XorString.h"

enum class MouseBtn
{
	null,
	lclick,
	rclick,
	ldown,
	rdown
};

class GUI
{
public:
	void Draw();
	void createGUI();
	bool bMenuActive = false;
	WNDPROC windowProc;

	MouseBtn mb = MouseBtn::null;

private:
	//bool alwaystrue = true; //test
	int optionspacing = 170;
	POINT Proportions{ 770, 370 }; // menu width and height; previous: 770, 250
	POINT mouse{ 0, 0 };
	POINT pmouse{ 0, 0 };
	POINT menupos{ 200, 200 }; //starting position of the menu

	int iTabHeight = 40, iTabWidth = 100;
	int iItemSpacing = 18;

	void getInput();
	bool mouseOver(int x, int y, int w, int h);

	enum class e_type
	{
		null,
		tab,
		boolean,
		integer,
		rightbool,
		rightint,
		emptyL,
		emptyR
	};
	struct Item
	{
		const char* name;
		e_type type = e_type::null;

		bool* bValue = nullptr;
		int* iValue = nullptr;
		int min = 0, max = 1, step = 1;
		bool* bRight = nullptr;
	};
	vector<Item> itemlist;


	bool addTab(const char* name, bool &value);
	void addBool(const char* name, bool &value);
	void addInt(const char* name, int &value, int min, int max, int step);
	void addRightBool(const char* name, bool &value);
	void addRightInt(const char* name, int &value, int min, int max, int step);
	void addEmptyL();
	void addEmptyR();
	string ReplaceInts(Item itemlist);
};

extern GUI gGUI;