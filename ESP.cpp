#include "ESP.h"
#include "CDrawManager.h"
#include "Util.h"
#include "Radar.h"
#include <string>
#include "CBacktracking.h"

CESP gESP;

void CESP::Run()
{
	CBaseEntity* pLocal = gInts.EntList->GetClientEntity(me);

	if (!pLocal)
		return;

	if (!esp_enabled.value)
		return;

	if (gCvars.visual_noscope_crosshair && pLocal->GetCond() & TFCond_Zoomed)
	{
		gDraw.DrawLine(gScreenSize.iScreenWidth / 2, 0, gScreenSize.iScreenWidth / 2, gScreenSize.iScreenHeight, Color::Black());
		gDraw.DrawLine(0, gScreenSize.iScreenHeight / 2, gScreenSize.iScreenWidth, gScreenSize.iScreenHeight / 2, Color::Black());
	}

	bool bIsThirdPerson = Util->IsThirdPerson(pLocal); //idk

	for (int i = 0; i <= gInts.Engine->GetMaxClients(); i++)
	{
		CBaseEntity* pEntity = GetBaseEntity(i);

		if (!pEntity)
			continue;

		//if (i == me && pEntity->GetUserId() == )

		if (i == me && !bIsThirdPerson)
			continue;

		if (pEntity->IsDormant())
			continue;

		if (pEntity->GetLifeState() != LIFE_ALIVE)
			continue;

		if (esp_enemy_only.value && pEntity->GetTeamNum() == pLocal->GetTeamNum() && i != me && gCvars.PlayerMode[pEntity->GetIndex()])
			continue;

		///Add backtracking ESP later when I add backtracking into the menu 'cause I forgot about it
		//if (gCvars.misc_backtracking && pEntity->GetTeamNum() != pLocal->GetTeamNum() && i != me)
		//{
		//	for (int j = 0; j < gCvars.bt_maxticks; ++j)
		//	{
		//		Vector vecScreenTicks[24][14];
		//		if (gDraw.WorldToScreen(btHeadPositions[i][j].hitboxpos, vecScreenTicks[i][j]))
		//			gDraw.DrawString(vecScreenTicks[i][j].x, vecScreenTicks[i][j].y, Color::White(), "+");
		//	}
		//}

		Player_ESP(pLocal, pEntity);
	}

	if (!gCvars.esp_buildings && !esp_items.value)
		return;

	for (int i = 0; i <= gInts.EntList->GetHighestEntityIndex(); i++)
	{
		CBaseEntity* pCurEnt = gInts.EntList->GetClientEntity(i);

		if (!pCurEnt)
			continue;

		if (pCurEnt->IsDormant())
			continue;

		Building_ESP(pLocal, pCurEnt);
		Item_ESP(pLocal, pCurEnt);
	}
}
void CESP::CustomSkybox()
{
	

}
void CESP::Player_ESP(CBaseEntity* pLocal, CBaseEntity* pEntity)
{
	if (esp_visible_only.value)
	{
		bool vis = false;
		for (int i = 0; i <= 17; i++)
		{
			if (Util->IsVisible(pLocal, pEntity, pLocal->GetEyePosition(), pEntity->GetHitboxPosition(i)))
				vis = true;
		}
		if (!vis)
			return;
	}

	player_info_t pInfo;
	if (!gInts.Engine->GetPlayerInfo(pEntity->GetIndex(), &pInfo))
		return;
	
	char szName[256];
	strcpy(szName, pInfo.name);
	wchar_t szBuf[256];
	MultiByteToWideChar(CP_UTF8, 0, szName, 256, szBuf, 256); //This is pUnicode support

	float x, y, w, h;
	if (!gDraw.GetEntityBounds(pEntity, x, y, w, h))
		return;

	int alpha = 255;
	if (esp_vis_check.value)
	{
		bool vis = false;
		for (int i = 0; i <= 17; i++)
		{
			if (Util->IsVisible(pLocal, pEntity, pLocal->GetEyePosition(), pEntity->GetHitboxPosition(i)))
				vis = true;
		}
		if (!vis)
			alpha = 60;
	}

	auto conds = pEntity->GetCond();

	Color clrPlayerCol = gDraw.GetPlayerColor(pEntity, alpha);
	if (conds & TFCond_Cloaked)
		clrPlayerCol = Color(127, 127, 127, 100);

	if (esp_disguise.value && (conds & TFCond_Disguised))
		pEntity->SetCond(conds & ~TFCond_Disguised);
	
	Color clrBoneCol = esp_bones.value == 1 ? Color::White() : esp_bones.value == 2 ? redGreenGradiant(pEntity->GetHealth(), pEntity->GetMaxHealth(), alpha) : clrPlayerCol;
	int iY = 0;
	if (esp_position.value)
	{
		iY = h + gDraw.GetESPHeight();
		//TODO: Make it so it aligns with the left too
	}

	//iHp is only for health bar
	int iHp = pEntity->GetHealth(), iMaxHp = pEntity->GetMaxHealth();
	if (iHp > iMaxHp)
		iHp = iMaxHp;

	gDraw.DrawBoxOnEntity(esp_boxes.value, pEntity, clrPlayerCol);
	
	//TODO: Add bottom screen snaplines, not like it matters anyways since nobody except mike uses them
	if (esp_snaplines.value)
	{
		Vector origin = pEntity->GetAbsOrigin();
		Vector feet;
		gDraw.WorldToScreen(origin, feet);
		gDraw.DrawLine(gCvars.iWidth/2, gCvars.iHeight/2, feet.x, feet.y, clrPlayerCol);
	}

	if (esp_health.value == 2 || esp_health.value == 3)
	{
		gDraw.OutlineRect(x - 6, y - 1, 5, h, Color::Black());

		gDraw.DrawRect(x - 5, y + (h - (h / iMaxHp * iHp)) - 1, 3, h / iMaxHp * iHp, redGreenGradiant(pEntity->GetHealth(), pEntity->GetMaxHealth(), alpha));
	}

	switch (esp_name.value)
	{
	case 0:
		break;
	case 1: //normal
		gDraw.DrawString(x + w + 2, y + iY, clrPlayerCol, szName);
		iY += gDraw.GetESPHeight();
		break;
	case 2: //special
		gDraw.DrawStringCenterFont(x + (w / 2), y - 14, Color::White(), szName, gESP.visual_aimware.value ? gFonts.consolas : gFonts.aimware);
	default:
		break;
	}

	//This is also missing from the menu lol
	if (esp_class.value)
	{
		gDraw.DrawString(x + w + 2, y + iY, clrPlayerCol, "%s", pEntity->szGetClass());
		iY += gDraw.GetESPHeight();
	}

	if (esp_health.value == 1 || esp_health.value == 3)
	{
		gDraw.DrawString(x + w + 2, y + iY, redGreenGradiant(pEntity->GetHealth(), pEntity->GetMaxHealth()), "%d / %d HP", pEntity->GetHealth(), pEntity->GetMaxHealth(), alpha);
		iY += gDraw.GetESPHeight();
	}

	//TODO: add esp_steamid variable
	if (false)
	{
		gDraw.DrawString(x + w + 2, y + iY, clrPlayerCol, pEntity->GetUserId());
		iY += gDraw.GetESPHeight();
	}

	if (esp_conditions.value)
	{
#define DRAWCOND(a) {gDraw.DrawString(x + w + 2, y + iY, Color(15, 150, 150, 255), a); iY += gDraw.GetESPHeight();}

		switch (conds)
		{
		case TFCond_Cloaked: DRAWCOND("** CLOAKED **"); break;
		case TFCond_Ubercharged: DRAWCOND("** UBERED **"); break;
		case TFCond_DeadRingered: DRAWCOND("** DEAD RINGER **"); break;
		case TFCond_Disguising: DRAWCOND("** DISGUISE **"); break;
		case TFCond_Overhealed: DRAWCOND("** OVERHEAL **"); break;
		case TFCond_Slowed: DRAWCOND("** SLOWED **"); break;
		case TFCond_Zoomed: DRAWCOND("** ZOOMED **"); break;
		case TFCond_Kritzkrieged: DRAWCOND("** KRITZ **"); break;
		case TFCond_MarkedForDeath: DRAWCOND("** MARKED **"); break;
		case TFCond_OnFire: DRAWCOND("** FIRE **"); break;
		case TFCond_MiniCrits: DRAWCOND("** MINICRIT **"); break;
		case TFCondEx_BulletResistance: DRAWCOND("** BULLET RESIST **"); break;
		default:
			break;
		}
		//if (conds & TFCond_Cloaked)
		//	gDrawManager.DrawString(x + w + 2, y + iY, Color(15, 150, 150, 255), "** CLOAK **");
		//if (conds & TFCond_Ubercharged || conds & TFCond_UberchargeFading)
		//	gDrawManager.DrawString(x + w + 2, y + iY, Color(15, 150, 150, 255), "** UBER **");
		//if (conds & TFCond_DeadRingered)
		//	pText = "** DEAD RINGER **";
		//if (conds & TFCond_Disguising)
		//	pText = "** DISGUISED **";
		//if (conds & TFCond_Overhealed)
		//	pText = "** OVERHEAL **";
		//if (conds & TFCond_Slowed)
		//	pText = "** SLOWED **";
		//if (conds & TFCond_Zoomed)
		//	pText = "** ZOOMED **";
		//if (conds & TFCond_Kritzkrieged)
		//	pText = "** KRITZ **";
		//if (conds & TFCond_MarkedForDeath)
		//	pText = "** MARKED **";
		//if (conds & TFCond_OnFire)
		//	pText = "** FIRE **";
		//if (conds & TFCond_MiniCrits)
		//	pText = "** MINI CRITS **";
		//if (conds & TFCondEx_BulletResistance)
		//	pText = "** BULLET RESIST **";
		//if (conds & TFCondEx_ExplosiveResistance)
		//	pText = "** BLAST RESIST **";
		///TODO: Add more conds, and maybe add the ability for them to stack
		//if (pText)
		//{
		//	gDrawManager.DrawString(x + w + 2, y + iY, Color(15, 150, 150, 255), pText);
		//	iY += gDrawManager.GetESPHeight();
		//}

	}

	if (esp_bones.value) //bones
	{
		static int iLeftArmBones[] = { 8, 7, 6, 4 };
		static int iRightArmBones[] = { 11, 10, 9, 4 };
		static int iHeadBones[] = { 0, 4, 1 };
		static int iLeftLegBones[] = { 14, 13, 1 };
		static int iRightLegBones[] = { 17, 16, 1 };

		DrawBone(pEntity, iLeftArmBones, 4, clrBoneCol);
		DrawBone(pEntity, iRightArmBones, 4, clrBoneCol);

		DrawBone(pEntity, iHeadBones, 3, clrBoneCol);

		DrawBone(pEntity, iLeftLegBones, 3, clrBoneCol);
		DrawBone(pEntity, iRightLegBones, 3, clrBoneCol);
	}
}


void CESP::Building_ESP(CBaseEntity* pLocal, CBaseEntity* pEntity)
{
	//Forgot to add buildings to the menu also
	if (!gCvars.esp_buildings)
		return;

	if (gCvars.esp_buildings_enemyonly && pLocal->GetTeamNum() == pEntity->GetTeamNum())
		return;

	string sCurName = pEntity->GetClientClass()->chName;

	if (sCurName == "CTFPlayer")
		return;

	if (sCurName != "CObjectSentrygun"
		&& sCurName != "CObjectDispenser"
		&& sCurName != "CObjectTeleporter")
		return;

	Color clrBuildingColor;
	if (pEntity->GetTeamNum() == 2) //RED
		clrBuildingColor = Color(255, 20, 20, 255);
	else if (pEntity->GetTeamNum() == 3) //BLU
		clrBuildingColor = Color(0, 153, 255, 255);
	else //wtf
		clrBuildingColor = Color(255, 255, 255, 255);

	gDraw.DrawBoxOnEntity(gCvars.esp_box, pEntity, clrBuildingColor);

	Vector worldpos, screenpos, centerpos;
	pEntity->GetWorldSpaceCenter(worldpos);
	Vector lowerinWorld = pEntity->GetAbsOrigin();
	pEntity->GetWorldSpaceCenter(centerpos);

	if (gDraw.WorldToScreen(centerpos, screenpos))
	{
		int iY = 0;
		float x, y, w, h;
		int iHp = pEntity->GetHealth(), iMaxHp = pEntity->GetMaxHealth();
		if (iHp > iMaxHp)
			iHp = iMaxHp;

		//Box
		gDraw.DrawBoxOnEntity(gCvars.esp_buildings_box, pEntity, clrBuildingColor);
		if (!gDraw.GetEntityBounds(pEntity, x, y, w, h))
			return;

		//Health
		if (gCvars.esp_buildings_health == 2 || gCvars.esp_buildings_health == 3)
		{
			gDraw.OutlineRect(x - 6, y - 1, 5, h, Color::Black());
			gDraw.DrawRect(x - 5, y + (h - (h / iMaxHp * iHp)) - 1, 3, h / iMaxHp * iHp, redGreenGradiant(pEntity->GetHealth(), pEntity->GetMaxHealth()));
		}
		if (gCvars.esp_buildings_health == 1 || gCvars.esp_buildings_health == 3)
		{
			gDraw.DrawString(x + w + 2, y + iY, redGreenGradiant(pEntity->GetHealth(), pEntity->GetMaxHealth()), "%d / %d HP", pEntity->GetHealth(), pEntity->GetMaxHealth());
			iY += gDraw.GetESPHeight();
		}

		//Sentry
		if (sCurName == "CObjectSentrygun")
		{
			CObjectSentryGun* pSentryGun = reinterpret_cast<CObjectSentryGun*>(pEntity);
			if (!pSentryGun) return;

			if (gCvars.esp_buildings_name)
			{
				gDraw.DrawString(x + w + 2, y + iY, clrBuildingColor, "Sentry: Lv.%i", pSentryGun->GetLevel());
				iY += gDraw.GetESPHeight();
			}

		}
		//Dispenser
		else if (sCurName == "CObjectDispenser")
		{
			CObjectDispenser* pDispenser = reinterpret_cast<CObjectDispenser*>(pEntity);
			if (!pDispenser) return;

			if (gCvars.esp_buildings_name)
			{
				gDraw.DrawString(x + w + 2, y + iY, clrBuildingColor, "Dispenser: Lv.%i", pDispenser->GetLevel());
				iY += gDraw.GetESPHeight();
			}
		}
		//Teleporter
		else if (sCurName == "CObjectTeleporter")
		{
			CObjectTeleporter* pTeleporter = reinterpret_cast<CObjectTeleporter*>(pEntity);
			if (!pTeleporter) return;

			if (gCvars.esp_buildings_name)
			{
				gDraw.DrawString(x + w + 2, y + iY, clrBuildingColor, "Teleporter: Lv.%i", pTeleporter->GetLevel());
				iY += gDraw.GetESPHeight();
			}

		}
	}

}

void CESP::Item_ESP(CBaseEntity * pLocal, CBaseEntity * pEntity)
{

	if (!esp_items.value)
		return;

	Vector vCenterWorld;
	Vector vCenterScreen;
	pEntity->GetWorldSpaceCenter(vCenterWorld);
	if (!gDraw.WorldToScreen(vCenterWorld, vCenterScreen))
		return;

	const char* pszModelName = gInts.ModelInfo->GetModelName(pEntity->GetModel());
	if (streql(pszModelName, "models/items/medkit_small.mdl") || streql(pszModelName, "models/items/medkit_small_bday.mdl") || streql(pszModelName, "models/props_halloween/halloween_medkit_small.mdl"))
	{
		gDraw.DrawString(vCenterScreen.x, vCenterScreen.y, Color::Green(), "HP S");

	}

	if (streql(pszModelName, "models/items/ammopack_small.mdl") || streql(pszModelName, "models/items/ammopack_small_bday.mdl"))
	{
		gDraw.DrawString(vCenterScreen.x, vCenterScreen.y, Color(128, 128, 128, 255), "Ammo S");

	}

	if (streql(pszModelName, "models/items/medkit_medium.mdl") || streql(pszModelName, "models/items/medkit_medium_bday.mdl") || streql(pszModelName, "models/props_halloween/halloween_medkit_medium.mdl"))
	{
		gDraw.DrawString(vCenterScreen.x, vCenterScreen.y, Color::Green(), "HP M");
	}

	if (streql(pszModelName, "models/items/ammopack_medium.mdl") || streql(pszModelName, "models/items/ammopack_medium_bday.mdl"))
	{
		gDraw.DrawString(vCenterScreen.x, vCenterScreen.y, Color(128, 128, 128, 255), "Ammo M");

	}

	if (streql(pszModelName, "models/items/medkit_large.mdl") || streql(pszModelName, "models/items/medkit_large_bday.mdl") || streql(pszModelName, "models/props_halloween/halloween_medkit_large.mdl"))
	{
		gDraw.DrawString(vCenterScreen.x, vCenterScreen.y, Color::Green(), "HP L");

	}

	if (streql(pszModelName, "models/items/ammopack_large.mdl") || streql(pszModelName, "models/items/ammopack_large_bday.mdl"))
	{
		gDraw.DrawString(vCenterScreen.x, vCenterScreen.y, Color(128, 128, 128, 255), "Ammo L");

	}

	if (streql(pszModelName, "models/items/plate.mdl") || streql(pszModelName, "models/items/plate_steak.mdl") || streql(pszModelName, "models/items/plate_robo_sandwich.mdl"))
	{
		gDraw.DrawString(vCenterScreen.x, vCenterScreen.y, Color::Green(), "Sandvich");

	}

	if (streql(pszModelName, "models/items/currencypack_small.mdl") || streql(pszModelName, "models/items/currencypack_medium.mdl") || streql(pszModelName, "models/items/currencypack_large.mdl"))
	{
		gDraw.DrawString(vCenterScreen.x, vCenterScreen.y, Color::Green(), "$");

	}

}

void CESP::DrawBone(CBaseEntity* pEntity, int* iBones, int count, Color clrCol)
{
	for (int i = 0; i < count; i++)
	{
		if (i == count - 1)
			continue;

		Vector vBone1 = pEntity->GetHitboxPosition(iBones[i]);
		Vector vBone2 = pEntity->GetHitboxPosition(iBones[i + 1]);

		Vector vScr1, vScr2;

		if (!gDraw.WorldToScreen(vBone1, vScr1) || !gDraw.WorldToScreen(vBone2, vScr2))
			continue;

		//gDrawManager.DrawLine(vScr1.x, vScr1.y, vScr2.x, vScr2.y, clrCol);
		gDraw.DrawLine(vScr1.x, vScr1.y, vScr2.x, iBones[0] == 0 && iBones[1] == 4 && iBones[2] == 1 ? vScr2.y + 2 : vScr2.y, clrCol);
	}
}