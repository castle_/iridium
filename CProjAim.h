#pragma once

#include "SDK.h"
#include "Util.h"


namespace Projectile
{

	float PlayerGravityMod(CBaseEntity * player);

	float DistanceToGround(CBaseEntity* ent);

	Vector ProjectilePrediction(CBaseEntity* ent, CBaseEntity* local, int hb);

	Vector SimpleLatencyPrediction(CBaseEntity * ent, int hb);

	float GetProjectileSpeed(CBaseCombatWeapon * wep);
}

float Projectile::PlayerGravityMod(CBaseEntity* player)
{
	//int movetype = player->GetMoveType();

	//if (movetype == MOVETYPE_FLY || movetype == MOVETYPE_NOCLIP)
	//	return 0.0f;

	if (player->GetCond() & TFCondEx2_Parachute)
		return 0.448f;

	return 1.0f;
}

float Projectile::DistanceToGround(CBaseEntity* ent)
{
	if (ent->GetFlags() & FL_ONGROUND) return 0.0;

	Vector currPos;
	ent->GetWorldSpaceCenter(currPos);

	trace_t tr;
	Ray_t Ray;
	CTraceFilter filterNoPlayer;
	filterNoPlayer.pSkip = ent;

	Ray.Init(currPos, Vector(currPos.x, currPos.y, currPos.z - 10000.0f));
	gInts.EngineTrace->TraceRay(Ray, MASK_PLAYERSOLID, &filterNoPlayer, &tr);

	return currPos.z - tr.endpos.z;
}

Vector Projectile::ProjectilePrediction(CBaseEntity* ent, CBaseEntity* local, int hb)
{
	if (!ent)
		return Vector();

	Vector result;

	if (gAim.aimbot_adv_feet_aim.value && !DistanceToGround(ent) && !Util->IsHeadshotWeapon(local, local->GetActiveWeapon())) //only aim at feet if they're in the ground
		result = ent->GetAbsOrigin();
	else
		result = ent->GetHitboxPosition(hb);

	float latency = ((INetChannel*)gInts.Engine->GetNetChannelInfo())->GetLatency(FLOW_OUTGOING) +
		((INetChannel*)gInts.Engine->GetNetChannelInfo())->GetLatency(FLOW_INCOMING);

	float speed = GetProjectileSpeed(local->GetActiveWeapon());

	if (speed == -1.0)
		return Vector();

	float dtg = DistanceToGround(ent);
	Vector vel = Util->EstimateAbsVelocity(ent);

	// TODO ProjAim
	CBaseEntity* pLocal = GetBaseEntity(me);
	float medianTime = pLocal->GetEyePosition().DistTo(result) / speed;
	float range = 1.5f;
	float currenttime = medianTime - range;
	if (currenttime <= 0.0f)
		currenttime = 0.01f;
	float besttime = currenttime;
	float mindelta = 65536.0f;
	Vector bestpos = result;
	int maxsteps = 300;
	for (int steps = 0; steps < maxsteps; steps++, currenttime += ((float)(2 * range) / (float)maxsteps))
	{
		Vector curpos = result;
		curpos += vel * currenttime;
		curpos += vel * currenttime * latency;
		if (dtg > 0.0f)
		{
			curpos.z -= currenttime * currenttime * 400.0f * PlayerGravityMod(ent);
			if (curpos.z < result.z - dtg)
				curpos.z = result.z - dtg;
		}
		float rockettime = pLocal->GetEyePosition().DistTo(curpos) / speed;

		if (fabs(rockettime - currenttime) < mindelta)
		{
			besttime = currenttime;
			bestpos = curpos;
			mindelta = fabs(rockettime - currenttime);
		}
	}

	bestpos.z += besttime * besttime;

	return bestpos;
}

Vector Projectile::SimpleLatencyPrediction(CBaseEntity* ent, int hb)
{
	if (!ent) return Vector();
	Vector result;
	//if (gCvars.aimbot_advanced_feet_aim && !DistanceToGround(ent) && !Util->IsHeadshotWeapon(local, local->GetActiveWeapon())) //only aim at feet if they're in the ground
	//	result = ent->GetAbsOrigin();
	//else
		result = ent->GetHitboxPosition(hb);
	float latency = ((INetChannel*)gInts.Engine->GetNetChannelInfo())->GetLatency(FLOW_OUTGOING) +
		((INetChannel*)gInts.Engine->GetNetChannelInfo())->GetLatency(FLOW_INCOMING);
	Vector vel = Util->EstimateAbsVelocity(ent);
	result += vel * latency;
	return result;
}

float Projectile::GetProjectileSpeed(CBaseCombatWeapon *wep)
{
	int id = wep->GetClientClass()->iClassID;

	float BeginCharge = wep->GetChargeTime();
	float Charge;

	switch (wep->GetItemDefinitionIndex())
	{
	case (int)sniperweapons::WPN_CompoundBow:
	case (int)sniperweapons::WPN_FestiveHuntsman:
	case (int)sniperweapons::WPN_Huntsman:
		{
			Charge = BeginCharge == 0.0f ? 0.0f : gInts.g_pGlobals->curtime - BeginCharge;

			if (Charge > 1.0f)
				Charge = 1.0f;

			return(800 * Charge + 1800);
		}
	}

	switch (wep->GetItemDefinitionIndex())
	{
	case (int)demomanweapons::WPN_GrenadeLauncher:
		return 840.0f;

		/*case classId::CBaseGrenade:
		{
		Charge = BeginCharge == 0.0f ? 0.0f : gInts.Globals->curtime - BeginCharge;

		return 805.0f + (Charge * 261.63);
		}*/

	case soldierweapons::WPN_RocketLauncher:
	case soldierweapons::WPN_NewRocketLauncher:
	case soldierweapons::WPN_BlackBox:
	case soldierweapons::WPN_Original:
	case soldierweapons::WPN_BeggersBazooka:
		return 1100.0f;
	case soldierweapons::WPN_DirectHit:
		return 1980.0f;
	case soldierweapons::WPN_LibertyLauncher:
		return 1540.0f;
	case soldierweapons::WPN_Airstrike:
	case soldierweapons::WPN_CowMangler:
		return 1100.0f;
	case soldierweapons::WPN_RighteousBison:
		return 1200.0f;

	case pyroweapons::WPN_Flaregun:
		return 1450.0f;

	case scoutweapons::WPN_Bat:
		return 1940.0f;

	case medicweapons::WPN_SyringeGun:
		return 990.0f;
	}

	return -1.0f;
}