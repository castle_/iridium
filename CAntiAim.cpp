#include "CAntiAim.h"

void CAntiAim::Run(CBaseEntity * pLocal, CUserCmd * pCommand, bool* bSendPacket)
{
	if (!aa_active.value) 
		return;

	CBaseCombatWeapon* pWep = pLocal->GetActiveWeapon();

	if (!pWep) 
		return;

	if (pCommand->buttons & IN_ATTACK2 && pWep->GetClientClass()->iClassID == (int)classId::CTFFlameThrower)/*Airblast*/ 
		return;

	if (pCommand->buttons & IN_ATTACK && Util->CanShoot(pLocal, pWep)) 
		return;

	if (pCommand->buttons & IN_ATTACK && pWep->GetSlot() == 2)
		return;

	Vector vAngs = pCommand->viewangles;

	int iFake = 0;

	if (aa_pitch_fake.value == 1)
		iFake = 360;
	else if (aa_pitch_fake.value == 2)
		iFake = -360;

	switch (aa_pitch_real.value)
	{
	case 1:
		vAngs.x = -89 + iFake;
		break;
	case 2:
		vAngs.x = -45 + iFake;
		break;
	case 3:
		vAngs.x = 0 + iFake;
		break;
	case 4:
		vAngs.x = 89 + iFake;
		break;
	case 5:
		vAngs.x = aa_pitch_real_custom.value + iFake;
		break;
	default:
		break;
	}

	switch (aa_yaw.value)
	{
	case 1: //left
		vAngs.y -= 90;
		break;
	case 2: //right
		vAngs.y += 90;
		break;
	case 3: //jitter
		vAngs.y = rand() % 180;
		break;
	case 4: //spin
	{
		jitterangs += aa_spin_speed.value;
		vAngs.y = jitterangs;
	}
	break;
	case 5: //Fake sideways
		if (*bSendPacket)
		{
			vAngs.y += 90.0f;
		}
		else
		{
			*bSendPacket = false;
			vAngs.y -= -90.0f;
		}
	case 6:
	{
		int iFakeSideY = 90;

		jitterangs = vAngs.y;
		if (bFlip)
		{
			jitterangs += iFakeSideY;
			vAngs.y = jitterangs;
			bFlip = !bFlip;
		}
		else
		{
			jitterangs -= iFakeSideY;
			vAngs.y = jitterangs;
			bFlip = !bFlip;
		}
	}
	default:
		break;
		}

	while (vAngs.y > 180)
		vAngs.y -= 360;
	while (vAngs.y < -180)
		vAngs.y += 360;

	Util->SilentMovementFix(pCommand, vAngs);
	pCommand->viewangles = vAngs;
}
