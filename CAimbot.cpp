//#include "CAimbot.h"
//#include "CProjAim.h"
//FILE* fp;
//#include <iostream>
//
//CAimbot2 gAimbot;
//
//inline float distance_point_to_line(Vector Point, Vector LineOrigin, Vector Dir)
//{
//	auto PointDir = Point - LineOrigin;
//
//	auto TempOffset = PointDir.Dot(Dir) / (Dir.x*Dir.x + Dir.y*Dir.y + Dir.z*Dir.z);
//	if (TempOffset < 0.000001f)
//		return FLT_MAX;
//
//	auto PerpendicularPoint = LineOrigin + (Dir * TempOffset);
//
//	return (Point - PerpendicularPoint).Length();
//}
//
//inline Vector angle_vector(Vector meme)
//{
//	auto sy = sin(meme.y / 180.f * static_cast<float>(PI));
//	auto cy = cos(meme.y / 180.f * static_cast<float>(PI));
//
//	auto sp = sin(meme.x / 180.f * static_cast<float>(PI));
//	auto cp = cos(meme.x / 180.f* static_cast<float>(PI));
//
//	return Vector(cp*cy, cp*sy, -sp);
//}
//inline Vector compute_angle(const Vector &source, const Vector &destination, Vector& angles)
//{
//	Vector delta = source - destination;
//	angles.x = static_cast<float>(asin(delta.z / delta.Length()) * M_RADPI);
//	angles.y = static_cast<float>(atan(delta.y / delta.x) * M_RADPI);
//	angles.z = 0.0f;
//
//	if (delta.x >= 0.0f)
//		angles.y += 180.0f;
//
//	return angles;
//}
//float CAimbot2::GetFOV(Vector angle, Vector src, Vector dst)
//{
//	Vector ang, aim;
//	float mag, u_dot_v;
//	ang = calc_angle(src, dst);
//
//
//	MakeVector(angle, aim);
//	MakeVector(ang, ang);
//
//	mag = sqrtf(pow(aim.x, 2) + pow(aim.y, 2) + pow(aim.z, 2));
//	u_dot_v = aim.Dot(ang);
//
//	return RAD2DEG(acos(u_dot_v / (pow(mag, 2))));
//}
//Vector CAimbot2::calc_angle(Vector src, Vector dst)
//{
//	Vector AimAngles, delta;
//	float hyp;
//	delta = src - dst;
//	hyp = sqrtf((delta.x * delta.x) + (delta.y * delta.y));
//	AimAngles.x = atanf(delta.z / hyp) * RADPI;
//	AimAngles.y = atanf(delta.y / delta.x) * RADPI;
//	AimAngles.z = 0.0f;
//	if (delta.x >= 0.0)
//		AimAngles.y += 180.0f;
//	return AimAngles;
//}
//void CAimbot2::MakeVector(Vector angle, Vector& vector)
//{
//	float pitch, yaw, tmp;
//	pitch = float(angle[0] * PI / 180);
//	yaw = float(angle[1] * PI / 180);
//	tmp = float(cos(pitch));
//	vector[0] = float(-tmp * -cos(yaw));
//	vector[1] = float(sin(yaw)*tmp);
//	vector[2] = float(-sin(pitch));
//}
//inline Vector vectorangles(Vector start, Vector end)
//{
//	Vector delta = end - start;
//
//	float magnitude = sqrtf(delta.x * delta.x + delta.y * delta.y);
//	float pitch = atan2f(-delta.z, magnitude) * RADPI;
//	float yaw = atan2f(delta.y, delta.x) * RADPI;
//
//	Vector angle(pitch, yaw, 0.0f);
//
//	return angle;
//}
//inline float get_fov(Vector angle, Vector viewangles)
//{
//	Vector offset = angle - viewangles;
//	ClampAngle(offset);
//	return sqrtf(offset.x * offset.x + offset.y * offset.y);
//}
//
////#include "Backtrack.h"
//int CAimbot2::GetTarget(CBaseEntity* pLocal, CUserCmd* pCommand, Vector* pos)
//{
//	int bestTarget = -1;
//	float flDistToBest = 99999.0;
//	float minimalDistance = 99999.0;
//
//	Vector vLocal = pLocal->GetEyePosition();
//	Vector vPos;
//
//	for (int i = 1; i <= gInts.Engine->GetMaxClients(); i++)
//	{
//		if (i == me)
//			continue;
//
//		CBaseEntity* pEntity = GetBaseEntity(i);
//
//		if (!pEntity)
//			continue;
//
//		float distance = pLocal->GetEyePosition().DistTo(pEntity->GetEyePosition()); // units distance
//		if (distance > 20000.f)
//			continue;
//
//		if (pEntity->IsDormant() || pEntity->GetLifeState() != LIFE_ALIVE || pEntity->GetTeamNum() == pLocal->GetTeamNum())
//			continue;
//
//		int conds = pEntity->GetCond();
//
//		if (conds & TFCond_Ubercharged
//			|| conds & TFCond_Bonked)
//			continue;
//
//		if (gCvars.aimbot_ignore_cloak && conds & TFCond_Cloaked)
//			continue;
//
//		Vector vEntity;
//		int hitbox = GetBestHitbox(pLocal, pEntity, &vEntity);
//
//		if (Projectile::GetProjectileSpeed(pLocal->GetActiveWeapon()) != -1.f)
//		{
//			vEntity = Projectile::ProjectilePrediction(pEntity, pLocal, gCvars.aimbot_hitbox);
//
//			if (!Util->AssumeVis(pLocal, pLocal->GetVecOrigin() + pLocal->GetAbsEyePosition(), vEntity))
//				continue;
//		}
//		else if (hitbox == -1)
//		{
//			// !!! TO DO: Change the backtrack tick to iterate (if you want it to) !!!
//			// Personally it's just best to leave it at the max back-trackable tick, because it's only 0.2 seconds behind
//			// This means we only do one check instead of twelve, which means 0.0001 more FPS!
//
//			// We can't hit them normally, so let's see if we can backtrack them.
//			//Vector backPos = gBacktrack.btHeadPositions[i][13].hitboxpos;
//			//if (!gCvars.backtrack_enabled || !Util->AssumeVis(pLocal, vLocal, backPos))
//				continue; // Nope. Not today.
//			//vEntity = backPos;
//			//doBacktrack = gBacktrack.btHeadPositions[i][13].tickcount;
//		}
//
//		float flFOV = GetFOV(pCommand->viewangles, vLocal, vEntity);
//		if (std::isnan(flFOV)) // Just for security
//			flFOV = 0.1f;
//
//		if (flFOV < flDistToBest && flFOV < gCvars.aimbot_fov)
//			flDistToBest = flFOV, vPos = vEntity, bestTarget = i;
//	}
//
//	freopen_s(&fp, "CONOUT$", "w", stdout);
//	std::cout << "bestTarget: " << bestTarget << endl;
//	//std::cout << "crit chance: " << wep->GetObservedCritChance() << endl;
//
//	if (pos) *pos = vPos;
//	return bestTarget;
//}
//
//void CAimbot2::RunNEW(CBaseEntity* pLocal, CUserCmd* pCommand)
//{
//	gCvars.iAimbotIndex = -1;
//	if (!gCvars.aimbot_active)
//		return;
//
//	if (!Util->IsKeyPressed(gCvars.aimbot_key))
//		return;
//
//	auto pWep = pLocal->GetActiveWeapon();
//	if (!pWep)
//		return;
//
//	//if (gCvars.Aimbot.zoomedonly && pWep->GetClientClass()->iClassID == (int)classId::CTFSniperRifle)
//	//	if (pWep->GetChargeDamage() < 9.8f)
//	//		return;
//
//	Vector targetPos;
//	int target = GetTarget(pLocal, pCommand, &targetPos);
//	if (target == -1)
//		return;
//
//	CBaseEntity* pTarget = GetBaseEntity(target);
//	if (!pTarget)
//		return;
//
//	Vector ViewAngles, start = pLocal->GetEyePosition();
//	VectorAngles((targetPos - start), ViewAngles);
//	ClampAngle(ViewAngles); // Don't want to accidentally create an anti-aim!
//
//	gCvars.iAimbotIndex = target;
//
//	//if (gCvars.aimbot_canshoot && !Util->CanShoot(pLocal, pWep))
//	//	return;
//
//	if (gCvars.aimbot_silent)
//		AimAt(ViewAngles, true, pCommand);
//	else if (!gCvars.aimbot_silent && gCvars.aimbot_smooth)
//	{
//		Vector smoothedAngle;
//		SmoothAngle(pCommand->viewangles, ViewAngles, smoothedAngle, gCvars.aimbot_smooth_amt);
//		pCommand->viewangles = smoothedAngle;
//		gInts.Engine->SetViewAngles(smoothedAngle);
//	}
//	else if (!gCvars.aimbot_silent)
//		AimAt(ViewAngles, false, pCommand);
//
//	if (gCvars.aimbot_autoshoot)
//	{
//		if (pLocal->GetActiveWeapon()->GetItemDefinitionIndex() == (int)sniperweapons::WPN_Huntsman ||
//			pLocal->GetActiveWeapon()->GetItemDefinitionIndex() == (int)sniperweapons::WPN_FestiveHuntsman ||
//			pLocal->GetActiveWeapon()->GetItemDefinitionIndex() == (int)sniperweapons::WPN_CompoundBow)
//			pCommand->buttons &= ~IN_ATTACK;
//		else
//			pCommand->buttons |= IN_ATTACK;
//	}
//}
//
//void CAimbot2::AimAt(Vector angles, bool silent, CUserCmd* pCommand)
//{
//	silent ? Util->silentMovementFix(pCommand, angles) : gInts.Engine->SetViewAngles(angles);
//	pCommand->viewangles = angles;
//}
//
//int CAimbot2::GetBestHitbox(CBaseEntity* pLocal, CBaseEntity* pEntity, Vector* pos)
//{
//	// - Variables for finding a hitbox, choosing a first hitbox, and choosing an automatic "best" hitbox
//	int iBestHitbox = -1, initial, autohit = Util->IsHeadshotWeapon(pLocal, pLocal->GetActiveWeapon()) ? 0 : 4;
//	Vector HitboxPos, eyepos = pLocal->GetEyePosition();
//
//	if (!gCvars.aimbot_hitbox) // Auto
//		initial = autohit;
//	else if (gCvars.aimbot_hitbox == 1) // Head
//		initial = 0;
//	else if (gCvars.aimbot_hitbox == 2) // Body
//		initial = 4;
//
//	if (Util->IsVisible(pLocal, pEntity, eyepos, HitboxPos = pEntity->GetHitboxPosition(initial)))
//		iBestHitbox = initial; // Cool! The hitbox we want is visible!
//
//	//if (Util->AssumeVis(pLocal, eyepos, pEntity->GetHitboxPosition(initial)))
//	//	iBestHitbox = initial; // Cool! The hitbox we want is visible!
//
//	else if (gCvars.aimbot_hitscan)
//	{
//		// We don't want to vischeck autohit again if we already checked it earlier
//		if (initial != autohit && Util->IsVisible(pLocal, pEntity, eyepos, HitboxPos = pEntity->GetHitboxPosition(autohit)))
//			iBestHitbox = autohit; // Alright, the best hitbox is visible
//		else // Or not... let's scan for more
//			for (int i = 0; i < 17; i++)
//			{
//				// We already vischecked these and failed!
//				if (i == initial || i == autohit)
//					continue;
//
//				if (Util->IsVisible(pLocal, pEntity, eyepos, HitboxPos = pEntity->GetHitboxPosition(i)))
//				{
//					iBestHitbox = i;
//					break; // All good to go. We don't need to find another hitbox!
//				}
//			}
//	}
//
//	freopen_s(&fp, "CONOUT$", "w", stdout);
//	std::cout << "iBestHitbox: " << iBestHitbox << endl;
//
//	if (iBestHitbox == -1 || HitboxPos.IsZero())
//		return -1;
//
//	// If we want the position returned, then check the pointer and set it
//	if (pos) *pos = HitboxPos;
//	return iBestHitbox;
//}
//
//void CAimbot2::SmoothAngle(Vector& currentAngle, Vector& aimAngle, Vector& smoothedAngle, float fSmoothPercentage)
//{
//	ClampAngle(aimAngle);
//	smoothedAngle = aimAngle - currentAngle;
//	smoothedAngle = currentAngle + smoothedAngle / 100.f * fSmoothPercentage;
//	ClampAngle(smoothedAngle);
//}
//
//// The SDK is all mashed together, so I'm just gonna leave these functions here to external prevent linker issues
//// !!! TO DO: Split entities and vischecking into their own headers to keep SDK clean and less buggy !!!
//bool CGameTrace::DidHitWorld() const
//{
//	return m_pEnt == GetBaseEntity(0);
//}
//
//bool CGameTrace::DidHitNonWorldEntity() const
//{
//	return m_pEnt != NULL && !DidHitWorld();
//}
//
//int CGameTrace::GetEntityIndex() const
//{
//	if (m_pEnt)
//		return m_pEnt->GetIndex();
//	else
//		return -1;
//}