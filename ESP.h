#pragma once
#include "SDK.h"

class CESP
{
public:
	Checkbox		esp_enabled				=			    Checkbox("Active");
	Listbox			esp_position			=				Listbox("Position", { "Default", "Bottom"});
	Checkbox		esp_enemy_only		    =				Checkbox("Enemy Only");
	Checkbox		esp_visible_only	    =				Checkbox("Visible Only");
	Checkbox		esp_vis_check		    =				Checkbox("Visibility Check");
	Listbox			esp_name			    =				Listbox("Name", { "OFF", "Normal", "Top"});
	Checkbox		esp_class			    =				Checkbox("Class");
	Checkbox		esp_boxes			    =				Checkbox("Boxes");
	Listbox			esp_bones			    =				Listbox("Bones", { "OFF", "White", "Health", "Team" });
	Listbox			esp_health			    =				Listbox("Health", { "OFF", "Value", "Bar", "Both" });
	Checkbox		esp_conditions		    =				Checkbox("Boxes");
	Listbox			esp_snaplines		    =				Listbox("Snaplines", { "OFF", "Crosshair", "Bottom"});
	Checkbox		esp_disguise		    =				Checkbox("Anti-Disguise");
	Checkbox		esp_items			    =				Checkbox("Items");
	Checkbox		esp_glow			    =				Checkbox("Glow (dx9)");
	Checkbox		esp_glow_enemy_only	    =				Checkbox("Glow Enemy Only");
	Checkbox		esp_glow_items		    =				Checkbox("Glow Items");

	///Add chams later lule
	/*Checkbox	visual_chams = Checkbox("Chams");
	Checkbox	visual_chams = Checkbox("Chams");
	Checkbox	visual_chams = Checkbox("Chams");
	Checkbox	visual_chams = Checkbox("Chams");*/

	Listbox			visual_nozoom		    =				Listbox("Scope", {"OFF", "No Scope", "No Zoom", "Both" });
	Listbox			visual_colors		    =				Listbox("Colors", { "m4risa", "TF2", "Null Core", "Custom" });
	Checkbox		visual_aimware		    =				Checkbox("AIMWARE ESP");

	/*
		clrAimbotTarget = HueToRGB(gCvars.visual_aim_hue);
		clrFriend = HueToRGB(gCvars.visual_friend_hue);
		clrRage = HueToRGB(gCvars.visual_rage_hue);
		clrRedTeam = HueToRGB(gCvars.visual_red_hue); 
		clrRedTeamMenu = HueToRGB(gCvars.visual_menu_hue); 
		clrBluTeam = HueToRGB(gCvars.visual_blu_hue); 
		clrBluTeamMenu = HueToRGB(gCvars.visual_menu_hue);
	*/
	ColorPicker		visual_clr_aim		    =				ColorPicker("Aim Target", Color::White());
	ColorPicker		visual_clr_friend	    =				ColorPicker("Friend", Color::Indigo());
	ColorPicker		visual_clr_rage		    =				ColorPicker("Rage Target", Color::Yellow());
	ColorPicker		visual_clr_red		    =				ColorPicker("Red Team", Color::Red());
	ColorPicker		visual_clr_red_menu     =				ColorPicker("Red Menu", Color::Red());
	ColorPicker		visual_clr_blu          =				ColorPicker("Blu Team", Color::Blue());
	ColorPicker		visual_clr_blu_menu     =				ColorPicker("Blu Menu", Color::Blue());

	Checkbox		visual_chams_active		=				Checkbox("Enabled");
	Checkbox		visual_chams_player     =				Checkbox("Player");
	Checkbox		visual_chams_weapons	=				Checkbox("Player Weapons");
	Listbox			visual_hands			=				Listbox("Hands", { "OFF", "No Draw", "Wireframe", "Flat" });

	Checkbox		visual_spawndoors		=				Checkbox("Spawn Doors");
	Checkbox		visual_nightmode		=				Checkbox("Nightmode");

	void Run();

private:

	void Building_ESP(CBaseEntity* pLocal, CBaseEntity* pEntity);
	void Player_ESP(CBaseEntity* pLocal, CBaseEntity* pEntity);
	void Item_ESP(CBaseEntity* pLocal, CBaseEntity* pEntity);

	//Other funcs
	void CustomSkybox();
	void DrawBone(CBaseEntity* pEntity, int* iBones, int count, Color clrCol);
};

extern CESP gESP; 