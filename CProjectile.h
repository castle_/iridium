#pragma once
#include "SDK.h"
#include "GUI.h"
#include "Util.h"
#define MIN(a, b)  (((a) < (b)) ? (a) : (b))

class CProjectile
{

public:
	Vector ProjectilePrediction(CBaseEntity* ent, CBaseEntity* local, int hb, float gravitymod, float entgmod);

private:
	Vector SimpleLatencyPrediction(CBaseEntity* ent, int hb);
	Vector EstimateAbsVelocity(CBaseEntity *ent);
	float DistanceToGround(CBaseEntity* ent);
	float VectorialDistanceToGround(Vector origin);

};

extern CProjectile gProj;