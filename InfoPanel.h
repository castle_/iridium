#pragma once
#include "SDK.h"
#include <vector>
#include <Windows.h>

enum class info_e_mb
{
	null,
	lclick,
	rclick,
	ldown,
	rdown
};

class InfoPanel
{
public:
	bool enabled = false;

	void draw();
	void createGUI();

private:
	info_e_mb mb = info_e_mb::null;
	POINT mouse{ 0, 0 }, pmouse{ 0, 0 }, pmenu{ 200, 200 };
	void getInput();
	bool mouseOver(int x, int y, int w, int h);

	// Just a quick and sloppy control struct
	enum class e_type
	{
		null,
		tab,
		boolean,
		integer
	};
	struct Item
	{
		const char* name;
		e_type type = e_type::null;

		bool* bValue = nullptr;
		int* iValue = nullptr;
		int min = 0, max = 1, step = 1;
	};
	vector<Item> itemlist;

	bool addTab(const char* name, bool &value);
	void addBool(const char* name, bool &value);
	void addInt(const char* name, int &value, int min, int max, int step);
};
extern InfoPanel gInfoPanel;