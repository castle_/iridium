#pragma once
#include "SDK.h"
#include "Util.h"

class CAntiAim
{
public:
	Checkbox		aa_active				=				Checkbox("Anti-Aim Active");
	Listbox  		aa_pitch_fake			=				Listbox("Fake Pitch", { "OFF", "Down", "Up"});
	Listbox			aa_pitch_real			=				Listbox("Real Pitch", { "OFF", "Up", "Half-Up", "Center", "Down", "Custom" });
	Slider			aa_pitch_real_custom	=				Slider("Custom Real Pitch", 0, -89, 89, 1);
	Listbox			aa_yaw					=				Listbox("AA Yaw", { "OFF", "Right", "Left", "Jitter", "Spin", "bFakeside", "Fakeside 2" });
	Slider			aa_spin_speed			=				Slider("Spin Speed", 2, -180, 180, 1);
	Listbox			aa_resolver				=				Listbox("Resolver", { "OFF", "ON", "Force Up", "Force Down" });
	Checkbox		aa_resolver_rageonly	=				Checkbox("Resolver Rage Only");
	Checkbox		aa_real_local_angs		=				Checkbox("Real Local Angs");
	Checkbox		fakelag_active		    =				Checkbox("Fakelag Enabled");
	Checkbox		fakelag_disable_attack	=				Checkbox("Disable On Attack");
	Slider			fakelag_choke			=				Slider("Choke", 0, 1, 15, 1);
	Slider			fakelag_unchoke			=				Slider("Unchoke", 0, 1, 2, 1);

	void Run(CBaseEntity* pLocal, CUserCmd* pCommand, bool* bSendPacket);
private:
	float jitterangs = 0;
	bool bFlip = 0;
};

extern CAntiAim gAntiAim;
