#include "SDK.h"
#include "Client.h"
#include "Panels.h"
#include "FrameStageNotify.h"
#include "Menu.h"
#include "NetVars.h"
#include "CAnnouncer.h"
#include "Settings.h"
#include "AutoAirblast.h"
#include "DrawModelExecute.h"
#include "Killsays.h"
#include "GUI.h"
#include "Misc.h"
COffsets gOffsets;
CGlobalVariables gCvars;
CInterfaces gInts;
//_NetVars nvars;
//EngineClient gEngine;

IPlayerInfoManager g_pPlayerInfoManager;

CGlobals g_pGlobals;

CreateInterface_t EngineFactory = NULL;
CreateInterface_t ClientFactory = NULL;
CreateInterface_t VGUIFactory = NULL;
CreateInterface_t VGUI2Factory = NULL;
CreateInterface_t CvarFactory = NULL;
CreateInterface_t MaterialSystemFactory = NULL;

typedef CGlowObjectManager* (*GlowObjectManager_t) ();

VMTBaseManager* clientHook;
VMTBaseManager* clientModeHook;
VMTBaseManager* panelHook;
VMTBaseManager* modelHook;

int vacUndetected = 1;
typedef void(__thiscall *OverrideViewFn) (void*, CViewSetup*);

void __fastcall Hooked_OverrideView(void* _this, void* _edx, CViewSetup* pSetup)
{

	// epic code
	VMTManager& hook = VMTManager::GetHook(_this); //Get a pointer to the instance of your VMTManager with the function GetHook.
	hook.GetMethod<OverrideViewFn>(16)(_this, pSetup); //Call the original.
	if (!gInts.Engine->IsInGame() && !gInts.Engine->IsConnected())
		return;

	CBaseEntity* pLocal = GetBaseEntity(me);

	if (!(pLocal->GetCond() & TFCond_Zoomed) && pSetup->m_fov != gMisc.misc_fov.value)
	{
		pSetup->m_fov = gMisc.misc_fov.value;
	}
}

DWORD WINAPI dwMainThread(LPVOID lpArguments)
{
	if (gInts.Client == NULL)
	{
		/*VMTBaseManager**/ clientHook = new VMTBaseManager();
		/*VMTBaseManager**/ clientModeHook = new VMTBaseManager();
		/*VMTBaseManager**/ panelHook = new VMTBaseManager();
		modelHook = new VMTBaseManager();

		ClientFactory = (CreateInterfaceFn)GetProcAddress(gSignatures.GetModuleHandleSafe("client.dll"), "CreateInterface");
		EngineFactory = (CreateInterfaceFn)GetProcAddress(gSignatures.GetModuleHandleSafe("engine.dll"), "CreateInterface");
		VGUIFactory = (CreateInterfaceFn)GetProcAddress(gSignatures.GetModuleHandleSafe("vguimatsurface.dll"), "CreateInterface");
		CvarFactory = (CreateInterfaceFn)GetProcAddress(gSignatures.GetModuleHandleSafe("vstdlib.dll"), "CreateInterface");
		MaterialSystemFactory = (CreateInterfaceFn)GetProcAddress(gSignatures.GetModuleHandleSafe("MaterialSystem.dll"), "CreateInterface");

		gInts.Client = (CHLClient*)ClientFactory("VClient017", NULL);
		gInts.EntList = (CEntList*)ClientFactory("VClientEntityList003", NULL);
		gInts.Engine = (EngineClient*)EngineFactory("VEngineClient013", NULL);
		gInts.Surface = (ISurface*)VGUIFactory("VGUI_Surface030", NULL);
		gInts.EngineTrace = (IEngineTrace*)EngineFactory("EngineTraceClient003", NULL);
		gInts.ModelInfo = (IVModelInfo*)EngineFactory("VModelInfoClient006", NULL);
		gInts.Cvar = (ICvar*)CvarFactory("VEngineCvar004", NULL);
		gInts.EventManager = (IGameEventManager2*)EngineFactory("GAMEEVENTSMANAGER002", NULL);
		gInts.ModelRender = (CModelRender*)EngineFactory("VEngineModel016", NULL);
		gInts.RenderView = (CRenderView*)EngineFactory("VEngineRenderView014", NULL);
		gInts.MatSystem = (CMaterialSystem *)MaterialSystemFactory("VMaterialSystem081", NULL);
		gInts.g_pGlobals = *reinterpret_cast<CGlobals **>(gSignatures.GetEngineSignature("A1 ? ? ? ? 8B 11 68") + 8); //waudhawhdaoufahfaehohfaeou

		//gInts.NewSignonMsg = gSignatures.GetEngineSignature("EC 53 56 8B F1 57 89 75 F0") - 0x4;
		//gInts.ClearSignonMsg = gSignatures.GetEngineSignature("34 10 C7 87 ? ? ? ? ? ? ? ? C7 87") - 0x17;
		//XASSERT(gInts.NewSignonMsg);
		//XASSERT(gInts.ClearSignonMsg);

		XASSERT(gInts.Client);
		XASSERT(gInts.EntList);
		XASSERT(gInts.Engine);
		XASSERT(gInts.Surface);
		XASSERT(gInts.EngineTrace);
		XASSERT(gInts.ModelInfo);
		XASSERT(gInts.EventManager);
		XASSERT(gInts.ModelRender);
		XASSERT(gInts.RenderView);
		XASSERT(gInts.MatSystem);
		XASSERT(gInts.g_pGlobals);

		gKillSay.InitKillSay();
		//Util->killCvars();

		//Load config
		gSettings.LoadSettings(".\\Iridium.json");
		gAnnouncer.AddListeners();

		//ConVar* cvar_test = new ConVar("ayylmao", "1", 0, "ayylmao");
		//Interfaces.Cvar->RegisterConCommand(cvar_test);
		//ConVar_Register(0);
		//ConVar<int> blue_test{ "blue_test", 10, 0, 100, nullptr };

		if (!gInts.Panels)
		{
			VGUI2Factory = (CreateInterfaceFn)GetProcAddress(gSignatures.GetModuleHandleSafe("vgui2.dll"), "CreateInterface");
			gInts.Panels = (IPanel*)VGUI2Factory("VGUI_Panel009", NULL);
			XASSERT(gInts.Panels);

			if (gInts.Panels)
			{
				panelHook->Init(gInts.Panels);
				panelHook->HookMethod(&Hooked_PaintTraverse, gOffsets.iPaintTraverseOffset);
				panelHook->Rehook();
			}
		}

		DWORD dwClientModeAddress = gSignatures.GetClientSignature("8B 0D ? ? ? ? 8B 02 D9 05");
		XASSERT(dwClientModeAddress);
		gInts.ClientMode = **(ClientModeShared***)(dwClientModeAddress + 2);
		LOGDEBUG("g_pClientModeShared_ptr client.dll+0x%X", (DWORD)gInts.ClientMode - dwClientBase);

		DWORD dwGlowLoc = gSignatures.GetClientSignature("8B 0D ? ? ? ? A1 ? ? ? ? 56 8B 37") + 0x2;
		XASSERT(dwGlowLoc);
		gInts.glowobjectmanager = (CGlowObjectManager*)(dwGlowLoc);
		XASSERT(gInts.glowobjectmanager);

		clientHook->Init(gInts.Client);
		clientHook->HookMethod(&Hooked_KeyEvent, gOffsets.iKeyEventOffset);
		clientHook->HookMethod(&Hooked_FrameStageNotify, gOffsets.iFrameStageNotify); // crash if gay
		clientHook->Rehook();

		clientModeHook->Init(gInts.ClientMode);
		clientModeHook->HookMethod(&Hooked_CreateMove, gOffsets.iCreateMoveOffset); //ClientMode create move is called inside of CHLClient::CreateMove, and thus no need for hooking WriteUserCmdDelta.
		clientModeHook->HookMethod(&Hooked_OverrideView, 16);
		clientModeHook->Rehook();

		modelHook->Init(gInts.ModelRender);
		modelHook->HookMethod(&DrawModelExecute::Hooked_DrawModelExecute, 19);
		modelHook->Rehook();

		HWND thisWindow;
		while ((thisWindow = FindWindow("Valve001", NULL)) == false);
		//if (!gCvars.cade_menu)
		//{
		//	gGUI.windowProc;
		//	if (thisWindow)
		//		gGUI.windowProc = (WNDPROC)SetWindowLongPtr(thisWindow, GWLP_WNDPROC, (LONG_PTR)&Hooked_WndProc);
		//}
		//else
		//{
			gMenu.windowProc;
			if (thisWindow)
				gMenu.windowProc = (WNDPROC)SetWindowLongPtr(thisWindow, GWLP_WNDPROC, (LONG_PTR)&Hooked_WndProc);
		//}


																											   //gInts.Surface->PlaySoundA("UT99/godlike.wav");
																											   //For some reason, the injection sound doesn't work like most cheats, cuz this one just plays as soon as you inject and you sometimes
																											   //Can't hear it because you don't click on the game fast enough, not sure how other cheats do it!

		//ConVar* ir_test = new ConVar("ir_test", "0", 0, "This is a test!");
		//gInts.Cvar->RegisterConCommand(ir_test);
		//ConVar_Register(0);
		
		gInts.Engine->ClientCmd_Unrestricted(XorString("echo Iridium Injected, have fun!"));

		Beep(659.26, 200);
		Beep(659.26, 200);
		Sleep(200);
		Beep(659.26, 200);
		Sleep(100);
		Beep(523.26, 200);
		Beep(659.26, 200);
		Sleep(200);
		Beep(783.98, 200);
		Sleep(400);
		Beep(391.99, 200);

	}
	return 0; //The thread has been completed, and we do not need to call anything once we're done. The call to Hooked_PaintTraverse is now our main thread.
}

BOOL APIENTRY DllMain(HMODULE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	if (dwReason == DLL_PROCESS_ATTACH)
	{
#ifdef _DEBUG
		AllocConsole();
#endif
		Log::Init(hInstance);
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)dwMainThread, NULL, 0, NULL);
	}
	return true;
}



//god what a fucking nightmare, I'll remove this soon
//for (size_t i = 0; i < 10; i++)
//	gInts.Engine->ClientCmd_Unrestricted("echo \x0A");
//gInts.Engine->ClientCmd_Unrestricted("echo <3");
//gInts.Engine->ClientCmd_Unrestricted("echo __________________________________________________________________________________________________________________");
//gInts.Engine->ClientCmd_Unrestricted("echo \x0A");
//gInts.Engine->ClientCmd_Unrestricted("echo MarkTF2 is in his hell");
//gInts.Engine->ClientCmd_Unrestricted("echo People I've had sex with: Ain't enough paper for the names I gotta mention");
//gInts.Engine->ClientCmd_Unrestricted("echo m4rida.jar by steamcommunity.com/id/gabelogannewell - v4.2.0");
//gInts.Engine->ClientCmd_Unrestricted("echo \x0A");
//gInts.Engine->ClientCmd_Unrestricted("echo __________________________________________________________________________________________________________________");
//gInts.Engine->ClientCmd_Unrestricted("echo \x0A");
//gInts.Engine->ClientCmd_Unrestricted("echo List of Features~");
//gInts.Engine->ClientCmd_Unrestricted("echo \x0A");
//gInts.Engine->ClientCmd_Unrestricted("echo Aimbot\t= F1 to toggle, E to baim (the only thing this cheat can do anyways)");
//gInts.Engine->ClientCmd_Unrestricted("echo Trigger\t= F2 to toggle, LShift to shoot");
//gInts.Engine->ClientCmd_Unrestricted("echo ESP			= F3 to toggle");
//gInts.Engine->ClientCmd_Unrestricted("echo Bunny Hop    = F4 to toggle");

//typedef bool(__thiscall * BInitTextBufferFn)(PVOID, CUtlBuffer &, int);
//static volatile DWORD BInitTextBuffer =
//	gSignatures.GetClientSignature("E8 ? ? ? ? 83 BD ? ? ? ? ? 8A D8") + 1;
//XASSERT(BInitTextBuffer);

//BInitTextBuffer = ((*(PDWORD)(BInitTextBuffer)) + BInitTextBuffer + 4);

//XASSERT(BInitTextBuffer);

//typedef PVOID(__thiscall * GetItemSchemaFn)(void);
//static volatile DWORD getItemSchemaFunc =
//	gSignatures.GetClientSignature("A1 ? ? ? ? 85 C0 75 41");
//XASSERT(getItemSchemaFunc);

//GetItemSchemaFn GetItemSchema = (GetItemSchemaFn)(getItemSchemaFunc);

//char *buffer = new char[4000000];

//char *      homeDir = getenv("HOMEPATH");
//std::string filePath = std::string(homeDir) + "\\Iridium\\custom_schema.txt";

//FILE *items_game = fopen(filePath.c_str(), "r");

//if (items_game) {
//	size_t newLen = fread(buffer, sizeof(char), 4000000, items_game);

//	CUtlBuffer buf(buffer, newLen, 9);

//	if (ferror(items_game) != 0) {
//		fputs("Error reading file", stderr);
//	}
//	else {
//		buffer[newLen++] = '\0'; // Just to be safe.
//	}
//	bool ret = ((BInitTextBufferFn)BInitTextBuffer)(GetItemSchema(), buf, 0);

//	Log::Error("Returned %s", ret ? "true" : "false");
//}
//else {
//	Log::Error("Error loading %s", filePath.c_str());
//}

//delete[] buffer;

//gInts.Engine->ClientCmd_Unrestricted("echo Cvar Bypass	= F5 to toggle");
//gInts.Engine->ClientCmd_Unrestricted("echo Game Events	= F6 to toggle");
//gInts.Engine->ClientCmd_Unrestricted("echo Third Person	= F7 to toggle");
//gInts.Engine->ClientCmd_Unrestricted("echo Anti-Aim		= F8 to toggle");
//gInts.Engine->ClientCmd_Unrestricted("echo Chat Spam	= F9 to toggle");
//gInts.Engine->ClientCmd_Unrestricted("echo Custom DC	= F10, does nothing because I don't know how to paste it");
//gInts.Engine->ClientCmd_Unrestricted("echo Panic Key	= F11 :)");
//gInts.Engine->ClientCmd_Unrestricted("echo Clean SS		= F12 / Always on (cuz it came that way in darkstorm!!!)");
//gInts.Engine->ClientCmd_Unrestricted("echo Tourney Spam = Make your own script, dipshit");
//gInts.Engine->ClientCmd_Unrestricted("echo Custom FOV	= It already exists in the original game, retard / 'fov_desired #' in console to toggle");
//gInts.Engine->ClientCmd_Unrestricted("echo Newline Name = lolno too edgy / 'quit being edgy' in console to toggle");
//gInts.Engine->ClientCmd_Unrestricted("echo Ping Mask	= (so ppl think ur asian when playing with asian vpn ^______^ / 'cl_cmdrate 66' in console to toggle");
//gInts.Engine->ClientCmd_Unrestricted("echo\x0A");
//gInts.Engine->ClientCmd_Unrestricted("echo __________________________________________________________________________________________________________________");
//gInts.Engine->ClientCmd_Unrestricted("echo\x0A");
//gInts.Engine->ClientCmd_Unrestricted("echo <3");
//gInts.Engine->ClientCmd_Unrestricted("echo \x0A"); //I don't think these newlines even work :thinking:
//gInts.Engine->ClientCmd_Unrestricted("echo Actual legit credits: gir for darkstorm I guess (even tho it should be kalvin xd), stick for pasteassium, plasmafart for some pMenu shit, outizm and nullifiedcat for rcond stuff, chill for creating chillware, dice for being fat, and... miketf2 I guess? also unknowncheats for having stupid answers for stupid questions");
//gInts.Engine->ClientCmd_Unrestricted("echo m4rida by youtube.com/GodLike100");
//Setup the Panel hook so we can draw.