	#include "SDK.h"
#include "Client.h"
#include "Util.h"
#include "Aimbot.h"
#include "CAimbot.h"
#include "Triggerbot.h"
#include "Misc.h"
#include "GUI.h"
#include "CTimeShift.h"
#include "CBacktracking.h"
#include "CAntiAim.h"

//#ifdef  _DEBUG
//FILE* fp;
//#include <iostream>
//#endif //  _DEBUG

static int iSpeedCounter = 0;
float fLastKeyTime = 0;
#define SET_LAST_KEY_TIME fLastKeyTime = gInts.g_pGlobals->curtime;

CAntiAim gAntiaim;

bool BindPressed(int key)
{
	return (GetAsyncKeyState(key) && fLastKeyTime + 0.25f < gInts.g_pGlobals->curtime);
}

//============================================================================================
bool __fastcall Hooked_CreateMove(PVOID ClientMode, int edx, float input_sample_frametime, CUserCmd* pCommand)
{
	if (gCvars.hvh_speedhack/* && gCvars.hvh_speedhack_mode*/)	//credits gir
		if (iSpeedCounter > 0 && Util->IsKeyPressed(gCvars.hvh_speedhack_key))
		{
			iSpeedCounter--;
			pCommand->tick_count--;
			_asm
			{
				push eax;
				mov eax, dword ptr ss : [ebp];
				mov eax, [eax];
				lea eax, [eax + 0x4];
				sub[eax], 0x5;
				pop eax;
			}
		}
		else
			iSpeedCounter = gCvars.hvh_speedhack_amt;

	VMTManager& hook = VMTManager::GetHook(ClientMode); //Get a pointer to the instance of your VMTManager with the function GetHook.
	bool bReturn = hook.GetMethod<bool(__thiscall*)(PVOID, float, CUserCmd*)>(gOffsets.iCreateMoveOffset)(ClientMode, input_sample_frametime, pCommand); //Call the original.
	try
	{
		//if (!pCommand)
		//	return bReturn;

		if (!pCommand->tick_count)
			return bReturn;

		CBaseEntity* pLocal = GetBaseEntity(me);
		if (!pLocal)
			return bReturn;

		DWORD dwEBP = NULL;
		__asm MOV dwEBP, EBP;
		bool* bSendPacket = (bool*)(*(char**)dwEBP - 0x1);

		CBaseCombatWeapon* wep = pLocal->GetActiveWeapon();

		gAim.Run(pLocal, pCommand);
		gMisc.Run(pLocal, pCommand, bSendPacket);
		gTrigger.Triggerbot_Run(pLocal, pCommand);
		gTrigger.Triggershoot_Run(pLocal, pCommand);
		gBacktrack.Run(pCommand);
		gMisc.lastAngles = pCommand->viewangles;

		if (gAntiaim.fakelag_active.value)
		{
			if (gAntiaim.fakelag_disable_attack.value && (pCommand->buttons & IN_ATTACK || pCommand->buttons & IN_ATTACK2))
				return;

			int iTicks = 0;
			int iChoke = gAntiaim.fakelag_choke.value;

			if (iTicks <= iChoke)
			{
				iTicks++;
				*bSendPacket = false;
			}
			else
			{
				*bSendPacket = true;
				iTicks = 0;
			}
		}

//#ifdef _DEBUG
//		freopen_s(&fp, "CONOUT$", "w", stdout);
//		std::cout << "curtime: " << gInts.g_pGlobals->curtime << endl;
//		std::cout << "BindPressed(VK_F1): " << BindPressed(VK_F1) << endl;
//		std::cout << "fLastKeyTime: " << fLastKeyTime << endl;
//		//std::cout << "crit chance: " << wep->GetObservedCritChance() << endl;
//#endif

	}
	catch(...)
	{
		Log::Fatal("Failed Hooked_CreateMove()");
	}

	return bReturn = false;
}
//============================================================================================
int __fastcall Hooked_KeyEvent(PVOID CHLClient, int edx, int eventcode, int keynum, const char *currentBinding)
{
	if (eventcode == 1)
	{
		if (keynum == 72) //insert
		{
			//gGUI.bMenuActive = !gGUI.bMenuActive;
			//gSettings.SaveSettings(".\\Iridium.json");
		}

	//	if (gCheatMenu.bMenuActive)
	//	{
	//		if (keynum == 88 || keynum == 112) // Up
	//		{

	//			if (gCheatMenu.iMenuIndex > 0) gCheatMenu.iMenuIndex--;
	//			else gCheatMenu.iMenuIndex = gCheatMenu.iMenuItems - 1;
	//			return 0;

	//		}
	//		else if (keynum == 90 || keynum == 113) // Down
	//		{
	//			if (gCheatMenu.iMenuIndex < gCheatMenu.iMenuItems - 1) gCheatMenu.iMenuIndex++;
	//			else gCheatMenu.iMenuIndex = 0;
	//			return 0;

	//		}
	//		else if (keynum == 89 || keynum == 107) // Left
	//		{
	//			if (gCheatMenu.pMenu[gCheatMenu.iMenuIndex].value)
	//			{
	//				gCheatMenu.pMenu[gCheatMenu.iMenuIndex].value[0] -= gCheatMenu.pMenu[gCheatMenu.iMenuIndex].flStep;
	//				if (gCheatMenu.pMenu[gCheatMenu.iMenuIndex].value[0] < gCheatMenu.pMenu[gCheatMenu.iMenuIndex].flMin)
	//					gCheatMenu.pMenu[gCheatMenu.iMenuIndex].value[0] = gCheatMenu.pMenu[gCheatMenu.iMenuIndex].flMax;
	//			}
	//			return 0;

	//		}
	//		else if (keynum == 91 || keynum == 108) // Right
	//		{
	//			if (gCheatMenu.pMenu[gCheatMenu.iMenuIndex].value)
	//			{
	//				gCheatMenu.pMenu[gCheatMenu.iMenuIndex].value[0] += gCheatMenu.pMenu[gCheatMenu.iMenuIndex].flStep;
	//				if (gCheatMenu.pMenu[gCheatMenu.iMenuIndex].value[0] > gCheatMenu.pMenu[gCheatMenu.iMenuIndex].flMax)
	//					gCheatMenu.pMenu[gCheatMenu.iMenuIndex].value[0] = gCheatMenu.pMenu[gCheatMenu.iMenuIndex].flMin;
	//			}
	//			return 0;

	//		}

	//	}
	}

	VMTManager &hook = VMTManager::GetHook(CHLClient); // Get a pointer to the instance of your VMTManager with the function GetHook.
	return hook.GetMethod<int(__thiscall *)(PVOID, int, int, const char *)>(gOffsets.iKeyEventOffset)(CHLClient, eventcode, keynum, currentBinding); // Call the original.
}



////TIME SHIFT MEMES
//if (Util->IsKeyPressed(gCvars.misc_remove_cond_key) && gCvars.misc_remove_cond)
//	gTShift.Regular(gCvars.misc_remove_cond_amt);

//if (gCvars.misc_insta_decloak)
//	gTShift.InstaDecloak(pLocal, pCommand);

//INetChannel* ch = (INetChannel*)gInts.Engine->GetNetChannelInfo();
//int& m_nOutSequenceNr = *(int*)((unsigned)ch + 8);
//m_nOutSequenceNr += gCvars.misc_remove_cond_amt;


//if (gCvars.m4rida_menu)
//{
//	//m4rida super menu
//	if ((GetAsyncKeyState(VK_F1) && fLastKeyTime + 0.25f < gInts.g_pGlobals->curtime)) //Aim
//	{
//		gCvars.aimbot_active = !gCvars.aimbot_active; SET_LAST_KEY_TIME
//	}
//	else if (BindPressed(VK_F2)) //Trigger
//	{
//		gCvars.triggerbot_active = !gCvars.triggerbot_active; SET_LAST_KEY_TIME
//	}
//	else if (BindPressed(VK_F3)) //ESP
//	{
//		gCvars.esp_active = !gCvars.esp_active; SET_LAST_KEY_TIME
//	}
//	else if (BindPressed(VK_F4)) //Bhop
//	{
//		gCvars.misc_bunnyhop = !gCvars.misc_bunnyhop; SET_LAST_KEY_TIME
//	}
//	else if (BindPressed(VK_F5)) //Cvar bypass
//	{
//		gInts.Engine->ClientCmd_Unrestricted("say Guys! I'm a loser! I tried to use *Cvar bypass* in m4rida.jar!");
//	}
//	else if (BindPressed(VK_F6)) //Game Events
//	{
//		gInts.Engine->ClientCmd_Unrestricted("say Guys! I'm a loser! I tried to use *Game Events* in m4rida.jar!");
//	}
//	else if (BindPressed(VK_F7)) //Third Person
//	{
//		//int& m_nForceTauntCam = pLocal->GetTauntCam();
//	}
//	else if (BindPressed(VK_F8)) //Anti-Aim
//	{
//		gCvars.hvh_aa_pitch = !gCvars.hvh_aa_pitch; SET_LAST_KEY_TIME
//	}
//	else if (BindPressed(VK_F9)) //Chat Spam
//	{
//		//gCvars.misc_bunnyhop = !gCvars.misc_bunnyhop; SET_LAST_KEY_TIME
//	}
//	else if (BindPressed(VK_F10)) //Custom DC
//	{
//		gInts.Engine->ClientCmd_Unrestricted("say Guys! I'm a loser! I tried to use *Custom DC* in m4rida.jar!");
//	}
//	else if (BindPressed(VK_F11)) //Panic Key
//	{
//		gInts.Engine->ClientCmd_Unrestricted("say Guys! I'm a loser! I just crashed my game by using the panic key in m4rida.jar!");
//		while (1)
//			gInts.Engine->ClientCmd_Unrestricted("hud_reloadscheme");
//	}
//	else if (BindPressed(VK_F12)) //Clean SS
//	{
//		gInts.Engine->ClientCmd_Unrestricted("say F12");
//	}

//}
