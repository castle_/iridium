#pragma once
#include "SDK.h"

class CAimbot
{
public:
	Checkbox		aimbot_active			=				Checkbox("Active");
	Slider			aimbot_fov				=				Slider("Aim FOV", 0, 1, 180, 1);
	Checkbox		aimbot_slowaim			=				Checkbox("Slow Aim");
	Slider			aimbot_slow_amt			=				Slider("Slow Amt", 0, 1, 40, 2);
	Checkbox		aimbot_autoshoot		=				Checkbox("Autoshoot");
	Checkbox		aimbot_silent			=				Checkbox("Aim Silent");
	Checkbox		aimbot_hitscan			=				Checkbox("Aim Hitscan");
	Listbox 		aimbot_position			=				Listbox("Aim Position", { "Auto", "Head", "Pelvis", "Lower Torse", "Lower Mid. Torse" });
	Checkbox		aimbot_minigun_toggle   =				Checkbox("Minigun Toggle");
	Checkbox		aimbot_ignore_cloak		=				Checkbox("Ignore Cloak");
	Checkbox		aimbot_adv_feet_aim		=				Checkbox("Advanced Feet Aim");
	Checkbox		aimbot_wait_for_charge  =				Checkbox("Wait For Charge");
	Checkbox		aimbot_scoped_only		=				Checkbox("Scoped Only");
	Checkbox		aimbot_canshoot			=				Checkbox("CanShoot() Check");

	KeyBind			aimbot_key				=				KeyBind("Aim Key", VK_XBUTTON2);
	Listbox			aimbot_key_alt			=				Listbox("Aim Bot Key", { "None", "Mouse1", "Mouse2", "Mouse3", "Mouse4", "Mouse5", "Shift", "Alt", "F" }, VK_XBUTTON2);

	void Run(CBaseEntity* pLocal, CUserCmd* pCommand);

	float GetFOV(Vector angle, Vector src, Vector dst);

private:
	void MakeVector(Vector angle, Vector& vector);

	Vector calc_angle(Vector src, Vector dst);

	int GetBestTarget(CBaseEntity* pLocal, CUserCmd* pCommand);

	int GetBestHitbox(CBaseEntity* pLocal, CBaseEntity* pEntity);

	bool CanAmbassadorHeadshot(CBaseEntity* pLocal);
};

extern CAimbot gAim; //dsajkhfdlksahf