#pragma once
#include "SDK.h"
#include "GUI.h"
#include "FrameStageNotify.h"
#include "CNetVars.h"
#include "NetVars.h"
#include "CUtlVector.h"

class CSkinchanger
{
public:
	void FSN(ClientFrameStage_t stage);

private:

};

class CAttribute
{
public:
	CAttribute();
	void *pad;
	uint16_t m_iAttributeDefinitionIndex;
	float m_flValue;
	unsigned int pad2;

	inline CAttribute(uint16_t iAttributeDefinitionIndex, float flValue)
	{
		m_iAttributeDefinitionIndex = iAttributeDefinitionIndex;
		m_flValue = flValue;
	}
};

class CAttributeList
{
public:
	void *pad;
	CUtlVector<CAttribute, CUtlMemory<CAttribute> > m_Attributes;

	inline void AddAttribute(int iIndex, float flValue)
	{
		//15 = MAX_ATTRIBUTES
		if (m_Attributes.Count() > 14)
			return;

		CAttribute attr(iIndex, flValue);

		m_Attributes.AddToTail(attr);
	}
};

enum attrib_ids
{
	//Apply these for australium item
	AT_is_australium_item = 2027,
	AT_loot_rarity = 2022,
	AT_item_style_override = 542,

	//This one and item style override 0 to golden pan
	AT_ancient_powers = 150,

	AT_uses_stattrack_module = 719,

	AT_is_festive = 2053,

	//Only ks sheen is visible
	AT_sheen = 2014,

	AT_unusual_effect = 134,
	AT_particle_effect = 370,

	AT_weapon_allow_inspect = 731,
};

enum weapon_effects
{
	WE_hot = 701,
	WE_isotope = 702,
	WE_cool = 703,
	WE_energyorb = 704,
};

//extern CAttribute gAttr;
//extern CAttributeList gAttrList;
extern CSkinchanger gSkins;
