#include "CBacktracking.h"

BacktrackData btHeadPositions[24][14];
CBacktracking gBacktrack;

//=======================================================================
inline float distance_point_to_line(Vector Point, Vector LineOrigin, Vector Dir)
{
	auto PointDir = Point - LineOrigin;

	auto TempOffset = PointDir.Dot(Dir) / (Dir.x*Dir.x + Dir.y*Dir.y + Dir.z*Dir.z);
	if (TempOffset < 0.000001f)
		return FLT_MAX;

	auto PerpendicularPoint = LineOrigin + (Dir * TempOffset);

	return (Point - PerpendicularPoint).Length();
}

inline Vector angle_vector(Vector meme)
{
	auto sy = sin(meme.y / 180.f * static_cast<float>(PI));
	auto cy = cos(meme.y / 180.f * static_cast<float>(PI));

	auto sp = sin(meme.x / 180.f * static_cast<float>(PI));
	auto cp = cos(meme.x / 180.f* static_cast<float>(PI));

	return Vector(cp*cy, cp*sy, -sp);
}
//=======================================================================

void CBacktracking::Run(CUserCmd* cmd)
{
	if (!gCvars.misc_backtracking) return;
	int iBestTarget = -1;
	float bestFov = 99999;
	gCvars.backtrack_tick = 0;
	CBaseEntity* pLocal = gInts.EntList->GetClientEntity(me);

	if (!pLocal)
		return;
	if (pLocal->GetLifeState() != LIFE_ALIVE)
		return;

	for (int i = 1; i <= gInts.Engine->GetMaxClients(); ++i)
	{
		CBaseEntity* pEntity = GetBaseEntity(i);

		if (!pEntity)
			continue;
		if (pEntity->IsDormant())
			continue;
		if (pEntity->GetLifeState() != LIFE_ALIVE)
			continue;
		if (pEntity->GetTeamNum() == pLocal->GetTeamNum())
			continue;

		float fOurTickCount = cmd->tick_count;
		Vector vTargetHitbox = pEntity->GetHitboxPosition(0);

		btHeadPositions[i][cmd->command_number % (gCvars.bt_maxticks + 1)] = BacktrackData{ fOurTickCount, vTargetHitbox };

		Vector ViewDir = angle_vector(cmd->viewangles);
		float FOVDistance = distance_point_to_line(vTargetHitbox, pLocal->GetEyePosition(), ViewDir);

		if (bestFov > FOVDistance)
		{
			bestFov = FOVDistance;
			iBestTarget = i;
		}
	}

	if (iBestTarget != -1)
	{
		int bestTick = 0;
		float bestFOV = 30;
		for (int t = 0; t < gCvars.bt_maxticks; ++t)
		{
			Vector ViewDir = angle_vector(cmd->viewangles);
			float tempFOV = distance_point_to_line(btHeadPositions[iBestTarget][t].hitboxpos, pLocal->GetEyePosition(), ViewDir);
			if (bestFOV > tempFOV)
			{
				bestTick = t, bestFOV = tempFOV;
				gCvars.backtrack_tick = t;
			}
		}

		if (cmd->tick_count >= 0 && cmd->buttons & IN_ATTACK)
			cmd->tick_count = btHeadPositions[iBestTarget][bestTick].tickcount;
	}
}