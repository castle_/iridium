#include "CDrawManager.h"
#include "ESP.h"
//===================================================================================
CDrawManager gDraw;
//Fonts gFonts;
#define ESP_HEIGHT 14
//===================================================================================
void CDrawManager::Initialize( )
{
	if ( gInts.Surface == NULL )
		return;


	if (!gCvars.cade_menu)
	{
		gInts.Engine->GetScreenSize(gScreenSize.iScreenWidth, gScreenSize.iScreenHeight);
		Tahoma12 = gInts.Surface->CreateFont();
		gInts.Surface->SetFontGlyphSet(Tahoma12, "Tahoma", ESP_HEIGHT, 500, 0, 0, 0x200);
		Consolas14 = gInts.Surface->CreateFont();
		gInts.Surface->SetFontGlyphSet(Consolas14, "Consolas", 14, 500, 0, 0, 0x200);
		Consolas16 = gInts.Surface->CreateFont();
		gInts.Surface->SetFontGlyphSet(Consolas16, "Consolas", 16, 500, 0, 0, 0x200);
		Verdana14 = gInts.Surface->CreateFont();
		gInts.Surface->SetFontGlyphSet(Verdana14, "Verdana", 14, 500, 0, 0, 0x200);
	}
	else
	{
		gFonts.Initialize();
		gTex.Initialize();

		Reload();
	}



	int _w, _h;
	for (int i = 0; i < g_vRankFiles.size(); i++) {
		g_vRankTextures[i] = gInts.Surface->CreateNewTextureID(false);
		gInts.Surface->DrawSetTextureFile(g_vRankTextures[i], g_vRankFiles.at(i).c_str(), false, false);
		gInts.Surface->DrawGetTextureSize(g_vRankTextures[i], _w, _h);
		gInts.Surface->DrawSetTextureRGBA(g_vRankTextures[i], Color::White()._color, _w, _h);
	}


	//for (int i = 0; i < g_vClassIcons.size(); i++) {
	//	int _w, _h;
	//	g_vClassIcons[i] = gInts.Surface->CreateNewTextureID(false);
	//	gInts.Surface->DrawSetTextureFile(g_vClassIcons[i], g_vClassIcons.at(i).c_str());
	//	gInts.Surface->DrawGetTextureSize(g_vClassIcons[i], _w, _h);
	//	byte gay[] = { 255, 255, 255, 255 };
	//	gInts.Surface->DrawSetTextureRGBA(g_vClassIcons[i], gay, _w, _h);
	//}

}
//===================================================================================
void CDrawManager::Reload() // Create (or reload) your font styles
{
	if (gInts.Surface == NULL)
		return;

	gInts.Engine->GetScreenSize(gScreenSize.iScreenWidth, gScreenSize.iScreenHeight);

	gFonts.Reload();
	gTex.Reload();
}
//===================================================================================

Color CDrawManager::GetPlayerColor(CBaseEntity* pPlayer, int alpha, bool isMenu) //TODO: Rework this garbage
{
	Color clrAimbotTarget, clrFriend, clrRage, clrRedTeam, clrRedTeamMenu, clrBluTeam, clrBluTeamMenu, clrHandz;

	switch (gESP.visual_colors.value)
	{
	case 0: //Stock m4risa colors
		clrAimbotTarget = Color(0, 255, 0, alpha);
		clrFriend = Color(0, 255, 255, alpha);
		clrRage = Color(255, 255, 0, alpha);
		clrRedTeam = Color(255, 0, 0, alpha); //Pure red
		clrRedTeamMenu = Color(255, 0, 0, alpha); //Pure red
		clrBluTeam = Color(0, 0, 255, alpha); //Pure blue
		clrBluTeamMenu = Color(0, 128, 255, alpha);
		break;
	case 1: //Potassium Colors
		clrAimbotTarget = Color(0, 255, 0, alpha);
		clrFriend = Color(66, 244, 140, alpha); //Teal
		clrRage = Color(255, 255, 0, alpha);
		clrRedTeam = Color(255, 20, 20, alpha); //TF2 red
		clrRedTeamMenu = Color(255, 20, 20, alpha); //TF2 red
		clrBluTeam = Color(0, 153, 255, alpha);  //TF2 blue
		clrBluTeamMenu = Color(0, 153, 255, alpha); //TF2 blue
		break;
	case 2: //Null Core Colors
		clrAimbotTarget = Color(200, 0, 200, alpha); //Pink
		clrFriend = Color(66, 244, 140, alpha); //Teal
		clrRage = Color(255, 255, 0, alpha);
		clrRedTeam = Color(250, 90, 80, alpha); //NC Red
		clrRedTeamMenu = Color(15, 150, 150, alpha); //NC Green
		clrBluTeam = Color(150, 190, 233, alpha); //NC Light Blue
		clrBluTeamMenu = Color(15, 150, 150, alpha); //NC Green
		break;
	case 3: //Custom Colors
		clrAimbotTarget = gESP.visual_clr_aim.color;
		clrFriend = gESP.visual_clr_friend.color;
		clrRage = gESP.visual_clr_rage.color;
		clrRedTeam = gESP.visual_clr_red.color;
		clrRedTeamMenu = gESP.visual_clr_red_menu.color;
		clrBluTeam = gESP.visual_clr_blu.color;
		clrBluTeamMenu = gESP.visual_clr_blu_menu.color;
		//clrHandz = gESP.visual_clr_aim.color;
		break;
	default:
		clrAimbotTarget = clrFriend = clrRage = clrRedTeam = clrRedTeamMenu = clrBluTeam = clrBluTeamMenu = Color::Black(); //just in case ;)
		break;
	}

	if (pPlayer->IsDev())
		return Color(102, 111, 255, alpha);
	else
	if (pPlayer->GetIndex() == gCvars.iAimbotIndex) //Aimbot Target
		return clrAimbotTarget;
	else if (pPlayer->GetCond() & TFCond_Cloaked)
		return Color(255, 255, 255, alpha/2);
	else if (!gCvars.PlayerMode[pPlayer->GetIndex()])
		return clrFriend;
	else if (gCvars.PlayerMode[pPlayer->GetIndex()] == 2)
		return Color(255, 255, 0, alpha);
	else
	{
		switch (pPlayer->GetTeamNum())
		{
		case 2: //RED
			if (isMenu)
				return clrRedTeamMenu;
			return clrRedTeam;
		case 3: //BLU
		{
			if (isMenu)
				return clrBluTeamMenu;
			else 
				return clrBluTeam;
		}
		default: //Spec
			return Color::White();
		}
	}

	return Color::Black(); //just in case!
}
//===================================================================================
void CDrawManager::DrawString( int x, int y, Color clrColor, const wchar_t *pszText)
{
	if( pszText == NULL )
		return;

	gInts.Surface->DrawSetTextPos( x, y );
	gInts.Surface->DrawSetTextFont( gFonts.esp );
	gInts.Surface->DrawSetTextColor( clrColor.r(), clrColor.g(), clrColor.b(), clrColor.a() );
	gInts.Surface->DrawPrintText( pszText, wcslen( pszText ) );
}
//===================================================================================
void CDrawManager::DrawString( int x, int y, Color clrColor, const char *pszText, ... )
{
	if( pszText == NULL )
		return;

	va_list va_alist;
	char szBuffer[1024] = { '\0' };
	wchar_t szString[1024] = { '\0' };

	va_start( va_alist, pszText );
	vsprintf_s( szBuffer, pszText, va_alist );
	va_end( va_alist );

	wsprintfW( szString, L"%S", szBuffer );

	gInts.Surface->DrawSetTextPos( x, y );
	gInts.Surface->DrawSetTextFont( gFonts.esp );
	gInts.Surface->DrawSetTextColor( clrColor.r(), clrColor.g(), clrColor.b(), clrColor.a() );
	gInts.Surface->DrawPrintText( szString, wcslen( szString ) );
}
void CDrawManager::DrawString(int x, int y, Color color, const char *pszText, HFont font, bool bCenter)
{
	if (pszText == NULL)
		return;

	va_list va_alist;
	char szBuffer[1024] = { '\0' };
	wchar_t szString[1024] = { '\0' };

	va_start(va_alist, pszText);
	vsprintf_s(szBuffer, pszText, va_alist);
	va_end(va_alist);

	wsprintfW(szString, L"%S", szBuffer);

	if (bCenter)
	{
		int Wide = 0, Tall = 0;
		gInts.Surface->GetTextSize(font, szString, Wide, Tall);
		x -= Wide / 2;
	}

	gInts.Surface->DrawSetTextPos(x, y);
	gInts.Surface->DrawSetTextFont(font);
	gInts.Surface->DrawSetTextColor(color[0], color[1], color[2], color[3]);
	gInts.Surface->DrawPrintText(szString, wcslen(szString));
}
//===================================================================================
void CDrawManager::DrawStringCenter(int x, int y, Color clrColor, const char* szText, ...)
{
	if (!szText)
		return;

	va_list va_alist = nullptr;
	int Wide = 0, Tall = 0;
	char szBuffer[256] = { '\0' };
	wchar_t szString[128] = { '\0' };

	va_start(va_alist, szText);
	vsprintf_s(szBuffer, szText, va_alist);
	va_end(va_alist);

	MultiByteToWideChar(CP_UTF8, 0, szBuffer, -1, szString, 128);


	gInts.Surface->GetTextSize(gFonts.esp, szString, Wide, Tall);
	x -= Wide / 2;


	gInts.Surface->DrawSetTextPos(x, y);
	gInts.Surface->DrawSetTextFont(gFonts.esp);
	gInts.Surface->DrawSetTextColor(clrColor.r(), clrColor.g(), clrColor.b(), clrColor.a());
	gInts.Surface->DrawPrintText(szString, strlen(szBuffer));
}
void CDrawManager::DrawStringFont(int x, int y, Color clrColor, const char * pszText, int font, ...)
{
	if (pszText == NULL)
		return;

	va_list va_alist;
	char szBuffer[1024] = { '\0' };
	wchar_t szString[1024] = { '\0' };

	va_start(va_alist, pszText);
	vsprintf_s(szBuffer, pszText, va_alist);
	va_end(va_alist);

	wsprintfW(szString, L"%S", szBuffer);

	gInts.Surface->DrawSetTextPos(x, y);
	gInts.Surface->DrawSetTextFont(font);
	gInts.Surface->DrawSetTextColor(clrColor.r(), clrColor.g(), clrColor.b(), clrColor.a());
	gInts.Surface->DrawPrintText(szString, wcslen(szString));
}
void CDrawManager::DrawStringCenterFont(int x, int y, Color clrColor, const char * szText, int font, ...)
{
	if (!szText)
		return;

	va_list va_alist = nullptr;
	int Wide = 0, Tall = 0;
	char szBuffer[256] = { '\0' };
	wchar_t szString[128] = { '\0' };

	va_start(va_alist, szText);
	vsprintf_s(szBuffer, szText, va_alist);
	va_end(va_alist);

	MultiByteToWideChar(CP_UTF8, 0, szBuffer, -1, szString, 128);


	gInts.Surface->GetTextSize(font, szString, Wide, Tall);
	x -= Wide / 2;


	gInts.Surface->DrawSetTextPos(x, y);
	gInts.Surface->DrawSetTextFont(font);
	gInts.Surface->DrawSetTextColor(clrColor.r(), clrColor.g(), clrColor.b(), clrColor.a());
	gInts.Surface->DrawPrintText(szString, strlen(szBuffer));
}
//===================================================================================
byte CDrawManager::GetESPHeight( )
{
	return ESP_HEIGHT;
}
//===================================================================================
void CDrawManager::DrawLine(int x, int y, int x1, int y1, Color clrColor)
{
	gInts.Surface->DrawSetColor(clrColor.r(), clrColor.g(), clrColor.b(), clrColor.a());
	gInts.Surface->DrawLine(x, y, x1, y1);
}
//===================================================================================
void CDrawManager::DrawLineEx(int x, int y, int w, int h, Color clrColor)
{
	gInts.Surface->DrawSetColor(clrColor.r(), clrColor.g(), clrColor.b(), clrColor.a());
	gInts.Surface->DrawLine(x, y, x + w, y + h);
}
//===================================================================================
void CDrawManager::DrawRect( int x, int y, int w, int h, Color clrColor)
{
	gInts.Surface->DrawSetColor( clrColor.r(), clrColor.g(), clrColor.b(), clrColor.a() );
	gInts.Surface->DrawFilledRect( x, y, x + w, y + h );
}
void CDrawManager::DrawPolygon(int count, AVertex_t* verts, HTexture texture)
{
	gInts.Surface->DrawSetColor(255, 255, 255, 255); // keep this full color and opacity use the RGBA @top to set values.
	gInts.Surface->DrawSetTexture(texture); // bind texture
	gInts.Surface->DrawTexturedPolygon(count, (Vertex_t*)verts);
}
void CDrawManager::DrawCircle(int x, int y, int radius, int segments, Color clrColor)
{
	gInts.Surface->DrawSetColor(clrColor.r(), clrColor.g(), clrColor.b(), clrColor.a());
	gInts.Surface->DrawOutlinedCircle(x, y, radius, segments);
}
void CDrawManager::DrawFilledCircle(int centerx, int centery, float radius, Color clrColor)
{
	for (int i = 0; i < radius; i++)
		gInts.Surface->DrawColoredCircle(centerx, centery, i, clrColor.r(), clrColor.g(), clrColor.b(), clrColor.a());
}
//===================================================================================
void CDrawManager::OutlineRect( int x, int y, int w, int h, Color clrColor)
{
	gInts.Surface->DrawSetColor( clrColor.r(), clrColor.g(), clrColor.b(), clrColor.a() );
	gInts.Surface->DrawOutlinedRect( x, y, x + w, y + h );
}
//===================================================================================
void CDrawManager::DrawTexturedRect(int textureId, int x, int y, int w, int h)
{
	int x0 = x;
	int x1 = x0 + w;
	int y0 = y;
	int y1 = y0 + h;
	gInts.Surface->DrawSetColor(255, 255, 255, 255);
	gInts.Surface->DrawSetTexture(textureId);
	gInts.Surface->DrawTexturedRect(x0, y0, x1, y1);
}
//===================================================================================
void CDrawManager::DrawBox( Vector vOrigin, int r, int g, int b, int alpha, int box_width, int radius )
{
	Vector vScreen;

	if( !WorldToScreen( vOrigin, vScreen ) )
		return;

	int radius2 = radius<<1;

	OutlineRect( vScreen.x - radius + box_width, vScreen.y - radius + box_width, radius2 - box_width, radius2 - box_width, Color::Black() );
	OutlineRect( vScreen.x - radius - 1, vScreen.y - radius - 1, radius2 + ( box_width + 2 ), radius2 + ( box_width + 2 ), Color::Black() );
	DrawRect( vScreen.x - radius + box_width, vScreen.y - radius, radius2 - box_width, box_width, Color( r, g, b, alpha ));
	DrawRect( vScreen.x - radius, vScreen.y + radius, radius2, box_width, Color( r, g, b, alpha ));
	DrawRect( vScreen.x - radius, vScreen.y - radius, box_width, radius2, Color( r, g, b, alpha ));
	DrawRect( vScreen.x + radius, vScreen.y - radius, box_width, radius2 + box_width, Color( r, g, b, alpha ) );
}
bool CDrawManager::GetEntityBounds(CBaseEntity * pEntity, float & x, float & y, float & w, float & h)
{
	if (!pEntity) return false;

	const matrix3x4& vMatrix = pEntity->GetRgflCoordinateFrame();

	Vector vMin = pEntity->GetCollideableMins();
	Vector vMax = pEntity->GetCollideableMaxs();

	Vector vPointList[] = {
		Vector(vMin.x, vMin.y, vMin.z),
		Vector(vMin.x, vMax.y, vMin.z),
		Vector(vMax.x, vMax.y, vMin.z),
		Vector(vMax.x, vMin.y, vMin.z),
		Vector(vMax.x, vMax.y, vMax.z),
		Vector(vMin.x, vMax.y, vMax.z),
		Vector(vMin.x, vMin.y, vMax.z),
		Vector(vMax.x, vMin.y, vMax.z)
	};

	Vector vTransformed[8];

	for (int i = 0; i < 8; i++)
		for (int j = 0; j < 3; j++)
			vTransformed[i][j] = vPointList[i].Dot((Vector&)vMatrix[j]) + vMatrix[j][3];

	Vector flb, brt, blb, frt, frb, brb, blt, flt;

	if (!gDraw.WorldToScreen(vTransformed[3], flb) ||
		!gDraw.WorldToScreen(vTransformed[0], blb) ||
		!gDraw.WorldToScreen(vTransformed[2], frb) ||
		!gDraw.WorldToScreen(vTransformed[6], blt) ||
		!gDraw.WorldToScreen(vTransformed[5], brt) ||
		!gDraw.WorldToScreen(vTransformed[4], frt) ||
		!gDraw.WorldToScreen(vTransformed[1], brb) ||
		!gDraw.WorldToScreen(vTransformed[7], flt))
		return false;

	Vector arr[] = { flb, brt, blb, frt, frb, brb, blt, flt };

	float left = flb.x;
	float top = flb.y;
	float right = flb.x;
	float bottom = flb.y;

	for (int i = 0; i < 8; i++)
	{
		if (left > arr[i].x)
			left = arr[i].x;
		if (top < arr[i].y)
			top = arr[i].y;
		if (right < arr[i].x)
			right = arr[i].x;
		if (bottom > arr[i].y)
			bottom = arr[i].y;
	}

	x = left;
	y = bottom;
	w = right - left;
	h = top - bottom;

	x += ((right - left) / 5);
	w -= ((right - left) / 5) * 2;

	return true;
}
void CDrawManager::DrawBoxOnEntity(int mode, CBaseEntity* entity, Color color)
{
	if (!mode) return;
	if (!entity) return;

	float x, y, w, h;
	if (!GetEntityBounds(entity, x, y, w, h))
		return;

	switch (mode)
	{
	case 0: break;
	case 1: //2D
		gDraw.OutlineRect(x - 1, y - 1, w + 2, h + 2, Color::Black());
		gDraw.OutlineRect(x, y, w, h, color);
		gDraw.OutlineRect(x + 1, y + 1, w - 2, h - 2, Color::Black());
		break;
	case 2: //Corner
		gDraw.DrawCornerBox(x, y, w, h - 1, 3, 5, color);
		break;
	case 3: //3D
	{
		//Vector vOrigin = entity->GetVecOrigin();
		//Vector min = entity->GetCollideableMins();
		//Vector max = entity->GetCollideableMaxs();
		//gDrawManager.Draw3DBox(min, max, color);
	}
	break;
	default:
		break;
	}
}
//===================================================================================
void CDrawManager::DrawCornerBox(int x, int y, int w, int h, int cx, int cy, Color Col)
{
	gDraw.DrawLine(x, y, x + (w / cx), y, Col);
	gDraw.DrawLine(x, y, x, y + (h / cy), Col);

	gDraw.DrawLine(x + w, y, x + w - (w / cx), y, Col);
	gDraw.DrawLine(x + w, y, x + w, y + (h / cy), Col);

	gDraw.DrawLine(x, y + h, x + (w / cx), y + h, Col);
	gDraw.DrawLine(x, y + h, x, y + h - (h / cy), Col);

	gDraw.DrawLine(x + w, y + h, x + w - (w / cx), y + h, Col);
	gDraw.DrawLine(x + w, y + h, x + w, y + h - (h / cy), Col);

}
//===================================================================================
bool CDrawManager::WorldToScreen(Vector &vOrigin, Vector &vScreen)
{
	const matrix3x4& worldToScreen = gInts.Engine->WorldToScreenMatrix(); //Grab the world to screen matrix from CEngineClient::WorldToScreenMatrix

	float w = worldToScreen[3][0] * vOrigin[0] + worldToScreen[3][1] * vOrigin[1] + worldToScreen[3][2] * vOrigin[2] + worldToScreen[3][3]; //Calculate the angle in compareson to the player's camera.
	vScreen.z = 0; //Screen doesn't have a 3rd dimension.

	if (w > 0.001) //If the object is within view.
	{
		float fl1DBw = 1 / w; //Divide 1 by the angle.
		vScreen.x = (gScreenSize.iScreenWidth / 2) + (0.5 * ((worldToScreen[0][0] * vOrigin[0] + worldToScreen[0][1] * vOrigin[1] + worldToScreen[0][2] * vOrigin[2] + worldToScreen[0][3]) * fl1DBw) * gScreenSize.iScreenWidth + 0.5); //Get the X dimension and push it in to the Vector.
		vScreen.y = (gScreenSize.iScreenHeight / 2) - (0.5 * ((worldToScreen[1][0] * vOrigin[0] + worldToScreen[1][1] * vOrigin[1] + worldToScreen[1][2] * vOrigin[2] + worldToScreen[1][3]) * fl1DBw) * gScreenSize.iScreenHeight + 0.5); //Get the Y dimension and push it in to the Vector.
		return true;
	}

	return false;
}
int CDrawManager::GetTextureIDFromEntity(CBaseEntity * pEntity, bool portrait)
{
	switch (pEntity->GetClassNum())
	{
	case TF2_Scout:
	{
		if (portrait)
		{
			if (pEntity->GetTeam() == TF_Team::RED)
				return gDraw.g_vRankTextures[ICON_SCOUT];
			else
				return gDraw.g_vRankTextures[ICON_SCOUT_BLUE];
		}
		else
		{
			return gDraw.g_vRankTextures[ICON_LEADERBOARD_SCOUT];
		}
	}
	case TF2_Soldier:
		if (portrait)
		{
			if (pEntity->GetTeam() == TF_Team::RED)
				return gDraw.g_vRankTextures[ICON_SOLDIER];
			else
				return gDraw.g_vRankTextures[ICON_SOLDIER_BLUE];
		}
		else
		{
			return gDraw.g_vRankTextures[ICON_LEADERBOARD_SOLDIER];
		}
	case TF2_Pyro:
		if (portrait)
		{
			if (pEntity->GetTeam() == TF_Team::RED)
				return gDraw.g_vRankTextures[ICON_PYRO];
			else
				return gDraw.g_vRankTextures[ICON_PYRO_BLUE];
		}
		else
		{
			return gDraw.g_vRankTextures[ICON_LEADERBOARD_PYRO];
		}
	case TF2_Demoman:
		if (portrait)
		{
			if (pEntity->GetTeam() == TF_Team::RED)
				return gDraw.g_vRankTextures[ICON_DEMOMAN];
			else
				return gDraw.g_vRankTextures[ICON_DEMOMAN_BLUE];
		}
		else
		{
			return gDraw.g_vRankTextures[ICON_LEADERBOARD_DEMOMAN];
		}
	case TF2_Heavy:
		if (portrait)
		{
			if (pEntity->GetTeam() == TF_Team::RED)
				return gDraw.g_vRankTextures[ICON_HEAVY];
			else
				return gDraw.g_vRankTextures[ICON_HEAVY_BLUE];
		}
		else
		{
			return gDraw.g_vRankTextures[ICON_LEADERBOARD_HEAVY];
		}
	case TF2_Engineer:
		if (portrait)
		{
			if (pEntity->GetTeam() == TF_Team::RED)
				return gDraw.g_vRankTextures[ICON_ENGINEER];
			else
				return gDraw.g_vRankTextures[ICON_ENGINEER_BLUE];
		}
		else
		{
			return gDraw.g_vRankTextures[ICON_LEADERBOARD_ENGINEER];
		}
	case TF2_Medic:
		if (portrait)
		{
			if (pEntity->GetTeam() == TF_Team::RED)
				return gDraw.g_vRankTextures[ICON_MEDIC];
			else
				return gDraw.g_vRankTextures[ICON_MEDIC_BLUE];
		}
		else
		{
			return gDraw.g_vRankTextures[ICON_LEADERBOARD_MEDIC];
		}
	case TF2_Sniper:
		if (portrait)
		{
			if (pEntity->GetTeam() == TF_Team::RED)
				return gDraw.g_vRankTextures[ICON_SNIPER];
			else
				return gDraw.g_vRankTextures[ICON_SNIPER_BLUE];
		}
		else
		{
			return gDraw.g_vRankTextures[ICON_LEADERBOARD_SNIPER];
		}
	case TF2_Spy:
		if (portrait)
		{
			if (pEntity->GetTeam() == TF_Team::RED)
				return gDraw.g_vRankTextures[ICON_SPY];
			else
				return gDraw.g_vRankTextures[ICON_SPY_BLUE];
		}
		else
		{
			return gDraw.g_vRankTextures[ICON_LEADERBOARD_SPY];
		}
	default:
		return -1;
	}

	return 0;
}
//===================================================================================
void CDrawManager::Draw3DBox(const Vector& mins, const Vector& maxs, Color Col)
{

	Vector diff = maxs - mins;

	Vector pointsToDraw[24] = {

		// Bottom Horizontal lines
		mins, mins + Vector(diff.x, 0, 0), // 1 - 2
		mins + Vector(diff.x, 0, 0), mins + Vector(diff.x, diff.y, 0), // 2 - 3
		mins + Vector(diff.x, diff.y, 0), mins + Vector(0, diff.y, 0), // 3 - 4
		mins + Vector(0, diff.y, 0), mins, // 4 - 1

										   // Vertical Lines
										   mins, mins + Vector(0, 0, diff.z), // 1 - 6
										   mins + Vector(diff.x, 0, 0), mins + Vector(diff.x, 0, diff.z), // 2 - 7
										   mins + Vector(diff.x, diff.y, 0), mins + Vector(diff.x, diff.y, diff.z), // 3 - 8
										   mins + Vector(0, diff.y, 0), mins + Vector(0, diff.y, diff.z), // 4 - 5

																										  // Top Horizontal lines.
																										  maxs, maxs - Vector(diff.x, 0, 0), // 8 - 5
																										  maxs - Vector(diff.x, 0, 0), maxs - Vector(diff.x, diff.y, 0), // 5 - 6
																										  maxs - Vector(diff.x, diff.y, 0), maxs - Vector(0, diff.y, 0), // 6 - 7
																										  maxs - Vector(0, diff.y, 0), maxs // 7 - 8
	};

	Vector startPos, endPos;
	for (int i = 0; i < 24; i += 2)
	{
		if (WorldToScreen(pointsToDraw[i], startPos))
		{
			if (WorldToScreen(pointsToDraw[i + 1], endPos))
				DrawLine(startPos.x, startPos.y, endPos.x, endPos.y, Col);
		}
	}

}